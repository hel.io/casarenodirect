<?php

class Potato_App_ImageOptimization
{
    const API_URL = 'http://app.potatocommerce.com:8080/image_optimization';

    /**
     * @param string $callback
     * @param array $images
     * @return Zend_Http_Response
     */
    static function process($callback, $images)
    {
        $data = Zend_Json::encode(
            array(
                'callback' => $callback,
                'images'   => $images
            )
        );
        $ch = curl_init(self::API_URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        return curl_exec($ch);
    }

    /**
     * @return array
     * @throws Exception
     */
    static function getOptimizedImages()
    {
        if (empty($_POST) || !array_key_exists('optimization_result', $_POST)) {
            throw new Exception('POST data is empty');
        }
        if (!is_string($_POST['optimization_result'])) {
            throw new Exception('String required');
        }
        if (!$images = json_decode($_POST['optimization_result'])) {
            throw new Exception('Not valid JSON data');
        }
        $collection = array();
        foreach ($images as $image) {
            array_push($collection, new Potato_App_Image_Result($image));
        }
        return $collection;
    }
}