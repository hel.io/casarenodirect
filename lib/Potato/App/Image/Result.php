<?php

class Potato_App_Image_Result
{
    protected $_optimized = '';
    protected $_original = '';
    protected $_result = '';
    protected $_status = '';

    public function __construct(stdClass $image)
    {
        $this->_optimized = $image->optimized;
        $this->_original = $image->original;
        $this->_result = $image->result;
        $this->_status = $image->status;
    }

    /**
     * @return bool
     */
    public function isOptimized()
    {
        return $this->_status == 0 ? true : false;
    }

    /**
     * @return string
     */
    public function getOptimizedUrl()
    {
        return $this->_optimized;
    }

    /**
     * @return string
     */
    public function getOriginalUrl()
    {
        return $this->_original;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->_result;
    }
}