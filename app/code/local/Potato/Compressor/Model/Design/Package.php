<?php

class Potato_Compressor_Model_Design_Package extends Mage_Core_Model_Design_Package
{
    /**
     * Prepare url for css replacement
     *
     * @param string $uri
     * @return string
     */
    protected function _prepareUrl($uri)
    {
        $uri = parent::_prepareUrl($uri);
        $uri = str_replace("'", '', $uri);
        $uri = str_replace('"', '', $uri);
        $uri = str_replace('http:','', $uri);
        $uri = str_replace('https:','', $uri);
        return $uri;
    }

    /**
     * Before merge css callback function
     *
     * @param string $file
     * @param string $contents
     * @return string
     */
    public function beforeMergeCss($file, $contents)
    {
        $this->_setCallbackFileDir($file);

        $cssImport = '/@import\\s+([\'"])(.*?)[\'"]/';
        $contents = preg_replace_callback($cssImport, array($this, '_cssMergerImportCallback'), $contents);

        /**
         * parent $cssUrl = '/url\\(\\s*(?!data:)([^\\)\\s]+)\\s*\\)?/';
         * bug with filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'>
         * result filter: url("http://example.com/./data:image/svg+xml;utf8,<sv")xmlns='http://www.w3.org/2000/svg'>
         */
        $cssUrl = '/url\\(\\s*(?![\"\']?data:)([^)]+)\\)/';
        $contents = preg_replace_callback($cssUrl, array($this, '_cssMergerUrlCallback'), $contents);

        return $contents;
    }

    public function getMergedJsUrl($files)
    {
        $targetFilename = $this->_getMergedFileName($files, 'js');
        $filePath = Potato_Compressor_Helper_Data::MAIN_FOLDER . DS . Mage::app()->getStore()->getId() . DS . 'js';
        $targetDir = $this->_initMergerDir($filePath, false, Potato_Compressor_Helper_Config::getJsCacheDir());
        if (!$targetDir) {
            return '';
        }
        $mergeFilesResult = true;
        if (!file_exists($targetDir . DS . $targetFilename)) {
            $mergeFilesResult = $this->_mergeFiles($files, $targetDir . DS . $targetFilename, false,
                array($this, 'beforeMergeJs'), 'js'
            );
        }
        if ($mergeFilesResult) {
            $filePath = str_replace('\\', '/', $filePath);
            return Mage::getBaseUrl(Potato_Compressor_Helper_Config::getJsCacheDir(), Mage::app()->getRequest()->isSecure())
                . $filePath . '/' . $targetFilename
            ;
        }
        return '';
    }

    protected function _getMergedFileName($files, $fileExt, $hashPostfix = '')
    {
        $result = '';
        foreach ($files as $file) {
            if (!file_exists($file)) {
                continue;
            }
            $contents = file_get_contents($file) . "\n";
            $result .= $contents;
        }
        return md5($result . $hashPostfix) . '.' . $fileExt;
    }

    /**
     * @param array  $files
     *
     * @return string
     */
    public function getMergedCssUrl($files)
    {
        // secure or unsecure
        $isSecure = Mage::app()->getRequest()->isSecure();
        $mergerDir = $isSecure ? 'css_secure' : 'css';
        $filePath = Potato_Compressor_Helper_Data::MAIN_FOLDER . DS . Mage::app()->getStore()->getId() . DS . $mergerDir;
        $targetDir = $this->_initMergerDir($filePath, false, Potato_Compressor_Helper_Config::getCssCacheDir());
        if (!$targetDir) {
            return '';
        }

        // base hostname & port
        $baseMediaUrl = Mage::getBaseUrl(Potato_Compressor_Helper_Config::getCssCacheDir(), $isSecure);
        $hostname = parse_url($baseMediaUrl, PHP_URL_HOST);
        $port = parse_url($baseMediaUrl, PHP_URL_PORT);
        if (false === $port) {
            $port = $isSecure ? 443 : 80;
        }

        // merge into target file
        $targetFilename = $this->_getMergedFileName($files, 'css', "|{$hostname}|{$port}");
        $mergeFilesResult = true;
        if (!file_exists($targetDir . DS . $targetFilename)) {
            $mergeFilesResult = $this->_mergeFiles(
                $files, $targetDir . DS . $targetFilename,
                false,
                array($this, 'beforeMergeCss'),
                'css'
            );
        }
        if ($mergeFilesResult) {
            $filePath = str_replace('\\', '/', $filePath);
            return $baseMediaUrl . $filePath . '/' . $targetFilename;
        }
        return '';
    }

    protected function _mergeFiles(array $srcFiles, $targetFile = false,
        $mustMerge = false, $beforeMergeCallback = null, $extensionsFilter = array())
    {
        $srcFiles = $this->_parseFilePath($srcFiles);
        $result = Mage::helper('core')->mergeFiles(
            $srcFiles,
            false,
            $mustMerge,
            $beforeMergeCallback,
            $extensionsFilter
        );
        $pathInfo = pathinfo($targetFile);
        if ($extensionsFilter == Potato_Compressor_Model_Compressor_Css::FILE_EXTENSION &&
            Potato_Compressor_Helper_Config::canCssCompression()
        ) {
            try {
                $result = Mage::getSingleton('po_compressor/compressor_css')->compression($result);
            } catch (Exception $e) {
                Mage::log($e->getMessage() . ' ' . $result, 1, 'po_cmp_exception.log', true);
            }
            if (Potato_Compressor_Helper_Config::canCssGzip()) {
                Potato_Compressor_Helper_Data::createHtaccessFile($pathInfo['dirname']);
            }
        } elseif ($extensionsFilter == Potato_Compressor_Model_Compressor_Js::FILE_EXTENSION &&
            Potato_Compressor_Helper_Config::canJsCompression()
        ) {
            try {
                $result = Mage::getSingleton('po_compressor/compressor_js')->compression($result);
            } catch (Exception $e) {
                Mage::log($e->getMessage() . ' ' . $result, 1, 'po_cmp_exception.log', true);
            }
            if (Potato_Compressor_Helper_Config::canJsGzip()) {
                Potato_Compressor_Helper_Data::createHtaccessFile($pathInfo['dirname']);
            }
        }
        file_put_contents($targetFile, $result, LOCK_EX);
        return true;
    }

    public function beforeMergeJs($file, $contents)
    {
        $contents = rtrim($contents, ';') . ';';
        return $contents;
    }

    protected function _parseFilePath($srcFiles)
    {
        $result = array();
        foreach ($srcFiles as $filePath) {
            $filePath = explode('?', $filePath);
            $result[] = $filePath[0];
        }
        return $result;
    }

    /**
     * Make sure merger dir exists and writeable
     * Also can clean it up
     *
     * @param string $dirRelativeName
     * @param bool $cleanup
     * @param string $type
     * @return bool
     */
    protected function _initMergerDir($dirRelativeName, $cleanup = false, $type = 'media')
    {
        $mediaDir = Potato_Compressor_Helper_Data::getBaseDir($type);
        try {
            $dir = Potato_Compressor_Helper_Data::getBaseDir($type) . DS . $dirRelativeName;
            if ($cleanup) {
                Varien_Io_File::rmdirRecursive($dir);
                Mage::helper('core/file_storage_database')->deleteFolder($dir);
            }
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            return is_writeable($dir) ? $dir : false;
        } catch (Exception $e) {
            Mage::logException($e);
        }
        return false;
    }
}