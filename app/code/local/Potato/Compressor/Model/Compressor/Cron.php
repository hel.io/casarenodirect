<?php

class Potato_Compressor_Model_Compressor_Cron
{
    const PROCESS_STEP = 25;

    /**
     * start image optimization
     *
     * @return $this
     */
    public function process()
    {
        if (!Potato_Compressor_Helper_Config::isEnabled() ||
            !Potato_Compressor_Helper_Config::isImageCronEnabled()
        ) {
            return $this;
        }

        $imageCollection = $this->_getImageGalleryFiles();
        $counter = 0;
        foreach ($imageCollection as $path) {
            try {
                $imageModel = Mage::getModel('po_compressor/image')->loadByPath($path);
                if ($imageModel->getId() &&
                    $imageModel->getStatus() == Potato_Compressor_Model_Compressor_Image::STATUS_OPTIMIZED
                ) {
                   continue;
                }
                $imageModel
                    ->setPath($path)
                    ->optimize()
                ;
                $counter++;
                if ($counter == self::PROCESS_STEP) {
                    break;
                }
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        Potato_Compressor_Helper_Data::sendServiceImages();
        return $this;
    }

    public function update()
    {
        if (!Potato_Compressor_Helper_Config::isEnabled() ||
            !Potato_Compressor_Helper_Config::isImageCronEnabled()
        ) {
            return $this;
        }
        $collection = Mage::getModel('po_compressor/image')->getCollection();
        foreach ($collection as $image) {
            if (file_exists($image->getPath()) && @filemtime($image->getPath()) != $image->getFiletime()) {
                try {
                    $image->delete();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
        return $this;
    }

    /**
     * get images collection
     *
     * @return mixed
     */
    protected function _getImageGalleryFiles()
    {
        return Mage::helper('po_compressor')->getImageGalleryFiles();
    }
}