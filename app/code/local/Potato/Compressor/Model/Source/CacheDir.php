<?php

class Potato_Compressor_Model_Source_CacheDir
{
    const MEDIA_VALUE = 'media';
    const SKIN_VALUE  = 'skin';
    const JS_VALUE    = 'js';

    const MEDIA_LABEL = 'Media';
    const SKIN_LABEL  = 'Skin';
    const JS_LABEL    = 'Js';

    public function toOptionArray()
    {
        return array(
            self::MEDIA_VALUE => Mage::helper('po_compressor')->__(self::MEDIA_LABEL),
            self::SKIN_VALUE  => Mage::helper('po_compressor')->__(self::SKIN_LABEL),
            self::JS_VALUE    => Mage::helper('po_compressor')->__(self::JS_LABEL),
        );
    }
}