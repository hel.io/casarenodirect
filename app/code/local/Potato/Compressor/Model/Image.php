<?php

class Potato_Compressor_Model_Image extends Mage_Core_Model_Abstract
{
    const TYPE_UNSUPPORTED = 0;
    const TYPE_GIF = 1;
    const TYPE_JPG = 2;
    const TYPE_PNG = 3;

    public function _construct()
    {
        parent::_construct();
        $this->_init('po_compressor/image');
    }

    public function loadByPath($path)
    {
        return $this->load($path , 'path');
    }

    public function optimize()
    {
        return Mage::getSingleton('po_compressor/compressor_image')->optimizeImage($this);
    }

    public function getType()
    {
        if (!$this->getPath() || !file_exists($this->getPath())) {
            return self::TYPE_UNSUPPORTED;
        }
        $info = @getimagesize($this->getPath());
        if ($info[2]) {
            return $info[2];
        }
        $ext = pathinfo($this->getPath(), PATHINFO_EXTENSION);
        switch ($ext) {
            case 'gif':
                return self::TYPE_GIF;
            case 'jpeg':
            case 'jpg':
                return self::TYPE_JPG;
            case 'png':
                return self::TYPE_PNG;
        }
        return self::TYPE_UNSUPPORTED;
    }
}