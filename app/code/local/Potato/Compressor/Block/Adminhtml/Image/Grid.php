<?php

class Potato_Compressor_Block_Adminhtml_Image_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('po_compressorImageGrid');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('po_compressor/image')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            array(
                'header' => $this->__('ID'),
                'index'  => 'id',
                'type'   => 'number',
                'width'  => 10
            )
        );
        $this->addColumn(
            'path',
            array(
                'header' => $this->__('Path'),
                'width'  => 250,
                'type'   => 'text',
                'index'  => 'path',
                'sortable' => false
            )
        );
        $this->addColumn(
            'status',
            array(
                'header' => $this->__('Status'),
                'width'  => 50,
                'type'   => 'options',
                'options'  => array(
                    Potato_Compressor_Model_Compressor_Image::STATUS_ERROR => $this->__('Error'),
                    Potato_Compressor_Model_Compressor_Image::STATUS_OPTIMIZED => $this->__('Optimized'),
                    Potato_Compressor_Model_Compressor_Image::STATUS_WAITING => $this->__('In Progress')
                ),
                'index'        => 'status',
                'sortable'     => true,
                'frame_callback' => array($this, 'decorateStatus')
            )
        );
        $this->addColumn(
            'result',
            array(
                'header'   => $this->__('Optimization Result'),
                'align'    => 'left',
                'index'    => 'result',
                'truncate' => 300,
                'escape'   => true,
                'sortable' => false,
                'filter'   => false,
                'type'     => 'text'
            )
        );
        return parent::_prepareColumns();
    }

    /**
     * Decorate status column values
     *
     * @param $value
     * @param $row
     * @param $column
     * @param $isExport
     *
     * @return string
     */
    public function decorateStatus($value, $row, $column, $isExport)
    {
        $result = '<span class="grid-severity-critical"><span>' . $value . '</span></span>';
        if ($row->getStatus() == Potato_Compressor_Model_Compressor_Image::STATUS_OPTIMIZED) {
            $result = '<span class="grid-severity-notice"><span>' . $value . '</span></span>';
        }
        if ($row->getStatus() == Potato_Compressor_Model_Compressor_Image::STATUS_WAITING) {
            $result = '<span class="grid-severity-major"><span>' . $value . '</span></span>';
        }
        return $result;
    }

    public function getRowUrl($row)
    {
        return '';
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem(
            'optimization',
            array(
                'label'   => $this->__('Optimize'),
                'url'     => $this->getUrl('adminhtml/potato_compressor_image/optimize'),
            )
        );
    }

    protected function _toHtml()
    {
        return parent::_toHtml() . $this->_getBottomHtml();
    }

    protected function _getBottomHtml()
    {
        return '<script type="text/javascript">var optimizationProgressBar = new AjaxRequestProgressBar("' . $this->getUrl('adminhtml/potato_compressor_index/optimization', array('force' => 1)) . '", $("po_compressorImageGrid_massaction-form"), function(){optimizationProgressBar.hideMask()}.bind(optimizationProgressBar));</script>';
    }
}