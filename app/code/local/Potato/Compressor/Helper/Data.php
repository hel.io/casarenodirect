<?php

class Potato_Compressor_Helper_Data extends Mage_Core_Helper_Abstract
{
    const MAIN_FOLDER = 'po_compressor';
    const GALLERY_CACHE_ID = 'po_compressor_GALLERY_CACHE_ID';
    const GALLERY_CACHE_LIFETIME = 1800;
    const SERVICE_IMAGES_DATA_NAME = 'service_images';
    const SERVICE_IMAGES_TRANSFER_LIMIT = 20;

    /**
     * @return string
     */
    public function getRootCachePath($type='media')
    {
        return self::getBaseDir($type) . DS. self::MAIN_FOLDER;
    }

    static function getBaseDir($type)
    {
        if ($type == Potato_Compressor_Model_Source_CacheDir::JS_VALUE) {
            return Mage::getBaseDir() . DS . $type;
        }
        return Mage::getBaseDir($type);
    }

    /**
     * @return string
     */
    public function getRootCacheUrl($type='media')
    {
        return Mage::getBaseUrl($type) . self::MAIN_FOLDER;
    }

    /**
     * @return $this
     */
    public function clearCache()
    {
        $this
            ->_removeFolder($this->getRootCachePath(Potato_Compressor_Helper_Config::getCssCacheDir()))
            ->_removeFolder($this->getRootCachePath(Potato_Compressor_Helper_Config::getJsCacheDir()))
        ;
        return $this;
    }

    /**
     * @param $dirPath
     *
     * @return bool
     */
    protected function _removeFolder($dirPath)
    {
        Varien_Io_File::rmdirRecursive($dirPath);
        return $this;
    }

    /**
     * @return array
     */
    public function getImageGalleryFiles()
    {
        $images = $this->_loadImageGalleryCache();
        if (!is_array($images) || empty($images)) {
            $images = array_merge($this->_getImagesFromDir(Mage::getBaseDir('media')),
                $this->_getImagesFromDir(self::getSkinDir())
            );
            $optimizedImagesHashes = Mage::getModel('po_compressor/image')->getCollection()->toOptionHash();
            $images = array_diff($images, $optimizedImagesHashes);
            $this->_saveImageGalleryCache($images);
        }
        return $images;
    }

    /**
     * @param $images
     *
     * @return bool
     */
    protected function _saveImageGalleryCache($images)
    {
        Mage::app()->saveCache(@serialize($images), self::GALLERY_CACHE_ID, array(), self::GALLERY_CACHE_LIFETIME);
        return true;
    }

    /**
     * @return bool|mixed
     */
    protected function _loadImageGalleryCache()
    {
        $data = Mage::app()->loadCache(self::GALLERY_CACHE_ID);
        if ($data) {
            return unserialize($data);
        }
        return false;
    }

    /**
     * @return $this
     */
    public function removeImageGalleryCache()
    {
        Mage::app()->removeCache(self::GALLERY_CACHE_ID);
        return $this;
    }

    /**
     * @return string
     */
    static function getSkinDir()
    {
        return Mage::getBaseDir('skin') . DS . 'frontend';
    }

    /**
     * @param $dirPath
     *
     * @return array
     */
    protected function _getImagesFromDir($dirPath)
    {
        $files = $this->_fastSearch($dirPath);
        if ($files && !empty($files)) {
            return $files;
        }
        $files = $this->_recursiveSearch($dirPath);
        $_result = array();
        foreach($files as $index => $object) {
            $filePath = $index;
            if (
                strpos($filePath, Potato_Compressor_Model_Compressor_Image::MEDIA_ORIGINAL_FOLDER_NAME) !== False ||
                strpos($filePath, Potato_Compressor_Model_Compressor_Image::SKIN_ORIGINAL_FOLDER_NAME) !== False ||
                strpos($filePath, '/tmp/') !== False ||
                strpos($filePath, '/.thumbs/') !== False
            ) {
                continue;
            }
            array_push($_result, $filePath);
        }
        return $_result;
    }

    protected function _recursiveSearch($dirPath)
    {
        $dirIterator = new RecursiveDirectoryIterator($dirPath);
        $iterator = new RecursiveIteratorIterator($dirIterator);
        $regex = new RegexIterator($iterator, '/^.+(.jpe?g|.png|.gif)$/i', RecursiveRegexIterator::GET_MATCH);
        return $regex;
    }

    protected function _fastSearch($dirPath)
    {
        if(!function_exists('exec')) {
            return false;
        }
        $_result = array();
        $_status = array();
        exec('find ' . $dirPath . ' -type f \( -iname \*.jpg -o -iname \*.png -o -iname \*.gif -o -iname \*.jpeg \) | egrep -v "'
            . Potato_Compressor_Model_Compressor_Image::MEDIA_ORIGINAL_FOLDER_NAME . '|'
            . Potato_Compressor_Model_Compressor_Image::SKIN_ORIGINAL_FOLDER_NAME . '|tmp|.thumbs" 2>&1', $_result, $_status
        );
        if ($_status == 0) {
            return $_result;
        }
        return false;
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function isIgnoredImage($path)
    {
        foreach (Potato_Compressor_Helper_Config::getSkippedImages() as $regexp) {
            if (preg_match('/' . preg_quote($regexp, '/') . '/', $path)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $folderPath
     *
     * @return mixed
     */
    static function prepareFolder($folderPath)
    {
        return Mage::getConfig()->createDirIfNotExists($folderPath);
    }

    /**
     * @param $path
     *
     * @return bool
     */
    static function createHtaccessFile($path)
    {
        $content = "<ifmodule mod_deflate.c>\n"
            . "AddOutputFilterByType DEFLATE text/html text/plain text/css application/json\n"
            . "AddOutputFilterByType DEFLATE application/javascript\n"
            . "AddOutputFilterByType DEFLATE text/xml application/xml text/x-component\n"
            . "AddOutputFilterByType DEFLATE application/xhtml+xml application/rss+xml application/atom+xml\n"
            . "AddOutputFilterByType DEFLATE image/svg+xml application/vnd.ms-fontobject application/x-font-ttf font/opentype\n"
            . "SetOutputFilter DEFLATE\n"
            . "</ifmodule>\n"
            . "<ifmodule mod_headers.c>\n"
            . "<FilesMatch '\.(css|js|jpe?g|png|gif)$'>\n"
            . "Header set Cache-Control 'max-age=2592000, public'\n"
            . "</FilesMatch>\n"
            . "</ifmodule>\n"
            . "<ifmodule mod_expires.c>\n"
            . "ExpiresActive On\n"
            . "ExpiresByType text/css 'access plus 30 days'\n"
            . "ExpiresByType text/javascript 'access plus 30 days'\n"
            . "ExpiresByType application/x-javascript 'access plus 30 days'\n"
            . "ExpiresByType image/jpeg 'access plus 30 days'\n"
            . "ExpiresByType image/png 'access plus 30 days'\n"
            . "ExpiresByType image/gif 'access plus 30 days'\n"
            . "</ifmodule>\n"
        ;
        file_put_contents($path . DS . '.htaccess', $content);
        return true;
    }

    /**
     * @param $image
     *
     * @return bool
     */
    static function isMediaImage($image)
    {
        return strpos($image, Mage::getBaseUrl('media', Mage::app()->getRequest()->isSecure())) !== FALSE;
    }

    /**
     * @param $image
     *
     * @return bool
     */
    static function isSkinImage($image)
    {
        return strpos($image, Mage::getBaseUrl('skin', Mage::app()->getRequest()->isSecure())) !== FALSE;
    }

    /**
     * @param $image
     *
     * @return bool
     */
    static function isSkinDir($image)
    {
        return strpos($image, Mage::getBaseDir('skin')) !== FALSE;
    }

    /**
     * @param $image
     *
     * @return $this
     */
    public function restoreImage($image)
    {
        $backupImg = $this->getBackupImagePath($image);
        if ($backupImg && is_file($backupImg) && file_exists($backupImg)) {
            $content = file_get_contents($backupImg);
            file_put_contents($image, $content);
        }
        return $this;
    }

    /**
     * @param $image
     *
     * @return bool|string
     */
    public function getBackupImagePath($image)
    {
        $path = false;
        if (strpos($image, Mage::getBaseDir('media')) !== FALSE) {
            $path = rtrim(Mage::getBaseDir('media'), DS)
                . DS . Potato_Compressor_Model_Compressor_Image::MEDIA_ORIGINAL_FOLDER_NAME
                . DS . trim(str_replace(Mage::getBaseDir('media'), '', $image), DS)
            ;
        }
        if (!$path && strpos($image, Potato_Compressor_Helper_Data::getSkinDir()) !== FALSE) {
            $path = rtrim(Potato_Compressor_Helper_Data::getSkinDir(), DS)
                . DS . Potato_Compressor_Model_Compressor_Image::SKIN_ORIGINAL_FOLDER_NAME
                . DS . trim(str_replace(Potato_Compressor_Helper_Data::getSkinDir(), '', $image), DS)
            ;
        }
        return $path;
    }

    public function encode($value)
    {
        //!do not use urlEncode function - grid mass actions will be work incorrect
        return base64_encode($value);
    }

    public function decode($value)
    {
        return base64_decode($value);
    }

    static function canScaleImage($imageSrc)
    {
        if (in_array($imageSrc, Potato_Compressor_Helper_Config::getScalingImages())) {
            return true;
        }
        return false;
    }

    static function registerServiceImage($image)
    {
        $registeredData = array();
        if (Mage::getSingleton('core/session')->getData(self::SERVICE_IMAGES_DATA_NAME)) {
            $registeredData = Mage::getSingleton('core/session')->getData(self::SERVICE_IMAGES_DATA_NAME);
        }
        array_push($registeredData, $image);
        Mage::getSingleton('core/session')->setData(self::SERVICE_IMAGES_DATA_NAME, $registeredData);
        if (count($registeredData) > self::SERVICE_IMAGES_TRANSFER_LIMIT) {
            self::sendServiceImages();
        }
        return true;
    }

    static function sendServiceImages()
    {
        $images = array();
        if (!is_array(self::getServiceImage())) {
            return true;
        }
        foreach (self::getServiceImage() as $image) {
            $image = str_replace(Mage::getBaseDir('media'), trim(Mage::getBaseUrl('media'), "/"), $image);
            if (self::isSkinDir($image)) {
                $image = str_replace(Mage::getBaseDir('skin'), trim(Mage::getBaseUrl('skin'), "/"), $image);
            }
            array_push($images, $image);
        }
        if (!empty($images)) {
            try {
                Potato_App_ImageOptimization::process(Mage::getUrl('po_compressor/image/index'), $images);
            } catch (Exception $e) {
                Mage::logException($e);
            }
            Mage::getSingleton('core/session')->setData(self::SERVICE_IMAGES_DATA_NAME, null);
        }
        return true;
    }

    static function getServiceImage()
    {
        return Mage::getSingleton('core/session')->getData(self::SERVICE_IMAGES_DATA_NAME);
    }
}