<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    TRUNCATE TABLE {$this->getTable('po_compressor/image')};
    ALTER TABLE {$this->getTable('po_compressor/image')} ADD `path` VARCHAR(255) NOT NULL DEFAULT '';
    ALTER TABLE {$this->getTable('po_compressor/image')} ADD `filetime` INT(10) UNSIGNED NOT NULL DEFAULT 0;
    ALTER TABLE {$this->getTable('po_compressor/image')} ADD `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 2;
    ALTER TABLE {$this->getTable('po_compressor/image')} ADD `result` TEXT NOT NULL DEFAULT '';
");
$installer->endSetup();