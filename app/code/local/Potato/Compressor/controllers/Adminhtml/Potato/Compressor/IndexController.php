<?php

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'System' . DS . 'ConfigController.php';
class Potato_Compressor_Adminhtml_Potato_Compressor_IndexController extends Mage_Adminhtml_System_ConfigController
{
    const CACHE_LIFETIME = 86400;

    public function optimizationAction()
    {
        $session = Mage::getSingleton('adminhtml/session');
        $response = array('progress' => 0);

        $compressorData = $this->_loadCache();
        if (!$compressorData) {
            $compressorData = new Varien_Object;

            $ids = $this->getRequest()->getParam('id', false);
            if ($ids) {
                $collection = Mage::getModel('po_compressor/image')->getCollection();
                $collection->addFieldToFilter('id', array("in" => array($ids)));
                $compressorData->setImageGallery($collection->toOptionHash());
            } else {
                $compressorData->setImageGallery(Mage::helper('po_compressor')->getImageGalleryFiles());
            }

            $compressorData->setImageGalleryCount(count($compressorData->getImageGallery()));
            $this->_saveCache($compressorData);
        } else {
            $imageGallery = $compressorData->getImageGallery();
            $counter = 0;
            foreach ($imageGallery as $key => $path) {
                try {
                    $imageModel = Mage::getModel('po_compressor/image')->loadByPath($path);
                    if (!$imageModel->getId() ||
                        $imageModel->getStatus() != Potato_Compressor_Model_Compressor_Image::STATUS_OPTIMIZED ||
                        $this->getRequest()->getParam('force', false)
                    ) {
                        $imageModel
                            ->setPath($path)
                            ->optimize()
                        ;
                    }
                } catch (Exception $e) {
                    $session->addException($e,
                        Mage::helper('adminhtml')->__('An error occurred while saving this configuration:') . ' '
                        . $e->getMessage())
                    ;
                    $response['reload'] = 1;
                    $response = Mage::helper('core')->jsonEncode($response);
                    $this->getResponse()->setBody($response);
                    return $this;
                }
                $counter++;
                unset($imageGallery[$key]);
                if ($counter == 5) {
                    break;
                }
            }
            $compressorData->setImageGallery($imageGallery);
            $this->_saveCache($compressorData);
            $response['progress'] = 100 - floor((count($compressorData->getImageGallery()) / $compressorData->getImageGalleryCount()) * 100);
            $response['total'] = $compressorData->getImageGalleryCount();
            $response['current'] = $compressorData->getImageGalleryCount() - count($compressorData->getImageGallery());
            if (count($compressorData->getImageGallery()) == 0) {
                Potato_Compressor_Helper_Data::sendServiceImages();
                $this->_removeCache();
                $session->addSuccess(Mage::helper('po_compressor')->__('Image Optimization complete.'));
                $response['reload'] = 1;
            }
        }
        $response = Mage::helper('core')->jsonEncode($response);
        $this->getResponse()->setBody($response);
        return $this;
    }

    protected function _removeCache()
    {
        Mage::app()->removeCache(Mage::getSingleton('adminhtml/session')->getEncryptedSessionId());
        return $this;
    }

    public function flushAction()
    {
        try {
            Mage::getResourceModel('po_compressor/image')->truncate();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('po_compressor')->__('The Compressor Images Cache has been cleaned.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirectReferer();
    }

    protected function _saveCache($varienObject)
    {
        Mage::app()->saveCache(@serialize($varienObject->getData()), $this->_getCacheId(), array(), self::CACHE_LIFETIME);
        return true;
    }

    protected function _loadCache()
    {
        $data = Mage::app()->loadCache($this->_getCacheId());
        if ($data) {
            return new Varien_Object(unserialize($data));
        }
        return false;
    }

    protected function _getCacheId()
    {
        return Mage::getSingleton('adminhtml/session')->getEncryptedSessionId()
            . implode(',', $this->getRequest()->getParams())
        ;
    }
}