<?php

class Potato_Compressor_ImageController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        try {
            $images = Potato_App_ImageOptimization::getOptimizedImages();
        } catch (Exception $e) {
            //Mage::logException($e);
            return $this->norouteAction();
        }

        /** @var  $image Potato_App_Image_Result*/
        foreach ($images as $image) {
            $imagePath = str_replace(
                trim(Mage::getBaseUrl('media'), '/'),
                Mage::getBaseDir('media'),
                $image->getOriginalUrl()
            );
            if (Potato_Compressor_Helper_Data::isSkinImage($image->getOriginalUrl())) {
                $imagePath = str_replace(
                    trim(Mage::getBaseUrl('skin'), '/'),
                    Mage::getBaseDir('skin'),
                    $image->getOriginalUrl()
                );
            }
            $optimizedImage = @file_get_contents($image->getOptimizedUrl());
            if (!$optimizedImage || !file_exists($imagePath)) {
                continue;
            }
            file_put_contents($imagePath, $optimizedImage);
            $optimizedImage = Mage::getModel('po_compressor/image')->loadByPath($imagePath);
            $optimizedImage->setStatus(Potato_Compressor_Model_Compressor_Image::STATUS_OPTIMIZED);
            if (!$image->isOptimized()) {
                $optimizedImage->setStatus(Potato_Compressor_Model_Compressor_Image::STATUS_ERROR);
            }
            $optimizedImage
                ->setPath()
                ->setResult($image->getResult())
                ->setFiletime(@filemtime($imagePath))
                ->save()
            ;
        }
        Mage::helper('po_compressor')->removeImageGalleryCache();
    }
}