<?php 
class Magebuzz_Sharecart_Model_Observer extends Mage_Sales_Model_Observer{
  public function checkProductAvailability($observer){
    if (!Mage::helper('sales')->getIsQuotePersistent()){
     return $this;
    }
    $quote = $observer->getEvent()->getQuote();
    $currentId = Mage::app()->getWebsite()->getId();
     
    $messages = array();
    
    foreach ($quote->getAllItems() as $item){   
     $product = $item->getProduct();
     if (!in_array($currentId, $product->getWebsiteIds())){
      $quote->removeItem($item->getId());
      $messages[] = Mage::helper('catalog')->__('Product %s is not available on website %s', $item->getName(), Mage::app()->getWebsite()->getName());
     }
    }
    foreach ($messages as $message){
     Mage::getSingleton('checkout/session')->addError($message);
    }
    return $this;
 }
}