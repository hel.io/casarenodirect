<?php 
class Magebuzz_Sharecart_Model_Checkout_Session extends Mage_Checkout_Model_Session {
 protected function _getQuoteIdKey()
    {
     if (Mage::helper('sharecart')->sharecartIsActive ()){//if behavior is not disabled
         return 'quote_id';
     }
     return parent::_getQuoteIdKey();
    }
}