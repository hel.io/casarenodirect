<?php
ini_set('max_execution_time', 0);

class Netprofile_Stock_Model_Stock extends Mage_Core_Model_Abstract
{
	private $url = '';
	private $download_dir = '';
	private $download_file = '';
	
	function __construct()
	{
		$url = Mage::getStoreConfig('netprofilestock/general/csv_url') . 'WebInventory.csv';
		$this->url = $url;
		$this->download_dir = Mage::getStoreConfig('netprofilestock/general/csv_url');
		$this->download_file = $this->download_dir.'1WebInventory.csv';
	}
	
    public function rebuildStock()
    {
    	$re_index_var = FALSE;
		$this->getFile();
		
		$products = $this->proccessCsv();
//       var_dump($products);
//       die();
		
		foreach($products as $key => $product)
		{
			$re_index_var = TRUE;
			$this->updateStock($key, $product);
		}
		
		if($re_index_var == TRUE){
			$this->reindexAll();	
		}

		$this->removeFile();        
        return true;
    }
	
	private function reindexAll()
	{
		$indexCollection = Mage::getModel('index/process')->getCollection();
		foreach ($indexCollection as $index) {
			$index->reindexAll();
		}
	}
	
	private function getFile()
	{
		$file_url = $this->url;
		$file_path = $this->download_dir.'1WebInventory.csv';
		
		if((!isset($file_url) || !isset($file_path))) return false;

		if (!file_exists($file_path))
		{
			touch($file_path);
		}
		
		if (!is_writable($file_path)) {
			echo dirname($file_path) . ' must writable!!!';
			return false;
		} else {
			if(!file_put_contents($file_path, fopen($file_url, 'r')))
			{
				return true;
			}
		}
	}
    
	private function proccessCsv()
	{
		$file_path = $this->download_file;
		
		if(!$file_path)
		{
			return false;
		}
		
		$delimiter = ',';
		$products = array();
		
		if (($handle = fopen($file_path = $this->download_dir . 'WebInventory.csv', 'r')) !== FALSE)
		{
			$i = 0;
			while (($row = fgetcsv($handle)) !== FALSE)
			{//print_r($row); die;
				if(++$i < 3) continue; 
				//if($i > 30) die; 
				//var_dump($row);die;
				$products[str_replace(' ', '', $row[0])] = str_replace(' ', '', $row[4]);
			}
			//die;
			fclose($handle);
			//var_dump($handle);
			//die;
		}		
		//die();

		return $products;
	}
	
	function updateStock($sku, $qty)
	{
		//$stock_item = Mage::getModel('catalog/product')->loadBySku($sku);

		$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
		
		if($product)
		{
			/*
			if(!$qty)
			{
				$stock_item->setData('manage_stock', 1); // should be 1 to make something out of stock
				$stock_item->setData('is_in_stock', 0); // is 0 or 1
			}else $stock_item->setData('is_in_stock', 1);
			*/

			if((int)$qty || $qty == '0')
			{
				try {
					$stock_item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
					$stock_item->setData('manage_stock', 1);
					$stock_item->setData('use_config_manage_stock', 0);
					$stock_item->setData('is_in_stock', 1);
					$stock_item->setData('stock_id', 1);
					$stock_item->setData('qty', $qty);
					$stock_item->setData('manage_stock', 0);
					$stock_item->save();
				} catch (Exception $e) {
					Mage::log($e);
				}
			}
		}

		return false;
	}

	private function removeFile(){

		$archive_dir = Mage::getBaseDir().DIRECTORY_SEPARATOR.'csvupload'.DIRECTORY_SEPARATOR.'inventory'.DIRECTORY_SEPARATOR;
		$archive_folder = 'archive';
		$file_url = $this->url;
		$file_path = $this->download_dir.'stock.csv';

		$url_array = explode('/',$file_url);
		$file_name =  $url_array[count($url_array)-1];

		$archive_filename = $file_name.'_'.date("Ymd").'_'.time().'.csv';

		if(!is_dir($archive_dir.'/'.$archive_folder)){
			mkdir($archive_dir.'/'.$archive_folder,0777);
		}

		if(!is_writable($archive_dir.'/'.$archive_folder)){
			chmod($archive_dir.'/'.$archive_folder, 0777);
		}
		copy($archive_dir.DIRECTORY_SEPARATOR.$file_name, $archive_dir.'/'.$archive_folder.'/'.$archive_filename);
		
		unlink($archive_dir.DIRECTORY_SEPARATOR.$file_name);
		unlink($file_path);
	}
	
}