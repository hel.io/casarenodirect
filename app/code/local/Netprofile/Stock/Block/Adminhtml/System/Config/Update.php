<?php

class Netprofile_Stock_Block_Adminhtml_System_Config_Update extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->_getUpdateButtonHtml();
    }

    protected function _getUpdateButtonHtml()
    {
        $url = Mage::helper('adminhtml')->getUrl('netprofilestock/adminhtml_stock/update/');
        $buttonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setLabel($this->__('Update Stock'))
            ->setOnClick("start()")
            ->toHtml();

        
        $js = "<script type='text/javascript'>     
            jQuery('body').append('<div class=\"ajax-loading\"><div>');
            
            function start(){
                var reloadurl = '". $this->getUrl('netprofilestock')."'
                new Ajax.Request(reloadurl, {
                    method: 'get',
                    onLoading: function (transport) {
                        jQuery('.ajax-loading').show();
                    },
                    onComplete: function(transport) {
                        jQuery('.ajax-loading').hide();
                        alert('Success');
                    }
                });
            }
        </script>";

		if (empty($css)) {
			$css = "";
		}
        return $buttonHtml . $css . $js;
    }

}