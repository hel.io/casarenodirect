<?php

class Netprofile_Stock_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $model = Mage::getModel('netprofilestock/stock');
            $model->rebuildStock();
        }
        return false;
    }
}