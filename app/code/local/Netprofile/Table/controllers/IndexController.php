<?php

class Netprofile_Table_IndexController extends Mage_Core_Controller_Front_Action
{
	private $homegroup = 2;
	
    public function indexAction()
	{
		$zip = $this->getRequest()->getParams('zip', 0);
		$flag = false;
		
		if($zip)
		{
			$model  = Mage::getModel('nettable/rate');
			$model_method = Mage::getModel('nettable/method');
			
			$store_id = $model->getNearestbyGroup($zip, $this->homegroup);
			$nearest_store = $model_method->load($store_id);
			$url = $nearest_store->getCms();
			$flag = true;
			/*$oUrlRewriteCollection = Mage::getModel('core/url_rewrite')
				->getCollection()
				->addFieldToFilter('target_path', $url);
			
			if (count($oUrlRewriteCollection) > 0) {
				$flag = true;
			}*/
		}

		if($flag) Mage::app()->getResponse()->setRedirect($url, 301)->sendResponse();
		else Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
    }
}