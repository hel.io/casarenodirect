<?php

class Netprofile_Table_Model_Config_Source_Ziptype extends Varien_Object
{
    public function toOptionArray()
    {
        $vals = array(
            '0' => Mage::helper('nettable')->__('Numeric'),
            '1'   => Mage::helper('nettable')->__('String'),
        );

        $options = array();
        foreach ($vals as $k => $v)
            $options[] = array(
                    'value' => $k,
                    'label' => $v
            );
        
        return $options;
    }
}