<?php

class Netprofile_Table_Model_Rate extends Mage_Core_Model_Abstract
{
    const MAX_LINE_LENGTH  = 2000;
    const COL_NUMS         = 16;
    const STORES_AMOUNT    = 1;
    const DISTANCE		   = 3959; //3959 - miles, 6371 - km
    const GOOGLE_API	   = "http://maps.google.com/maps/api/geocode/json";
    const COUNTRY_CODE	   = "CA";

	private $groups;
	private $nearest;
	
    public function _construct()
    {
        parent::_construct();
        $this->_init('nettable/rate');
		$this->groups = array(1 => 'Terminal Pickup', 2 => 'Pickup from store');
    }
    
    public function findBy($request, $collection)
    {
        if (!$request->getAllItems()) {
            return array();
        }

        if($collection->getSize() == 0)
        {
            return array();
        }
        
        $methodIds = array();        
        foreach ($collection as $method)
        {
           $methodIds[] = $method -> getMethodId();

        }
        // calculate price and weight
        $allowFreePromo = Mage::getStoreConfig('carriers/nettable/allow_promo');  
        $ignoreVirtual  = Mage::getStoreConfig('carriers/nettable/ignore_virtual');
        $fuel = Mage::getStoreConfig('carriers/nettable/fuel');

		if($fuel = (float)$fuel) 
		{
			$fuel = 1+$fuel/100;
		}else $fuel = false;

        $items = $request->getAllItems();
        $shippingTypes = array();
        $shippingTypes[] = 0;
		$taligate = 0;
		$additional_cost = 0;
		$store_id = Mage::app()->getStore()->getStoreId();
		
        foreach($items as $item)
        {
            // if attribute isn't load to product
            //$additional_cost = 0;
            $product = Mage::getModel('catalog/product')->load($item->getProduct()->getEntityId());
			$taigate_val = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product->getId(), 'tailgate', $store_id);
			$additional_cost_val = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product->getId(), 'additional_charge', $store_id);
            
            // echo 'Addition Cost Val :'.$additional_cost_val.'<br/>';

            if(empty($additional_cost_val) === FALSE && $additional_cost_val !=0 ):
                $additional_cost_val = $item->getQty() * $additional_cost_val;
                $additional_cost += $additional_cost_val;
            endif;
			
			if($taligate < $taigate_val) $taligate = $taigate_val;
			//if($additional_cost < $additional_cost_val) $additional_cost = $additional_cost_val;
			
            if ($product->getAmShippingType()){
                $shippingTypes[] = $product->getAmShippingType();                
            } else {
               $shippingTypes[] = 0; 
            }
        }
        $shippingTypes = array_unique($shippingTypes);
        $allCosts = array(); 
        
        $allRates = $this->getResourceCollection();
        $allRates->addMethodFilters($methodIds);
        $ratesTypes = array();
        
        foreach ($allRates as $singleRate){
            $ratesTypes[$singleRate->getMethodId()][] = $singleRate->getShippingType();    
        }
                
        $intersectTypes = array();
        $this->nearest = $this->getNearest($request->getDestPostcode());
		
        foreach ($ratesTypes as $key => $value){
            $intersectTypes[$key] = array_intersect($shippingTypes,$value);
            arsort($intersectTypes[$key]);
            $methodIds = array($key);
            $allTotals =  $this->calculateTotals($request, $ignoreVirtual, $allowFreePromo,'0');
            
            foreach ($intersectTypes[$key] as $shippingType){
                $totals = $this->calculateTotals($request, $ignoreVirtual, $allowFreePromo,$shippingType);
                if ($allTotals['qty'] > 0 && (!Mage::getStoreConfig('carriers/nettable/dont_split') || $allTotals['qty'] == $totals['qty'])) {

                    if ($shippingType == 0)
                        $totals = $allTotals;
                        
                    $allTotals['not_free_price'] -= $totals['not_free_price'];
                    $allTotals['not_free_weight'] -= $totals['not_free_weight'];
                    $allTotals['not_free_qty'] -= $totals['not_free_qty'];
                    $allTotals['qty'] -= $totals['qty'];
 
                    $allRates = $this->getResourceCollection();
                    $allRates->addAddressFilters($request);
                    $allRates->addTotalsFilters($totals,$shippingType);
                    $allRates->addMethodFilters($methodIds);
                    foreach($this->calculateCosts($allRates, $totals, $request,$shippingType/*, $taligate*/) as $key => $cost){
						if (!empty($allCosts[$key])){
                            $allCosts[$key] += $cost['cost']; 
							if($cost['taligate'])
							{
								$allCosts[$key] += $taligate;                    
								$allCosts[$key] += $additional_cost;  
							}
                        }  else {
							if($cost['cost'] != 0)
							{
								$allCosts[$key] = $cost['cost'];
								if($cost['taligate']) $allCosts[$key] += $taligate + $additional_cost;
							}else $allCosts[$key] = $cost['cost'];
                        }
						if($cost['taligate'] && $fuel) $allCosts[$key] = $allCosts[$key] * $fuel;
                    }                                
                }  
            }            
        }
        

        return $allCosts;
    }
    protected function getNearest($zip)
	{
		$latlng = $this->getLatLng($zip);
		$nearest = array();

		if($latlng)
		{
			$groups = $this->groups;
			foreach($groups as $key=>$group)
			{
				$lat = $latlng['lat'];
				$lng = $latlng['lng'];
				$resource = Mage::getSingleton('core/resource');
				$readConnection = $resource->getConnection('core_read');
				$query = "SELECT method_id, ( ".self::DISTANCE." * acos( cos( radians('".($lat)."') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('".($lng)."') ) + sin( radians('".($lat)."') ) * sin( radians( lat ) ) ) ) AS distance FROM net_table_method WHERE `is_active` = 1 AND `is_map` = 1 AND `group` = ".$key." HAVING distance < 25000000 ORDER BY distance LIMIT 0 , ".self::STORES_AMOUNT;
				$results = $readConnection->fetchAll($query);
				foreach($results as $result)
				{
					$nearest[] = (int)$result["method_id"];
				}
			}
		}
		return $nearest;
		
	}
	
	public function getNearestbyGroup($zip, $group)
	{
		$latlng = $this->getLatLng($zip);
		$nearest = array();

		if($latlng)
		{
			
			$lat = $latlng['lat'];
			$lng = $latlng['lng'];
			$resource = Mage::getSingleton('core/resource');
			$readConnection = $resource->getConnection('core_read');
			$query = "SELECT method_id, ( ".self::DISTANCE." * acos( cos( radians('".($lat)."') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('".($lng)."') ) + sin( radians('".($lat)."') ) * sin( radians( lat ) ) ) ) AS distance FROM net_table_method WHERE `is_active` = 1 AND `is_map` = 1 AND `group` = ".$group." HAVING distance < 25000000 ORDER BY distance LIMIT 0 , 1";
			$results = $readConnection->fetchAll($query);
			foreach($results as $result)
			{
				$nearest = (int)$result["method_id"];
			}
			
		}
		return $nearest;
		
	}
	
	private function getLatLng($zip)
	{
		$url = self::GOOGLE_API."?components=country:".self::COUNTRY_CODE."|postal_code:".str_replace (" ", "+", $zip)."&sensor=false";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		
		$latlng = array();
		if($response)
		{
			$response_a = json_decode($response);
            $latlng['lat'] = isset($response_a->results[0]->geometry->location->lat) ? $response_a->results[0]->geometry->location->lat : null;
            $latlng['lng'] = isset($response_a->results[0]->geometry->location->lng) ? $response_a->results[0]->geometry->location->lng : null;
		}

		return $latlng;
	}
	
    protected function calculateCosts($allRates, $totals, $request,$shippingType/*, $taligate=0*/)
    {
        $shippingFlatParams  =  array('country', 'state', 'city');
        $shippingRangeParams =  array('price', 'qty', 'weight');
        
        $minCounts = array();   // min empty values counts per method
        $results   = array();
		
		//$nearest_store = $this->getNearest($request->getDestPostcode());
		$nearest_store = $this->nearest;
		$methods_id = array();
		
        foreach ($allRates as $rate){
            
            $rate = $rate->getData();
			$model  = Mage::getModel('nettable/method')->load((int)$rate['method_id']);
			$taligate = $model->getData('taligate');
			
			if($nearest_store && $model->getData('is_map') == '1')
			{
				if (in_array((int)$rate['method_id'], $nearest_store)) {
					$methods_id[] = (int)$rate['method_id'];
				}else continue;
			}

            $emptyValuesCount = 0;

            if(empty($rate['shipping_type'])){
                $emptyValuesCount++;
            }
            
            foreach ($shippingFlatParams as $param){
                if (empty($rate[$param])){
                    $emptyValuesCount++;
                }                    
            }
            
            foreach ($shippingRangeParams as $param){
                if ((ceil($rate[$param . '_from'])== 0) && (ceil($rate[$param . '_to'])== 999999)) {
                    $emptyValuesCount++;
                }                   
            }

            if (empty($rate['zip_from']) && empty($rate['zip_to']) ){
                $emptyValuesCount++;
            } 

            if (!$totals['not_free_price'] && !$totals['not_free_qty'] && !$totals['not_free_weight']){
                $cost = 0;    
            } 
            else {
                $cost =  $rate['cost_base'] +  $totals['not_free_price'] * $rate['cost_percent'] / 100 + $totals['not_free_qty'] * $rate['cost_product'] + $totals['not_free_weight'] * $rate['cost_weight'];                
			}
            
			if($rate['min_price'] > $cost)
			{
				$cost = $rate['min_price'];
			}
            $id   = $rate['method_id'];
            if ((empty($minCounts[$id]) && empty($results[$id])) || ($minCounts[$id] > $emptyValuesCount) || (($minCounts[$id] == $emptyValuesCount) && ($cost > $results[$id]))){
                $minCounts[$id] = $emptyValuesCount;
				/*if($model->getData('taligate') == '1'){ //additional charge attribute?
					$results[$id] = $cost + $taligate;
				}else */
				$results[$id]['cost'] = $cost; 
				$results[$id]['taligate'] = $taligate;
            }
			
        } 

        return $results;
    }
    
    protected function calculateTotals($request, $ignoreVirtual, $allowFreePromo,$shippingType)
    { 
        $totals = $this->initTotals();

        $newItems = array();

        //reload child items 
        
        $isCalculateLater = array();
        
        foreach ($request->getAllItems() as $item) {
            // if attribute isn't load to product
            $product = Mage::getModel('catalog/product')->load($item->getProduct()->getEntityId());
            if (($product->getAmShippingType() != $shippingType) && ($shippingType != 0)) 
                continue;
           
           if ($item->getParentItemId())
            continue;

            if ($ignoreVirtual && $item->getProduct()->isVirtual())
                continue;


            
            if ($item->getHasChildren()) {
                 $qty = 0;
                 $notFreeQty =0;
                 $price = 0;
                 $weight = 0;
                foreach ($item->getChildren() as $child) {
          
                    $qty        +=  $child->getQty();
                    $notFreeQty += ($qty - $this->getFreeQty($child, $allowFreePromo));                    
                    $price  += $child->getPrice() * $child->getQty();
                    $weight += $child->getWeight() * $qty;
                    $totals['tax_amount']       += $child->getBaseTaxAmount() + $child->getBaseHiddenTaxAmount();
                    $totals['discount_amount']  += $child->getBaseDiscountAmount();  
                }
                
                if ($item->getProductType() == 'bundle'){
                    $qty        = $item->getQty();

                    if ($item->getProduct()->getWeightType() == 1){
                        $weight  = $item->getWeight();    
                    }
                    
                    if ($item->getProduct()->getPriceType() == 1){
                        $price   = $item->getPrice();    
                    }
                    
                    if ($item->getProduct()->getSkuType() == 1){
                        $totals['tax_amount']       += $item->getBaseTaxAmount() + $item->getBaseHiddenTaxAmount();
                        $totals['discount_amount']  += $item->getBaseDiscountAmount(); 
                    }
                                        
                    $notFreeQty = ($qty - $this->getFreeQty($item, $allowFreePromo));
                    $totals['qty']              += $qty;
                    $totals['not_free_qty']     += $notFreeQty; 
                    $totals['not_free_price'] += $price * $notFreeQty;
                    $totals['not_free_weight'] += $weight * $notFreeQty;  
                                                                             
                }
                
                if ($item->getProductType() == 'configurable'){
                    $qty     = $item->getQty();
                    $price   = $item->getPrice();
                    $weight  = $item->getWeight();
                    $notFreeQty = ($qty - $this->getFreeQty($item, $allowFreePromo));
                    $totals['qty']              += $qty;
                    $totals['not_free_qty']     += $notFreeQty; 
                    $totals['not_free_price'] += $price * $notFreeQty;
                    $totals['not_free_weight'] += $weight * $notFreeQty;
                    $totals['tax_amount']       += $item->getBaseTaxAmount() + $item->getBaseHiddenTaxAmount();
                    $totals['discount_amount']  += $item->getBaseDiscountAmount();                                                                                   
                } 
                                

            } else {
                $qty        = $item->getQty();
                $notFreeQty = ($qty - $this->getFreeQty($item, $allowFreePromo));
                $totals['not_free_price'] += $item->getBasePrice() * $notFreeQty;
                $totals['not_free_weight'] += $item->getWeight() * $notFreeQty;
                $totals['qty']              += $qty;
                $totals['not_free_qty']     += $notFreeQty;
                $totals['tax_amount']       += $item->getBaseTaxAmount() + $item->getBaseHiddenTaxAmount();
                $totals['discount_amount']  += $item->getBaseDiscountAmount();                
            }

                               
        } 
           
        // fix magento bug
        if ($totals['qty'] != $totals['not_free_qty']) 
            $request->setFreeShipping(false);   

        $afterDiscount = Mage::getStoreConfig('carriers/nettable/after_discount');
        $includingTax =  Mage::getStoreConfig('carriers/nettable/including_tax');
             
        if ($afterDiscount)
            $totals['not_free_price'] -= $totals['discount_amount'];   
        
        if($includingTax)
            $totals['not_free_price'] += $totals['tax_amount'];   
            
        if ($totals['not_free_price'] < 0)
            $totals['not_free_price'] = 0;
        
        if ($request->getFreeShipping() && $allowFreePromo)
            $totals['not_free_price'] = $totals['not_free_weight'] = $totals['not_free_qty'] = 0;     

        return $totals;       
    }
    
    public function getFreeQty($item, $allowFreePromo)
    {
        $freeQty = 0;

        if ($item->getFreeShipping() && $allowFreePromo)
            $freeQty = ((is_numeric($item->getFreeShipping())) && ($item->getFreeShipping() <= $item->getQty())) ? $item->getFreeShipping() : $item->getQty();
            
        return $freeQty;        
    }
    
    public function import($methodId, $fileName)
    {
        $err = array(); 
        
        $fp = fopen($fileName, 'r');
        if (!$fp){
            $err[] = Mage::helper('nettable')->__('Can not open file %s .', $fileName);  
            return $err;
        }
        $methodId = intval($methodId);
        if (!$methodId){
            $err[] = Mage::helper('nettable')->__('Specify a valid method ID.');  
            return $err;
        }
        
        $countryCodes = $this->getCountries();
        $stateCodes   = $this->getStates();
        $countryNames = $this->getCountriesName();
        $stateNames   = $this->getStatesName();
        $typeLabels   = Mage::helper('nettable')->getTypes();
                    
        $data = array();
        $dataIndex = 0;
        
        $currLineNum  = 0;
        while (($line = fgetcsv($fp, self::MAX_LINE_LENGTH, ',', '"')) !== false) {
            $currLineNum++;

            if (count($line) == 1)
            {
                continue;
            }
            
            if (count($line) != self::COL_NUMS){ 
               $err[] = 'Line #' . $currLineNum . ': skipped, expected number of columns is ' . self::COL_NUMS;
               continue;
            }
            
            for ($i = 0; $i < self::COL_NUMS; $i++) {
               $line[$i] = str_replace(array("\r", "\n", "\t", "\\" ,'"', "'", "*"), '', $line[$i]);
            }
            
            $countries = array('');
            if ($line[0]){
                $countries = explode(',', $line[0]);  
            } else {
                $line[0] = '0';
            } 
            $states = array('');
            if ($line[1]){
                $states = explode(',', $line[1]);  
            }
            
            $types = array('');
            if ($line[11]){
                $types = explode(',', $line[11]);  
            }              

            $zips = array('');
            if ($line[3]){
                $zips = explode(',', $line[3]);  
            } 
            
            if(!$line[6]) $line[6] =  999999; 
            if(!$line[8]) $line[8] =  999999;
            if(!$line[10]) $line[10] =  999999;
            
            foreach ($types as $type){
               if ($type == 'All'){
                    $type = 0;   
                }
                if ($type && empty($typeLabels[$type])) {
                    if (in_array($type, $typeLabels)){
                        $typeLabels[$type] = array_search($type, $typeLabels);   
                    }  else {
                        $err[] = 'Line #' . $currLineNum . ': invalid type code ' . $type;
                        continue;                       
                    }

                }
                $line[11] = $type ? $typeLabels[$type] : '';                                
            }
            
            foreach ($countries as $country){
               if ($country == 'All'){
                    $country = 0;   
                }
                
                if ($country && empty($countryCodes[$country])) {
                    if (in_array($country, $countryNames)){
                        $countryCodes[$country] = array_search($country, $countryNames);   
                    }  else {
                        $err[] = 'Line #' . $currLineNum . ': invalid country code ' . $country;
                        continue;                       
                    }

                }
                $line[0] = $country ? $countryCodes[$country] : '';

                foreach ($states as $state){
                    
                    if ($state == 'All'){
                        $state = '';  
                    }
                                        
                    if ($state && empty($stateCodes[$state][$country])) {
                        if (in_array($state, $stateNames)){
                            $stateCodes[$state][$country] = array_search($state, $stateNames);    
                        } else {  
                            $err[] = 'Line #' . $currLineNum . ': invalid state code ' . $state;
                            continue;                            
                        }                    

                    }
                    $line[1] = $state ? $stateCodes[$state][$country] : '';
                    
                    foreach ($zips as $zip){
                        $line[3] = $zip;
                        
                        
                        $data[$dataIndex] = $line;
                        $dataIndex++;

                        if ($dataIndex > 500){
                            $err2 = $this->getResource()->batchInsert($methodId, $data);
                            if ($err2){
                                $err[] = 'Line #' . $currLineNum . ': duplicated conditions before this line have been skipped';
                            }
                            $data = array();
                            $dataIndex = 0;
                        }
                    }                    
                }// states  
            }// countries 
        } // end while read  
        fclose($fp);
        
        if ($dataIndex){
            $err2 = $this->getResource()->batchInsert($methodId, $data);

            if ($err2){
                $err[] = 'Line #' . $currLineNum . ': duplicated conditions before this line have been skipped';
            }
        }
        
        return $err;
    }
    
    public function getCountries()
    {
        $hash = array();
        
        $collection = Mage::getResourceModel('directory/country_collection');
        foreach ($collection as $item){
            $hash[$item->getIso3Code()] = $item->getCountryId();
            $hash[$item->getIso2Code()] = $item->getCountryId();
        }
        
        return $hash;
    }
    
    public function getStates()
    {
        $hash = array();
        
        $collection = Mage::getResourceModel('directory/region_collection');
        foreach ($collection as $state){
            $hash[$state->getCode()][$state->getCountryId()] = $state->getRegionId();
        }

        return $hash;
    }
    public function getCountriesName()
    {
        $hash = array();
        $collection = Mage::getResourceModel('directory/country_collection');
        foreach ($collection as $item){
            $country_name=Mage::app()->getLocale()->getCountryTranslation($item->getIso2Code());
            $hash[$item->getCountryId()] = $country_name;
                
        }
        return $hash;
    }
    
    
    public function getStatesName()
    {
        $hash = array();
        
        $collection = Mage::getResourceModel('directory/region_collection');
        $countryHash = $this->getCountriesName();
        foreach ($collection as $state){
            $string = $countryHash[$state->getCountryId()].'/'.$state->getDefaultName();
            $hash[$state->getRegionId()] =  $string;  
        } 
        return $hash;               
    }
        
    public function initTotals()
    {
        $totals = array(
          //  'price'              => 0,
            'not_free_price'     => 0,
          //  'weight'             => 0,
            'not_free_weight'    => 0,
            'qty'                => 0,
            'not_free_qty'       => 0,
            'tax_amount'         => 0,
            'discount_amount'    => 0,
        );        
        return $totals;
    } 
    
    public function deleteBy($methodId)
    {
        return $this->getResource()->deleteBy($methodId);   
    }
}
