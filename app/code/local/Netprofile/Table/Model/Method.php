<?php

class Netprofile_Table_Model_Method extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('nettable/method');
    }
    
    public function massChangeStatus ($ids, $status) {
        foreach ($ids as $id) {
                $model = Mage::getModel('nettable/method')->load($id);
                $model->setIsActive($status);
                $model->save();
            }
        return $this;
    }
}