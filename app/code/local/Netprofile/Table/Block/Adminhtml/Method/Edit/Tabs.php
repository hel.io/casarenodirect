<?php

class Netprofile_Table_Block_Adminhtml_Method_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('methodTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('nettable')->__('Method Methods'));
    }

    protected function _beforeToHtml()
    {
        $tabs = array(
            'general'    => 'General',
            'stores'     => 'Stores & Customer Groups',
            'import'     => 'Import',
        );
        
        foreach ($tabs as $code => $label){
            $label = Mage::helper('nettable')->__($label);
            $content = $this->getLayout()->createBlock('nettable/adminhtml_method_edit_tab_' . $code)
                ->setTitle($label)
                ->toHtml();
                
            $this->addTab($code, array(
                'label'     => $label,
                'content'   => $content,
            ));
        }
        
        $this->addTab('rates', array(
            'label'     => Mage::helper('nettable')->__('Methods and Rates'),
            'class'     => 'ajax',
            'url'       => $this->getUrl('nettable/adminhtml_rate/index', array('_current' => true)),
        ));
    
        
        $this->_updateActiveTab();    
    
        return parent::_beforeToHtml();
    }
    
    protected function _updateActiveTab()
    {
    	$tabId = $this->getRequest()->getParam('tab');
    	if ($tabId) {
    		$tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
    		if ($tabId) {
    			$this->setActiveTab($tabId);
    		}
    	}
    	else {
    	   $this->setActiveTab('main'); 
    	}
    }     
 
    
}