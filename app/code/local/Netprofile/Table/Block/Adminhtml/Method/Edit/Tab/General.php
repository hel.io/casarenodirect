<?php

class Netprofile_Table_Block_Adminhtml_Method_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        $hlp = Mage::helper('nettable');
    
        $fldInfo = $form->addFieldset('general', array('legend'=> $hlp->__('General')));
        $fldInfo->addField('name', 'text', array(
            'label'     => $hlp->__('Name'),
            'required'  => true,
            'name'      => 'name',
        ));
		$fldInfo->addField('info', 'text', array(
            'label'     => $hlp->__('Information'), 
            'name'      => 'info',
        ));
		
        $fldInfo->addField('is_active', 'select', array(
            'label'     => Mage::helper('salesrule')->__('Status'),
            'name'      => 'is_active',
            'options'    => $hlp->getStatuses(),
        ));  
		
		$fldInfo->addField('is_map', 'select', array(
            'label'     => Mage::helper('salesrule')->__('Distance based'),
            'name'      => 'is_map',
            'options'    => $hlp->getMap()
        )); 
		
		$fldInfo->addField('taligate', 'select', array(
            'label'     => Mage::helper('salesrule')->__('Add additional charges'),
            'name'      => 'taligate',
            'options'    => $hlp->getTaligate()
        )); 
		
		$fldInfo->addField('group', 'select', array(
            'label'     => Mage::helper('salesrule')->__('Group'),
            'name'      => 'group',
            'options'    => $hlp->getGroups()
        )); 
		
		$fldInfo->addField('cms', 'text', array(
            'label'     => $hlp->__('Cms alias'), 
            'name'      => 'cms',
        ));
		
		$fldInfo->addField('lat', 'text', array(
            'label'     => $hlp->__('Lattitude'), 
            'name'      => 'lat',
        ));
		
		$fldInfo->addField('lng', 'text', array(
            'label'     => $hlp->__('Longtitude'), 
            'name'      => 'lng',
        ));
       
        $fldInfo->addField('pos', 'text', array(
            'label'     => Mage::helper('salesrule')->__('Priority'), 
            'name'      => 'pos',
        ));
        
        //set form values
        $form->setValues(Mage::registry('nettable_method')->getData()); 
        
        return parent::_prepareForm();
    }
}