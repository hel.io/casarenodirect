<?php

class Netprofile_Table_Block_Adminhtml_Method extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_method';
        $this->_blockGroup = 'nettable';
        $this->_headerText = Mage::helper('nettable')->__('Methods');
        $this->_addButtonLabel = Mage::helper('nettable')->__('Add Method');
        parent::__construct();
    }
}