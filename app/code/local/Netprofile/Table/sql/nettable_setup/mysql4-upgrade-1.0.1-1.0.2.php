<?php

/* @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE `net_table_method` ADD `cms` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `info`;");

$installer->endSetup();