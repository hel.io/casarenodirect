<?php

$this->startSetup();

$this->run("
CREATE TABLE `{$this->getTable('nettable/method')}` (
  `method_id`  mediumint(8) unsigned NOT NULL auto_increment,
  `is_active`   tinyint(1) unsigned NOT NULL default '0',
  `is_map`	    tinyint(1) unsigned NOT NULL default '0',
  `pos`         mediumint  unsigned NOT NULL default '0',
  `name`        varchar(255) default '', 
  `info`        varchar(255) default '', 
  `lat`			FLOAT( 10, 6 ) NOT NULL DEFAULT '0',
  `lng` 		FLOAT( 10, 6 ) NOT NULL DEFAULT '0',
  `stores`      varchar(255) NOT NULL default '', 
  `cust_groups` varchar(255) NOT NULL default '', 
  PRIMARY KEY  (`method_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{$this->getTable('nettable/rate')}` (
  `rate_id`     int(10) unsigned NOT NULL auto_increment,
  `method_id`   mediumint(8) unsigned NOT NULL,
 
  `country`     varchar(4)  NOT NULL default '',
  `state`       int(10)     NOT NULL default '0',  
  `city`        varchar(12) NOT NULL default '',  
  
  `zip_from`    varchar(10) NOT NULL default '',
  `zip_to`      varchar(10) NOT NULL default '', 
  
  `min_price` decimal(12,2) unsigned NOT NULL DEFAULT '0.00',
  `price_from`  decimal(12,2) unsigned NOT NULL default '0',
  `price_to`    decimal(12,2) unsigned NOT NULL default '0',

  `weight_from` decimal(12,4) unsigned NOT NULL default '0',
  `weight_to`   decimal(12,4) unsigned NOT NULL default '0',

  `qty_from`    	int(10) unsigned NOT NULL default '0',
  `qty_to`      	int(10) unsigned NOT NULL default '0', 
  `shipping_type` 	int(10) NOT NULL default '0',
  
  `cost_base`      decimal(12,2) unsigned NOT NULL default '0',
  `cost_percent`   decimal(5,2)  unsigned NOT NULL default '0',
  `cost_product`   decimal(12,2) unsigned NOT NULL default '0',
  `cost_weight`    decimal(12, 4) NOT NULL default '0.00',
  
  PRIMARY KEY  (`rate_id`),
  UNIQUE KEY(`method_id`, `country`, `state` , `city`, `zip_from`, `zip_to`,  `price_from`, `price_to`, `weight_from`, `weight_to`, `qty_from`, `qty_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$this->run("
ALTER TABLE  `{$this->getTable('nettable/rate')}` DROP INDEX  `method_id` ,
ADD UNIQUE  `method_id` (  `method_id` ,  `country` ,  `state` ,  `city` ,  `zip_from` ,  `zip_to` ,  `price_from` ,  `price_to` ,  `weight_from` ,  `weight_to` ,  `qty_from` ,  `qty_to` ,  `shipping_type` );
");

$this->addAttribute('catalog_product', 'net_shipping_type', array(
    'type'              => 'varchar',
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Shipping Type',
    'input'             => 'select',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'default'           => '0',
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => '',
    'is_configurable'   => false
));
$attributeId = $this->getAttributeId('catalog_product', 'net_shipping_type');

foreach ($this->getAllAttributeSetIds('catalog_product') as $attributeSetId) 
{
    try {
        $attributeGroupId = $this->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    } catch (Exception $e) {
        $attributeGroupId = $this->getDefaultAttributeGroupId('catalog_product', $attributeSetId);
    }
    $this->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
}

$this->endSetup();