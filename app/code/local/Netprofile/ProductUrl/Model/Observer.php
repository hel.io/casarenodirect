<?php

class Netprofile_ProductUrl_Model_Observer
{
	//redirect product to store view
	public function redirectProduct(Varien_Event_Observer $observer)
	{
		//$_product = Mage::registry('current_product');
		$event = $observer->getEvent();
		$x = $event['controller_action'];
		$productId = $x->getRequest()->getParam('id');
		$product = Mage::getModel('catalog/product')->load($productId);
		
		if(!Mage::app()->getRequest()->isXmlHttpRequest() && $product)
		{
			$session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
			$locale = Mage::app()->getLocale()->getLocaleCode();
			
			$redirect_enabled = Mage::getStoreConfig('catalog/product_rewrite/redirect_enabled',0);
			$default = $product->getAttributeText('base_store');
			$default_array = explode(',',$default);
			$stores = $this->getStoresArray();
			
			$storeId = Mage::app()->getStore()->getStoreId();
			$current = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);
			$locale = Mage::app()->getLocale()->getLocaleCode();
			$product_stores = $current->getStoreIds();

			if($redirect_enabled && $default && !in_array($storeId, $product_stores) && $product instanceof Mage_Catalog_Model_Product)
			{
				$new_store_code = false;
				
				//get redirect store code
				foreach($stores[$locale] as $store)
				{
					if(in_array($store, $default_array)) $new_store_code = $store;
				}
			
				
				if($new_store_code)
				{
					$new_store = Mage::getModel('core/store')->load($new_store_code, 'code')->getId();
					$url = Mage::getModel('catalog/product')->setStoreId($new_store)->load($productId)->getProductUrl();
					
					if ($url)
					{
						Mage::app()->getResponse()->setRedirect($url, 301);
					}
				}
			}
		}
	}
	
	public function getStoresArray()
	{
		$stores = array();
		$store_codes = Mage::getStoreConfig('catalog/product_rewrite/stores');
		$temp = preg_split("/\\r\\n|\\r|\\n/", $store_codes);
		
		foreach($temp as $code)
		{
			$codes = explode(':',$code);
			$store_lang = $codes[0];
			$store_string = explode(',',$codes[1]);
			foreach($store_string as $store_code)
			{
				$stores[$store_lang][] = trim($store_code);
			}
		}
		
		return $stores;
	}
}