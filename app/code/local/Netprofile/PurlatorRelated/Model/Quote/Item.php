<?php

class Netprofile_PurlatorRelated_Model_Quote_Item extends Mage_Sales_Model_Quote_Item
{
   public function setProduct($product)
   {
		parent::setProduct($product);
		$product_related = $product->getCustomProductIds();

		if(count($product_related) > 0)
		{
			//Mage::getSingleton( 'customer/session' )->setData( 'purlator_related',  '');
			
			$related = Mage::getSingleton( 'customer/session' )->getData('purlator_related', array());
			if(!$related) $related = array();
			$result = array_merge($related, $product_related);
			$unique = array_keys(array_flip($result)); 
			Mage::getSingleton( 'customer/session' )->setData( 'purlator_related',  $unique);
		}

		return $this;
   }
}