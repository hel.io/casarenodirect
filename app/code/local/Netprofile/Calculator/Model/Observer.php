<?php

class Netprofile_Calculator_Model_Observer
{    
/*
	public function hookToAddToCartPrepare($observer)
	{
		$event	= $observer->getEvent();
		$transport	= $event->getTransport();
		$product= $event->getProduct();
		$buyRequest = $event->getBuyRequest();
		$config = $this->getProductConfig($product);		
		if ($config && isset($config['unit']) && $config['unit'] && isset($config['carton'])){
			$id = $config['carton'];
			$unit = $config['unit'];
			$min = $config['min'];
			$qty = $buyRequest->getQty();
			$newVal = $unit * $qty;
			$transport->options[$id] = $newVal;
		}
		return $transport->options;
	}
*/
	public function hookToUpdateToCart($event)
	{
		$cart	= $event->getCart();
		$items 	= $cart->getItems();
		$coverage = $_REQUEST['coverage'];
		$storeId = Mage::app()->getStore()->getStoreId();
		
		$msg = false;
		foreach ($items as $item)
		{
			$msg = false;
			$item_id = $item->getProductId();
			
			if(array_key_exists($item_id, $coverage))
			{
				$product = Mage::getModel('catalog/product')->load($item_id);
				$config = $this->getProductConfig($product);
				
				/*$packing_type = Mage::getResourceModel('catalog/product')->getAttributeRawValue($item_id, 'mico_carton_unit_suff', $storeId);
				if($packing_type == 3 || $packing_type == 4 || $packing_type == 5 || $packing_type == 34) $unit_text = Mage::helper('calculator')->__('Boxes');
				else $unit_text = $product->getAttributeText('mico_carton_unit_suff');*/

				$new_coverage = $coverage[$item_id]['new'];
				$old_coverage = $coverage[$item_id]['old'];
				
				$min = $config['min'];
				$unit = (float)$config['unit'];
				$unit_text 	= $config['unit_text'];
				
				$new_qty = $coverage[$item_id]['new'];
				$old_qty = ceil($old_coverage / $unit);
				
				$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
				$max = (float)$stock->getQty();
				$max_coverage = ceil($max * $unit);

				if($new_coverage != $old_coverage && (float)$new_coverage == $new_coverage)
				{
					if($new_coverage > $min && $new_coverage < $max_coverage)
					{
						$new_coverage = $min;
						$new_qty = ceil($new_coverage / $unit);
						$item->setQty($new_qty);
					}else{
						if($new_coverage < $min) $msg = Mage::helper('calculator')->__('Min coverage for item').' '.$product->getName().' '.Mage::helper('calculator')->__('is').' '.(string)$min.' '.$unit_text;
						elseif($new_coverage > $max_coverage) $msg = Mage::helper('calculator')->__('Max coverage for item').' '.$product->getName().' '.Mage::helper('calculator')->__('is').' '.(string)$max_coverage.' '.$unit_text;
					}
					
				}elseif($new_qty){
					$new_coverage = $item->getQty() * $unit;
					if($new_coverage < $min)
					{
						$msg = Mage::helper('calculator')->__('Min coverage for item').' '.$product->getName().' '.Mage::helper('calculator')->__('is').' '.(string)$min.' '.$unit_text;
					}elseif($new_coverage > $max_coverage){
						$msg = Mage::helper('calculator')->__('Max coverage for item').' '.$product->getName().' '.Mage::helper('calculator')->__('is').' '.(string)$max_coverage.' '.$unit_text;
					}
				}
			}
			if($msg) 
			{
				Mage::getSingleton('core/session')->addNotice(strip_tags($msg));
				$item->setQty($old_qty);
			}

		}
		return $cart;
	}
	
	function getProductConfig($product){
		$this->model = Mage::getModel('calculator/config');

		$unit = $product->getMicoCartonUnit();
		if (!$unit){
			$oproduct = $product->load($product->getId());			
			$product->setMicoCartonUnit($oproduct->getMicoCartonUnit());
		}
		return $this->model->getProductConfig($product);		
	}
}