<?php

class Netprofile_Calculator_Model_Config
{

	protected function _construct()
    {
        $this->_init('calculator/config');
    }
	
	function getProductConfig($product){

		$unit = $product->getMicoCartonUnit();
		$min = $product->getMicoCartonMin();
		
		$config = array();
		if(!$unit) $unit = 1;
		
		$config['unit'] = $unit;
		$config['min'] = $min;
		$config['id'] = $product->getId();
		$config['unit_text'] = $product->getAttributeText('mico_carton_unit_suff');
		$config['price'] = $product->getFinalPrice();
		$config['tier'] = $product->getData('tier_price');
		
		
		return $config;
	}
}