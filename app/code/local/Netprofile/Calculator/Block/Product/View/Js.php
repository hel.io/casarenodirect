<?php

class Netprofile_Calculator_Block_Product_View_Js extends  Mage_Catalog_Block_Product_View_Abstract
{
	function getProductConfig(){
		$this->model = Mage::getModel('calculator/config');
		return $this->model->getProductConfig($this->getProduct());
	}	
    public function getProductJs()
    { 
		$config = $this->getProductConfig();		
		return Zend_Json::encode($config);
    }
	
}