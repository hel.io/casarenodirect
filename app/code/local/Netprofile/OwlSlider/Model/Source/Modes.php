<?php

class Netprofile_OwlSlider_Model_Source_Modes
{
	public function toOptionArray()
	{
		return array(
			array('value' => '',				'label' => ' '),
			array('value' => 'category',		'label' => 'category'),
			array('value' => 'attribute',		'label' => 'attribute'),
			array('value' => 'bestseller',		'label' => 'bestseller'),
		);
	}
}
