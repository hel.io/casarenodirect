<?php

class Netprofile_OwlSlider_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getCfg($option)
    {
        return Mage::getStoreConfig('owlslider/general/'.$option);
    }
	
	public function getAllCfg()
    {
        $config = array();
		$config['mode']	= $this->getCfg('mode');
		$config['variable']	= $this->getCfg('variable');
		$config['count']	= (int)$this->getCfg('count');
		$config['speed']	= (int)$this->getCfg('speed');
		$config['defaultitems']	= $this->getCfg('defaultitems');
		$config['breakpoints']	= $this->getCfg('breakpoints');
		$config['filters']	= $this->getCfg('filters');
		
		return $config;
    }
}
