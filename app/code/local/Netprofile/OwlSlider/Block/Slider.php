<?php

class Netprofile_OwlSlider_Block_Slider extends Mage_Core_Block_Template
{
	
	/**
	 * Get collection
	 *
	 * @return product collection
	 */
	public function getSliderProducts($mode, $variable)
	{
		$config = $this->getSliderCfg();

		switch($mode)
		{
			case('category'): $collection = $this->getCategoryMode($variable);break;
			case('attribute'): $collection = $this->getAttributeMode($variable);break;
			case('bestseller'): $collection = $this->getBestsellerMode();break;
			default: $collection = $this->getBestsellerMode();break;
		}
		
		return $collection;
	}

	
	private function getCategoryMode($category)
	{
		$config = $this->getSliderCfg();
		$store_id = Mage::app()->getStore()->getId();
		$_category = Mage::getModel('catalog/category')->load($category);

		if($_category)
		{
			$subcats = $_category->getChildren();
			$subcategories = array();
			$categoryIds = explode(',',$subcats);
			$categoryIds[] = $_category->getId();
			
			$_productCollection = Mage::getModel('catalog/product')
				->getCollection()
				->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
				->addStoreFilter($store_id)
				->addAttributeToSelect('*')
				->addAttributeToFilter('category_id', array('in' => $categoryIds));
			
			$_productCollection->getSelect()->limit($config['count']);

			return $_productCollection;
		}else return false;
	}
	
	private function getAttributeMode($attribute)
	{
		$config = $this->getSliderCfg();
		
		$_productCollection = Mage::getModel('catalog/product')
			->getCollection()
			->addAttributeToSelect('*')
			->addAttributeToFilter($attribute, array('eq' => 1))
			->limit($config['count']);
					
		return $_productCollection;
	}
	
	private function getBestsellerMode()
	{
		$storeId = (int) Mage::app()->getStore()->getId();
		
		$config = $this->getSliderCfg();
		// Date
		$date = new Zend_Date();
		$toDate = $date->setDay(1)->getDate()->get('Y-MM-dd');
		$fromDate = $date->subMonth(1)->getDate()->get('Y-MM-dd');
					 
		$_productCollection = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToSelect('*');
					 
		$_productCollection->getSelect()
			->joinLeft(
				array('aggregation' => $_productCollection->getResource()->getTable('sales/bestsellers_aggregated_monthly')),
				"e.entity_id = aggregation.product_id AND aggregation.store_id={$storeId} AND aggregation.period BETWEEN '{$fromDate}' AND '{$toDate}'",
				array('SUM(aggregation.qty_ordered) AS sold_quantity')
			)
			->group('e.entity_id')->limit($config['count'])
			->order(array('sold_quantity DESC', 'e.created_at'));
				 
			Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($_productCollection);
			Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($_productCollection);
							
		return $_productCollection;
	}

	public function getFilters()
	{
		$config = $this->getSliderCfg();

		if($config['filters'])
		{
			$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'filter');
			if ($attribute->usesSource()) {
				$filters = $attribute->getSource()->getAllOptions(false);
			}			
		}else $filters = '';
		
		return $filters;
	}

	public function getSliderCfg()
	{
		
		$helper = Mage::helper('owlslider');
		
		$config = array();
		$config['mode']	= $helper->getCfg('mode');
		$config['variable']	= $helper->getCfg('variable');
		$config['count'] = (int)$helper->getCfg('count');
		$config['speed'] = (int)$helper->getCfg('speed');
		$config['defaultitems']	= $helper->getCfg('defaultitems');
		$config['breakpoints']	= $helper->getCfg('breakpoints');
		$config['filters']	= $helper->getCfg('filters');
		
		
		return $config;
	}
}