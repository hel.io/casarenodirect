<?php

class Netprofile_OwlSlider_Block_Config extends Mage_Core_Block_Template
{
	public function getSliderCfg()
	{
		$helper = Mage::helper('owlslider');
		
		$config = array();
		$config['mode']	= $helper->getCfg('mode');
		$config['variable']	= $helper->getCfg('variable');
		$config['count']	= (int)$helper->getCfg('count');
		$config['speed']	= (int)$helper->getCfg('speed');
		$config['defaultitems']	= $helper->getCfg('defaultitems');
		$config['breakpoints']	= $helper->getCfg('breakpoints');
		$config['filters']	= $helper->getCfg('filters');
		
		
		return $config;
	}
}