<?php

/** @var $this Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn(
    $installer->getTable('mageworx_downloads/files'),
    'imgurl',
    "varchar (1024) default ''"
);

$installer->endSetup();