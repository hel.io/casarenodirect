<?php


class Staylime_DownloadImg_Helper_Data  extends MageWorx_Downloads_Helper_Data
{

    public function getDownloadsPath($fileId, $addPath = '')
    {
        return Mage::getBaseDir('media') . DS . 'downloads' . DS . $fileId . DS . $addPath . DS;
    }

}