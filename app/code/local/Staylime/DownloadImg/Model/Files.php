<?php
/**
 * MageWorx
 * File Downloads & Product Attachments Extension
 *
 * @category   MageWorx
 * @package    MageWorx_Downloads
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */

class Staylime_DownloadImg_Model_Files extends MageWorx_Downloads_Model_Files
{
    

    public function removeDownloadsFile($fileId, $isRemoveFolder = true, $addPath='')
    {
        $dir = Mage::helper('mageworx_downloads')->getDownloadsPath($fileId,$addPath);
        $files = Mage::helper('mageworx_downloads')->getFiles($dir);
        if ($files) {
            foreach ($files as $value) {
                unlink($value);
            }
            if ($isRemoveFolder === true) {
                $io = new Varien_Io_File();
                $io->rmdir($dir);
            }
        }
    }

   
}
