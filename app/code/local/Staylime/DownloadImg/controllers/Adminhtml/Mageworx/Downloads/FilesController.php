<?php

require_once(Mage::getModuleDir('controllers','MageWorx_Downloads').DS.'Adminhtml'.DS.'Mageworx'.DS.'Downloads'.DS.'FilesController.php');
class Staylime_DownloadImg_Adminhtml_Mageworx_Downloads_FilesController extends MageWorx_Downloads_Adminhtml_Mageworx_Downloads_FilesController
{

        protected function _uploadFile($id, $data)
    {
         
        $helper = Mage::helper('mageworx_downloads');
        $data = $helper->getFilter($data);

        $fileSession = Mage::getSingleton('adminhtml/session')->getUploadFiles();
        $registerKey = 'files_' . Mage::getSingleton('adminhtml/session')->getSessionId();
        $multiUpload = false;

        try {

            if ($_FILES['file'] && !empty($_FILES['file']['tmp_name']) && $_FILES['file']['error']  > 0) {
                $excMessage = 'An error occured during file uploading. [Error code: '.$_FILES['file']['error'].']';
                throw new Exception($excMessage);
            }

            if (($_FILES['file']['size'] > 0 && isset($data['general']['url']))
                || isset($fileSession[$registerKey])
                || ($_FILES['file']['size'] == 0 && isset($data['general']['url']) && $data['general']['url'] == '')
            ) {
                unset($data['general']['url']);
                unset($data['general']['embed_code']);
            }

            $productIds = $this->_prepareProductIds($data);

            if (isset($fileSession[$registerKey])) {
                $multiUpload = true;

                foreach ($fileSession[$registerKey] as $fileDesc) {

                    $file = Mage::getSingleton('mageworx_downloads/files');
                    $file->setData($data['general']);
                    $file->setName(substr($fileDesc['name'], 0, strrpos($fileDesc['name'], '.')));
                    $file->setType(substr($fileDesc['name'], strrpos($fileDesc['name'], '.') + 1));
                    $file->setFilename($fileDesc['name']);
                    $file->setSize(filesize($fileDesc['path']));
                    $file->save();

                    $this->_moveFile($file, $fileDesc);

                    $this->_setProductsFileRelation($productIds, $file->getId(), $id);
                }
                Mage::getSingleton('adminhtml/session')->setUploadFiles(array());
            } else {
                $file = Mage::getSingleton('mageworx_downloads/files');
                $file->setData($data['general']);
                if ($id) {
                    $file->setId($id);
                }
                $file->save();
            }
            // Upload File
            
            
            if ($_FILES['file']['size'] > 0 || $_FILES['imgurl']['size'] > 0) {
                
                if($_FILES['file']['size'] > 0){
                    $upload = $this->_doUploadFile('file', $file->getId());
                }
                
                if($_FILES['imgurl']['size'] > 0){
                    $upload_img = $this->_doUploadFile('imgurl', $file->getId(), 'thmb/');
                }

                if (!empty($upload['error']) && !$id) {
                    $file->delete();
                    throw new Exception($this->__('Unable to save file'));
                }
                
                
                if($upload_img){
                   $file->setImgurl($upload_img['newname']); 
                   $file->save();
                }
                
                if ($upload) {
                    $file->setUrl('');
                    $file->setEmbedCode('');
                    $file->setFilename($_FILES['file']['name']);
                    $file->addData($upload)
                        ->setId($file->getId())
                        ->save();
                }
               
            } else if (isset($data['general']['url'])) {
                $files = $helper->getFiles($helper->getDownloadsPath($file->getId()));
                if (!empty($files)) {
                    foreach ($files as $fileName) {
                        unlink($fileName);
                    }
                }

                $url = parse_url($data['general']['url']);
                if (!isset($url['path'])) {
                    $url['path'] = '';
                }
                $ext  = pathinfo($url['path'], PATHINFO_EXTENSION);
                $type = $this->_getLinkType($ext, $data);

                $file->setType($type);
                $file->setFilename('');
                $file->setSize(0);
                $file->save();
            } elseif (isset($data['general']['filename'])) {
                $file->setType(substr($data['general']['filename'], strrpos($data['general']['filename'], '.') + 1));
                if ($file->getId()) {
                    $filePath = Mage::getBaseDir() . DS . 'media' . DS . 'downloads' . DS . $file->getId();
                    $file->setSize(filesize($filePath .  DS . $file->getFilename()));
                }
                $file->save();
            }

            $this->_setProductsFileRelation($productIds, $file->getId(), $id);

            $successMessage = $multiUpload ? $helper->__('Files were successfully saved') : $helper->__('File was successfully saved');
            Mage::getSingleton('adminhtml/session')->addSuccess($successMessage);
            Mage::getSingleton('adminhtml/session')->setData('downloads_data');

            if ($this->getRequest()->getParam('back')) {
                $this->_redirect('*/*/edit', array('id' => $file->getId()));
                return;
            }
            $this->_redirect('*/*/');
            return;
        } catch (Exception $e) {
            if ($e->getMessage()) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            Mage::getSingleton('adminhtml/session')->setData('downloads_data', $data);
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
    }
    
        private function _doUploadFile($keyFile, $fileId, $thmbPath='')
    {
        $result = array();
        if (isset($_FILES[$keyFile]['name']) && $_FILES[$keyFile]['name'] != '') {
            try {
                Mage::getSingleton('mageworx_downloads/files')->removeDownloadsFile($fileId, false, $thmbPath);

                $uploader = new Varien_File_Uploader($keyFile);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);

                $correctFilename = $uploader->getCorrectFileName($_FILES[$keyFile]['name']);
                $newFileName = Mage::helper('mageworx_downloads')->getDownloadsPath($fileId, $thmbPath ) . $correctFilename;
                $uploader->save(Mage::helper('mageworx_downloads')->getDownloadsPath($fileId, $thmbPath), $correctFilename);

                if (!file_exists($newFileName) || !filesize($newFileName)) {
                    $result['error'] = true;
                }

                $result['size']     = $_FILES[$keyFile]['size'];
                $result['type']     = substr($_FILES[$keyFile]['name'], strrpos($_FILES[$keyFile]['name'], '.') + 1);
                $result['newname']  = $correctFilename;
            } catch (Exception $e) {
                if ($e->getMessage()) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
        return $result;
    }
}
