<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Full Page Cache
 * @version   1.0.49
 * @build     755
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */


if (Mage::helper('mstcore')->isModuleInstalled('Mirasvit_AsyncIndex') && class_exists('Mirasvit_AsyncIndex_Model_Process')) {
    abstract class Mirasvit_Fpc_Model_Index_Process_Abstract extends Mirasvit_AsyncIndex_Model_Process {

    }
} else {
    abstract class Mirasvit_Fpc_Model_Index_Process_Abstract extends Mage_Index_Model_Process {

    }
}

class Mirasvit_Fpc_Model_Index_Process extends Mirasvit_Fpc_Model_Index_Process_Abstract
{

    protected $_cleanTags = array();

    /**
     * Process event with assigned indexer object
     *
     * @param Mage_Index_Model_Event $event
     * @return Mage_Index_Model_Process
     */
    public function processEvent(Mage_Index_Model_Event $event)
    {
        if (Mage::getSingleton('fpc/config')->getUpdateStockMethod() != Mirasvit_Fpc_Model_Config::UPDATE_STOCK_METHOD_REINDEX) {
            return parent::processEvent($event);
        }

        if (!$this->matchEvent($event)) {
            return $this;
        }
        if ($this->getMode() == self::MODE_MANUAL) {
            $this->changeStatus(self::STATUS_REQUIRE_REINDEX);
            return $this;
        }

        $this->_clearCacheByTags($event, $force = false);

        $this->_getResource()->updateProcessStartDate($this);
        $this->_setEventNamespace($event);
        $isError = false;

        try {
            $this->getIndexer()->processEvent($event);
        } catch (Exception $e) {
            $isError = true;
        }
        $event->resetData();
        $this->_resetEventNamespace($event);
        $this->_getResource()->updateProcessEndDate($this);
        $event->addProcessId($this->getId(), $isError ? self::EVENT_STATUS_ERROR : self::EVENT_STATUS_DONE);

        $this->_clearCacheByTags(null, true);

        return $this;
    }

    /**
     * Очищаем кеш исходя из события
     * @param  object  $event
     * @param  boolean $force
     * @return object
     */
    protected function _clearCacheByTags($event, $force = false)
    {
        if ($event != null) {
            $cacheTag = $event->getData('entity') . '_' . $event->getData('entity_pk');
            $this->_cleanTags[] = $cacheTag;
        }

        if ($force && count($this->_cleanTags)) {
            foreach ($this->_cleanTags as $idx => $tag) {
                $this->_cleanTags[$idx] = strtoupper($tag);
            }

            Mage::getSingleton('fpc/cache')->clearCacheByTags($this->_cleanTags);

        }

        return $this;
    }
}