<?php

$this->startSetup();
$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'homepage_category', array(
    'group'         => 'General Information',
    'input'         => 'select',
    'type'          => 'int',
    'label'         => 'Show to Homepage',
    'source'   		=> 'eav/entity_attribute_source_boolean',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'homepage_category_image', array(
    'input'         => 'image', // you can change here 
    'type'          => 'varchar',
    'group' 		=> 'General Information',/// Change here whare you want to show this
    'label'         => 'Homepage Image',
    'visible'       => 1,
    'backend' 		=> 'catalog/category_attribute_backend_image',
    'required'      => 0,
    'user_defined' 	=> 1,
    'frontend_input' =>”,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible_on_front'  => 1,
));
 
$this->endSetup();

?>