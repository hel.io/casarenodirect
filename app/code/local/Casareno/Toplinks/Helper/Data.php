<?php

/**
 * @author     Erlis Dhima
 * @package    Casareno_Toplinks
 * @copyright  Copyright (c) 2017 Casa Reno Direct (https://www.casarenodirect.com/)
 *
 */

class Casareno_Toplinks_Helper_Data extends Mage_Core_Helper_Url
{
	public function getContactUrl() {
		$baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		$storeCode = Mage::app()->getStore()->getCode();

		if ($storeCode == "en") {
			$contactUrl = $baseUrl . $storeCode . '/contacts';
			return $contactUrl;
		} elseif ($storeCode == "fr") {
			$contactUrl = $baseUrl . $storeCode . '/contacts';
			return $contactUrl;
		} else {
			$contactUrl = $baseUrl . '/en/contacts';
			return $contactUrl;
		}
	}

	public function getAboutUrl() {
		$baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		$storeCode = Mage::app()->getStore()->getCode();

		if ($storeCode == "en") {
			$aboutUrl = $baseUrl . $storeCode . '/about-us';
			return $aboutUrl;
		} elseif ($storeCode == "fr") {
			$aboutUrl = $baseUrl . $storeCode . '/a-propos';
			return $aboutUrl;
		} else {
			$aboutUrl = $baseUrl . '/en/about-us';
			return $aboutUrl;
		}
	}
}