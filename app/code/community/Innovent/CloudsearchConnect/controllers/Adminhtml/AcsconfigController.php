<?php
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */

/**
 * acsconfig admin controller
*
* @category	Innovent
* @package	Innovent_CloudsearchConnect
* @author 	Innovent Solutions, Inc.
*/
class Innovent_CloudsearchConnect_Adminhtml_AcsconfigController extends Mage_Adminhtml_Controller_Action
{
	protected $_config;
	const KEY_BASE = Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS;
	
	/**
	 * init the config
	 * @access protected
	 * @return Innovent_CloudsearchConnect_Model_Config
	 */
	protected function _construct(){
		// Define module dependent translate
		$this->setUsedModuleName('Innovent_CloudsearchConnect');
		$this->_config = Mage::getSingleton('search/acsconfig');

	}
		
	protected function _initModuleFromData($data)
	{
		//Mage::log('acsconfigController::_initModuleFromData data = ' . var_export($data,true));
		//$config = Mage::getSingleton('search/acsconfig');
		if (isset($data['general']))
		{
			$this->_config->addData($data['general']);
			if (isset($data['general']['acs_enabled']))
			{
				$this->_config->setAcsEnabled(($data['general']['acs_enabled'] === null) ? 0 : 1);
			}
			else 
			{
				$this->_config->setAcsEnabled(0);
			}
							
		}
		if (isset($data['remote']))
		{
			$this->_config->addData($data['remote']);
		}
		if (isset($data['ops']))
		{
			$this->_config->addData($data['ops']);
		}
		if (isset($data['export']))
		{
			$this->_config->addData($data['export']);
		}
		
		return $this->_config;
	}

	/**
	 * default action
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function indexAction()
	{
		//Mage::log('In acsconfigController::indexAction');
		$layout = $this->loadLayout();
		$layout->_title(Mage::helper('search')->__('Form'))->_title(Mage::helper('search')->__('Cloudsearch Configuration'));
		$layout->_addBreadcrumb(Mage::helper('search')->__('Form'), Mage::helper('search')->__('Form'));
		$this->renderLayout();
	}

	public function validateAction()
	{
		$errors = array();
		$response = new Varien_Object();
		$response->setError(false);
			
		//Mage::log('acsconfigController::validateAction');
		$data = $this->getRequest()->getPost();

		$ops = $this->getRequest()->getPost('ops');
		if (empty($ops['domain'])){
			$error = new Varien_Object();
			$error->setField('ops_domain');
			$error->setMessage(Mage::helper('search')->__('Search domain name cannot be empty.'));
			$errors[] = $error->toArray();
		}
		
		$remote = $this->getRequest()->getPost('remote');
		if (empty($remote['region'])){
			$error = new Varien_Object();
			$error->setField('remote_region');
			$error->setMessage(Mage::helper('search')->__('Region cannot be empty.'));
			$errors[] = $error->toArray();
		}
		if (empty($remote['access_key'])){
			$error = new Varien_Object();
			$error->setField('remote_access_key');
			$error->setMessage(Mage::helper('search')->__('Access key cannot be empty.'));
			$errors[] = $error->toArray();
		}
		if (empty($remote['secret_key'])){
			$error = new Varien_Object();
			$error->setField('remote_secret_key');
			$error->setMessage(Mage::helper('search')->__('Secret key cannot be empty.'));
			$errors[] = $error->toArray();
		}
		if (empty($remote['client_ip'])){
			$error = new Varien_Object();
			$error->setField('remote_client_ip');
			$error->setMessage(Mage::helper('search')->__('Client IP address cannot be empty.'));
			$errors[] = $error->toArray();
		}
		
		$export = $this->getRequest()->getPost('export');
		if (empty($export['index_filename'])){
			$error = new Varien_Object();
			$error->setField('export_index_filename');
			$error->setMessage(Mage::helper('search')->__('Data filename cannot be empty.'));
			$errors[] = $error->toArray();
		}
		$dir = dirname($export['index_filename']);
		if ($dir !== '.')
		{
			$error = new Varien_Object();
			$error->setField('export_index_filename');
			$error->setMessage(Mage::helper('search')->__('Data filename contains a directory path - please remove it.'));
			$errors[] = $error->toArray();
		}
		
		try
		{
			$this->_config = $this->_initModuleFromData($data);
		}
		catch (Exception $e){
			$error = new Varien_Object();
			$error->setMessage($e->getMessage());
			$errors[] = $error->toArray();
		}
		if (count($errors) > 0){
			$response->setError(true);
			$response->setErrors($errors);
		}
		$this->getResponse()->setBody($response->toJson());
		///Mage::register('config_data', $config);
	}

	/**
	 * Save Acs config - action
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function saveAction()
	{
		$redirectBack = $this->getRequest()->getParam('back', false);
		try
		{
			$this->_config = $this->_initModuleFromData($this->getRequest()->getPost());			
			$this->_config->save();
			Mage::getModel('search/engine_acs_search')->setDomainEndpoints();
			
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('search')->__('CloudSearch configuration has been saved.'));
			Mage::getSingleton('adminhtml/session')->setFormData($this->_config);
			Mage::helper('search')->clearCache();
		}
		catch (Exception $e)
		{
			$this->_getSession()->addError($e->getMessage());
			$redirectBack = true;
		}
		if ($redirectBack) {
			$this->_redirect('*/*/');
		}
		else {
			if( $this->getRequest()->getParam('tab') ){
				$tab = explode("_", $this->getRequest()->getParam('tab'));
				$this->_redirect('*/*/', array('active_tab' => $tab));
			}
		}
	
	}
	
	/**
	 * Delete all existing data action
	 */
	public function deleteDataAction() 
	{
		if (!Mage::helper('search')->test())
		{
			Mage::getSingleton('core/session')->addError('Your CloudSearch server is not running or is not responding, cannot process command.');
		}
		elseif ($this->_config->getDocumentEndpoint($docEndpoint) == null)
		{
			Mage::getSingleton('core/session')->addError('Cannot process command, document endpoint is empty, and//or domain [' . $this->_config->getDomain() . '] does not exist.');
		}
		else
		{
		
			try {
				$search = Mage::getModel('engine/acs_search');
				$retval = $search->deleteAllDocuments();
				if ($retval == -1)
				{
					Mage::getSingleton('adminhtml/session')->addNotice($this->__('There is no search domain data to delete.'));
				}
				elseif ($retval == 1)
				{
					Mage::getSingleton('adminhtml/session')->addSuccess($this->__('All search domain data have been deleted'));
				}
				else 
				{
					Mage::getSingleton('adminhtml/session')->addError($this->__('Cannot delete search domain data.'));
				}
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addNotice($this->__('Cannot delete search domain data, %s',$e->getMessage()));
			}
		}
		$this->_redirect('*/*/', array('active_tab' => 'ops'));
	}
	
	/**
	 * buildData for Acs - action
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function buildDataAction() 
	{
		//Mage::log('acsconfigController::buildDataAction');
		$model = Mage::getModel('search/engine_acs_indexer');
		if (Mage::getStoreConfig(self::KEY_BASE . '/file_type') === 'csv' || 
		    Mage::getStoreConfig(self::KEY_BASE . '/file_type') === 'txt' )
		{
			$result = $model->createDelimited();
		}
		else 
		{
			$result = $model->createXML();
		}
				
		if ($result == 1) {
			Mage::getSingleton('adminhtml/session')->addSuccess('Data file was created successfully.');
		}
		else 
		{
			Mage::getSingleton('adminhtml/session')->addError('Unable to create the data file, check the log for more information.');
		}
		
		$msgs = $model->getErrorMsgs();
		$pMsgs = $model->getProcessMsgs();
		if (count($msgs))
		{
			foreach($msgs as $msg)
			{
				Mage::getSingleton('core/session')->addError($msg);
			}
		}
		if (count($pMsgs))
		{
			foreach($pMsgs as $msg)
			{
				Mage::getSingleton('core/session')->addNotice($msg);
			}
		}
		
		$this->_redirect('*/*/', array('active_tab' => 'export'));					
	}

	/**
	 * buildIndex for Acs - action
	 * Creates a Cloudsearch domain
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function buildIndexAction()
	{
		//Mage::log('acsconfigController::buildIndexAction');
		$result = Mage::getModel('search/engine_acs_indexer')->createIndex();
		if ($result == 1) {
			$msg = Mage::helper('search')->__('Your search domain has been created successfully, and the domain is loading. Check the index status in a few minutes then create and load your index data. ');
			Mage::getSingleton('core/session')->addNotice($msg);
		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError('Unable to create the CloudSearch index domain, check the log for more information.');
		}
		$this->_redirect(('*/*/'), array('active_tab' => 'ops'));
	}
	
	/**
	 * checkStatus - action
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function checkStatusAction()
	{		
		//Mage::log('acsconfigController::checkStatusAction');
		if (!Mage::helper('search')->test())
		{
			Mage::getSingleton('core/session')->addError('Your CloudSearch server is not running or is not responding, cannot process command.');
		}
		else
		{		
			$result = Mage::getModel('search/engine_acs_search')->checkIndexStatus();
		}
		$this->_redirect('*/*/', array('active_tab' => 'ops'));
	}	

	/**
	 * checkStatus - action
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function autoProcessAction()
	{
		//Mage::log('acsconfigController::autoProcessAction');
		if (!Mage::helper('search')->test())
		{
			Mage::getSingleton('core/session')->addError('Your CloudSearch server is not running or is not responding, cannot process command.');
		}
		elseif ($this->_config->getDocumentEndpoint($docEndpoint) == null)
		{
			Mage::getSingleton('core/session')->addError('Cannot process command, document endpoint is empty, and//or domain [' . $this->_config->getDomain() . '] does not exist.');
		}
		else
		{
			$model = Mage::getModel('search/engine_acs_indexer');
			$result = $model->rebuildIndex();
			$msgs = $model->getErrorMsgs();
			$pMsgs = $model->getProcessMsgs();
			if (count($msgs))
			{
				foreach($msgs as $msg)
				{
					Mage::getSingleton('core/session')->addError($msg);
				}
			}
			if (count($pMsgs))
			{
				foreach($pMsgs as $msg)
				{
					Mage::getSingleton('core/session')->addNotice($msg);
				}
			}
				
		}
		$this->_redirect('*/*/', array('active_tab' => 'ops'));
	}
	
}
