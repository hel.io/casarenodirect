<?php
/*

 * Innovent_CloudsearchConnect extension

*

* NOTICE OF LICENSE

*

* This source file is subject to the Magento Extension License Agreement

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://www.innoventsolutions.com/magento-extension-license.html

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to magento@innoventsolutions.com so we will send you a copy immediately.

*

*

* @category	Innovent

* @package		Innovent_CloudsearchConnect

* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)

* @author      Innovent Solutions, Inc.

* @license     http://www.innoventsolutions.com/magento-extension-license.html

*/

// CALL ME : http://local.magento.com/magento/search/Autocomplete/terms?q=sho
// http://search-magento-f-thenqgdfgt7pwwbanprbwljv5e.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q=sho*&format=xml&return=sku,category,name

class Innovent_CloudsearchConnect_AutocompleteController extends Mage_Core_Controller_Front_Action
{

	protected function getKeyBase()
	{
		return Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS;
	}

    protected function getAcsURL(){
    	$host = Mage::getStoreConfig($this->getKeyBase() . '/search_endpoint');
    	$api_version = Mage::getStoreConfig($this->getKeyBase() . '/api_version');
    	$url = 'http://'. $host . '/'. $api_version . '/search';

	   	return $url;
    }

    protected function fixHTMLtags($string){
    	$retStr = trim($string);
    	//$retStr = @str_replace(array("</li><li>","<ul>","</ul>","</ul", "<li>","</li>","<br>","<br/>","<br />","&lt;br&gt;"), ' ', $retStr);
        $retStr = @str_replace(array("</li><li>","<ul>","</ul>","</ul", "<li>","</li>","<br>","<br/>","<br />"), ' ', $retStr);

    	$retStr = @str_replace(array(' _ ',' _'), ' ', $retStr);

    	return $retStr;
    }


    // Access : http://local.magento.com/search/Autocomplete/terms
    // ACS Facet : http://localhost:8983/solr/select?facet=on&facet.field=autocomplete&facet.prefix=ra
    public function termsAction()
    {
		$sayttermsurl = $this->getAcsURL();

		$params = array();
		$q = $this->getRequest()->getParam('q');
		$return = 'name,category';
		$size = 5;



		// ******************** AWS STUFF ***************
		$sayttermsurl .= '?q=' . urlencode($q) . "*" . "&return=". $return ."&size=". $size ;

		$responseBody = $this->curlAWS($sayttermsurl);

		$found = json_decode($responseBody);
// echo "<BR>Found= ". $found;		
		$retVal = $retHash = array();
		if( isset($found->hits->hit) && count($found->hits->hit))
		foreach($found->hits->hit as $hit){
			$retHash{ $hit->fields->category[0] }[] = str_replace("&amp;", "&", $hit->fields->name);
		}
		foreach($retHash as $cat=>$catname){
			foreach($catname as $title){
				$retVal{'items'}[] = array(
						// "name" =>  str_replace("&lt;br&gt;", "<br>",  $title),
						"name" => $title,
						"category"=>$cat);
			}
		}

		$retVal = $this->sortCategories($retVal, "category");

		$responseBody = json_encode($retVal);
		// ******************** END AWS STUFF ***************

		$responseBody = $this->fixHTMLtags($responseBody);
		$this->getResponse()->setHeader('X-JSON', '');
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody($responseBody);

    }

	// Access : http://local.magento.com/search/Autocomplete/items
	public function itemsAction()
    {
		$saytitemsurl =  $this->getAcsURL();
		$params = array();
		$q = $this->getRequest()->getParam('q');
		$format = 'json';
		$return = 'name,id,sku,price,imageurl,url,category';
		$size = 6;



		// ******************** ACS STUFF ***************
		$saytitemsurl .= '?q=' . urlencode($q) ."*" . "&format=". $format . "&return=". $return . "&size=". $size;

		$responseBody = $this->curlAWS($saytitemsurl);

		$found = json_decode($responseBody);
		$retVal = array();
		$remotehost = $this->full_path();

		if( isset($found->hits->hit) && count($found->hits->hit))
		foreach($found->hits->hit as $hit){
			// ******************** AB 11feb2015  ***************
			// Turns out that Mage 1.7 / 8 CE behaves very
			// differently in the URLS than 1.9 CE
			// Added this check for url and below for imageurl
			// **************************************************
			/*
			if(strtolower(Mage::getEdition()) === "community" && strpos( Mage::getVersion(), "1.9" ) === FALSE ){
				$pattern = "/^.*\/index.php\//";
				$replace = $remotehost . 'index.php';
				$url = preg_replace($pattern, $replace, $hit->fields->url);
			}else{
				$url = $hit->fields->url;
			}

			$remotehostImgUrl = preg_replace($pattern, "", $remotehost);
			if(strtolower(Mage::getEdition()) === "community" && strpos( Mage::getVersion(), "1.9" ) === FALSE ){
				$pattern = "/^.*\/media\//";
				$replace = $remotehost . "media/";
				$imageurl = preg_replace($pattern, $replace, $hit->fields->imageurl);
			}else{
				$imageurl = $hit->fields->imageurl;
			}*/


$product = Mage::getModel('catalog/product')->load($hit->fields->id);
$url = $product->getProductUrl();

			/* ******************** FOR CASA RENO ******************************** 
			$regexendpattern = "/\/$/i";
			$regexpattern = "/\/index\.php\/admin\/catalog\/product\/view\/id\/(\d+)\/(\w+)/i";
			$url = preg_replace ( $regexpattern, '/en', $hit->fields->url );
			$url = preg_replace ( $regexendpattern, ".html", $url );
*/
			//$url = $hit->fields->url;
			$imageurl = $hit->fields->imageurl;
			/* ********* END custom Casa Reno code : AB @ Innovent Solutions 10/28/2015 ** */ 


			$retVal{'items'}[] = array("sku"=> $hit->fields->sku[0],
									"name" => str_replace("&amp;", "&", $hit->fields->name),
									"price"=> $hit->fields->price?$hit->fields->price:'',
									"imageurl"=> $imageurl?$imageurl:'',
									"url"=> $url?$url:'');
		}
		$responseBody = json_encode($retVal);
		// ******************** END AWS STUFF ***************




		$responseBody = $this->fixHTMLtags($responseBody);
		$this->getResponse()->setHeader('X-JSON', '');
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody($responseBody);
    }

	public function getStoreId() {
        return Mage::app()->getStore()->getId();
    }


	function curlAWS($awsurl){
		$searchResults = FALSE;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $awsurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  //10 sec timeout
		curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 100);
		curl_setopt($ch, CURLOPT_LOW_SPEED_TIME, 10);
		curl_setopt($ch, CURLOPT_VERBOSE, false);  // VERY verbose output for debugging A.B.
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Since I know who I'm curling I don't want to verify the certificate ...  A.B.

		if ( ($searchResults = curl_exec($ch)) === FALSE ) {
			// Error: 7 = Solr was unavailable
			// echo "<BR>Error: ".curl_errno($ch)."<br>\n";
			$searchrResults  = '<?xml version="1.0" encoding="UTF-8"?>
								<response><Error errorNumber="'. curl_errno($ch) .'">Solr was not available</Error></response>';

		}

		curl_close($ch);
		return $searchResults;    // XML Results OR JSON Results. Let the calling function decide what it wants.
	}


	function  sortCategories(&$array, $key) {
		$sorter=array();
		$ret=array();
		reset($array);
		foreach ($array as $ii => $va) {
			$sorter[$ii]=$va[$key];
		}
		asort($sorter);
		foreach ($sorter as $ii => $va) {
			$ret[$ii]=$array[$ii];
		}
		$array=$ret;
		return $array;
	}

	function full_path(){
		$s = &$_SERVER;
		$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
		$sp = strtolower($s['SERVER_PROTOCOL']);
		$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
		$port = $s['SERVER_PORT'];
		$port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
		$host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
		$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
		$uri = $protocol . '://' . $host . $s['REQUEST_URI'];
		$segments = explode('index.php', $uri, 2);
		$url = $segments[0];
		return $url;
	}

}
