<?php
/*

* Innovent_CloudsearchConnect extension

*

* NOTICE OF LICENSE

*

* This source file is subject to the Magento Extension License Agreement

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://www.innoventsolutions.com/magento-extension-license.html

* If you did not rece0ive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to magento@innoventsolutions.com so we will send you a copy immediately.

*

*

* @category	Innovent

* @package		Innovent_CloudsearchConnect

* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)

* @author      Innovent Solutions, Inc.

* @license     http://www.innoventsolutions.com/magento-extension-license.html

*/

/**
 * CloudSearch Connect module install script
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author		Innovent Solutions, Inc.
 */


$this->startSetup();

$installer = $this;
$installer->startSetup();
$conn = $installer->getConnection();
$indexProcessTable = $installer->getTable('index/process');
$coreConfigDataTable = $installer->getTable('core/config_data');

/*
 * Change the Catalog Search Index process option to manual to prevent
 * recurring index rebuilds during product changes.
*/
if($conn->isTableExists($indexProcessTable)) {
	$sql = "UPDATE ". $indexProcessTable ." SET mode = 'manual' WHERE indexer_code = 'catalogsearch_fulltext'";
	$this->run($sql);
}

/*
 * Modify the attribute table to add a search_weight column
 */
$conn->addColumn(
		$this->getTable('catalog/eav_attribute'),
		'search_weight',
		"decimal(5,3) NOT NULL DEFAULT '1.0' "
);


/*
 * Add default entries for the search engines' configuration,
 * only if the entries for each search engine do not already exist (don't overwrite on an upgrade)
 *
 * Test for the "enabled" entry for each search engine - if there, assume other default entries
 * are already in the table...
 */
if($conn->isTableExists($coreConfigDataTable)) {
	$prefix = 'innovent_search/acs/';
	$sql = "INSERT IGNORE INTO ". $coreConfigDataTable. " (scope, scope_id, path, value) VALUES('default',0,'" . $prefix . "acs_enabled','0'),";
	$sql .= "('default',0,'" . $prefix . "domain','Please set your search domain!'),";
	$sql .= "('default',0,'" . $prefix . "return_fields','sku,name'),";
	$sql .= "('default',0,'" . $prefix . "extra_searchable_fields',' '),";
	$sql .= "('default',0,'" . $prefix . "search_limit','100'),";
	$sql .= "('default',0,'" . $prefix . "api_version','2013-01-01'),";
	$sql .= "('default',0,'" . $prefix . "document_endpoint',''),";
	$sql .= "('default',0,'" . $prefix . "search_endpoint',''),";
	$sql .= "('default',0,'" . $prefix . "access_key',''),";
	$sql .= "('default',0,'" . $prefix . "secret_key',''),";
	$sql .= "('default',0,'" . $prefix . "region','us-west-1'),";
	$sql .= "('default',0,'" . $prefix . "index_filename','cloudsearch_feed.xml'),";
	$sql .= "('default',0,'" . $prefix . "sayt_enabled','0'),";
	$sql .= "('default',0,'" . $prefix . "file_type','XML'),";
	$sql .= "('default',0,'" . $prefix . "client_ip','0.0.0.0')";

	$this->run($sql);
}

$this->endSetup();

?>