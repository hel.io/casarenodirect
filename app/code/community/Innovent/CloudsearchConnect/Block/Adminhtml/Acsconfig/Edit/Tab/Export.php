<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * ExportColumn edit form tab
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Block_Adminhtml_Acsconfig_Edit_Tab_Export extends Mage_Adminhtml_Block_Widget_Form
{	
	
	/**
	 * prepare the form
	 * @access protected
	 * @return Acs_Excol_Block_Adminhtml_Excol_Edit_Tab_Form
	 * @author Innovent Solutions, Inc.
	 */
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('export_');
		
		$data = Mage::registry('Search_Acsconfig');
		if ($data)
		{
			$values = new Varien_Object($data->asArray());
		}
		else
		{
			$values = Mage::getSingleton('search/acsconfig');
		}
		$fieldset = $form->addFieldset('setup', array('legend'=>Mage::helper('search')->__('Create Export File')));
		$fileType = $fieldset->addField('file_type','select', array(
				'name'		=> 'file_type',
				'label'     => Mage::helper('search')->__('Output file type '),
				'class'     => 'required-entry',
				'required'  => true,
				'value'  => $values->getFileType(),
				'onchange'=> "fileTypeChange(this)",
				'values' => array('xml' => 'Formatted XML','txt' => 'Delimited Text','csv' => 'Comma-separated Values'),
				'note' => Mage::helper('search')->__('Choose the format for your data file: fully-formatted XML, TXT or CSV.
						The "txt" and "csv" formats allow you to define the field and multivalued fields separator characters.'),				
		));
		$fieldset->addField('index_filename', 'text', array(
				'name'	=> 'index_filename',
				'required'  => true,
				'label'     => Mage::helper('search')->__('Data Filename'),
				'value'	=> $values->getIndexFilename(),
				'note' => Mage::helper('search')->__('File will be located in {{base_dir}}/var/export.')				
		));
		$fieldSep = $fieldset->addField('field_sep', 'select', array(
				'name'	=> 'field_sep',
				'required'  => true,
				'label'     => Mage::helper('search')->__('Field Separator'),
				'value'		=> $values->getFieldSep(),
				'values' => array('tab' => 'Tab','comma' => 'Comma (,)','colon' => 'Colon (:)','semicolon' => 'Semicolon (;)')
		));
		$multiSep = $fieldset->addField('multivalue_sep', 'select', array(
				'name'	=> 'multivalue_sep',
				'required'  => true,
				'label'     => Mage::helper('search')->__('MultiValued Items Separator'),
				'value'		=> $values->getMultivalueSep(),
				'values' => array('bar' => 'Bar (|)','tilde' => 'Tilde (~)', 'colon' => 'Colon (:)','semicolon' => 'Semicolon (;)', 'comma' => 'Comma (,)'),
				'onchange'	=> "checkMultivalueSep(this)"
		));
		
		$fieldset->addField('create_data', 'button', array(
				'label' => Mage::helper('search')->__('Create data file'),
				'name'  => 'create_data',
				'value' => Mage::helper('search')->__(' Go '),
				'onclick'	=> "setLocation('" . Mage::helper('adminhtml')->getUrl('adminhtml/acsconfig/buildData') . "')",
				'note' => Mage::helper('search')->__('Do not click this button if you have pending changes on this page - save your changes first!')				
		));	
								
		$this->setForm($form);
		$form->addFieldNameSuffix('export');
		
		$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
				->addFieldMap($fileType->getHtmlId(), $fileType->getName())
				->addFieldMap($fieldSep->getHtmlId(), $fieldSep->getName())
				->addFieldMap($multiSep->getHtmlId(), $multiSep->getName())
				->addFieldDependence($fieldSep->getName(),$fileType->getName(),array('csv', 'txt'))
				->addFieldDependence($multiSep->getName(),$fileType->getName(),array('csv', 'txt'))
		);
		
		return parent::_prepareForm();
	}

	/*
	 * Inject col width definitions into the form table before rendering
	*/
	public function _afterToHtml($html)
	{
		$srch = '<table cellspacing="0" class="form-list">';
		$newHtml = "";
		$strToSearch = $html;
		$pos1 = 0; $pos2 = 0;
		$insert = "<col width='20%'><col width='80%'>";
		//Mage::log('before html: ' . $html);
		
		$pos2 = strpos($strToSearch, $srch, $pos1);

		while (1 == 1)
		{
			$pos2 += strlen($srch);
			$htmlPart = substr($strToSearch, $pos1, $pos2 - $pos1 + 1) . $insert;
			//Mage::log('String found, pos1 = ' . $pos1 . ', pos2 = ' . $pos2 . ', html = ' . $htmlPart);
			$newHtml .= $htmlPart;	
			
			$pos1 = $pos2 + 1;
			$pos2 = strpos($strToSearch, $srch, $pos1);
			if ($pos2 === false) 
			{ 
				$pos2 = strlen($strToSearch);
				$htmlPart = substr($strToSearch, $pos1, $pos2 - $pos1 + 1) . $insert;
				$newHtml .= $htmlPart;
				
				break; 
			}
				
		}
		return $newHtml;
	}
}