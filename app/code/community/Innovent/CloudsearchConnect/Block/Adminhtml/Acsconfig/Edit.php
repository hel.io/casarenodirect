<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * ExportColumn admin edit block
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Block_Adminhtml_Acsconfig_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	/**
	 * constuctor
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->_blockGroup = 'search';
		$this->_controller = 'adminhtml_acsconfig';
		$this->_headerText = Mage::helper('search')->__('Edit Form');
		$this->setTemplate('innovent/acsconnect/acsedit.phtml');
	}

	/**
	 * get the edit form header
	 * @access public
	 * @return string
	 * @author Innovent Solutions, Inc.
	 */
	public function getHeaderText()
	{
		return Mage::helper('search')->__('Configuration');
	}
	
	/* prepare the layout
	* @access protected
	* @return Innovent_CloudsearchConnect_Block_Adminhtml_Config_Edit
	* @see Mage_Adminhtml_Block_Widget_Form_Container::_prepareLayout()
	* @author Innovent Solutions, Inc.
	*/
	protected function _prepareLayout()
	{
		$layout = $this->getLayout();
		$tabsBlock = $layout->getBlock('config_tabs');
		if ($tabsBlock) {
			$tabsBlockJsObject = $tabsBlock->getJsObjectName();
			$tabsBlockPrefix   = $tabsBlock->getId() . '_';
		} else {
			$tabsBlockJsObject = 'page_tabsJsTabs';
			$tabsBlockPrefix   = 'page_tabs_';
		}
		
		$this->setChild('back_button',
				$this->getLayout()
				->createBlock('adminhtml/widget_button')
				->setData(array(
						'label' => Mage::helper('search')->__('Back'),
						'onclick'   => 'setLocation(\''.$this->getUrl('*').'\')',
						'class' => 'back'
				))
		);
		$this->setChild('reset_button',
				$this->getLayout()
				->createBlock('adminhtml/widget_button')
				->setData(array(
						'label' => Mage::helper('search')->__('Reset'),
						'onclick'   => 'window.location.reload()',
						'class' => 'reset'
				))
		);
		$this->setChild('save_button',
				$this->getLayout()
				->createBlock('adminhtml/widget_button')
				->setData(array(
						'label' => Mage::helper('search')->__('Save'),
						'onclick'   => 'saveAndContinueEdit(\''.$this->getSaveAndContinueUrl().'\')',
						'class' => 'save'
				))
		);
		
		$block = $layout->createBlock('core/text');
		$block->setText(
			'<script type="text/javascript">
				function fileTypeChange(select)
				{
					var re = /(?:\.([^.]+))?$/;
					
					var filename = document.getElementById("export_index_filename").value;
					var ext = re.exec(filename)[1];	
					var newFilename = filename.replace(re.exec(filename)[1], select.value);
					document.getElementById("export_index_filename").value = newFilename;	

					// Set the delimiters if filetype = CSV
					if (select.value == "csv")
					{
						document.getElementById("export_field_sep").value = "comma";
						document.getElementById("export_multivalue_sep").value = "bar";
					}
					// Set the delimiters if filetype = TXT
					if (select.value == "txt")
					{
						document.getElementById("export_field_sep").value = "tab";
						document.getElementById("export_multivalue_sep").value = "comma";
					}
	}
				function checkMultivalueSep(select) 
				{
					var fieldSep = document.getElementById("ops_field_sep").value;
					var multiSep = document.getElementById("ops_multivalue_sep").value;
				
					if (fieldSep == multiSep)
					{
						alert("The multivalued field separator cannot match the field separator.");
					}
				}
				fileTypeChange(document.getElementById("ops_file_type"));
			</script>'
		);
		$layout->getBlock('js')->append($block);
		return $this;
	}
	
	/**
	 * get the back button html
	 * @access public
	 * @return string
	 * @author Innovent Solutions, Inc.
	 */
	public function getBackButtonHtml()
	{
		return $this->getChildHtml('back_button');
	}
	
	/**
	 * get the reset button html
	 * @access public
	 * @return string
	 * @author Innovent Solutions, Inc.
	 */
	public function getResetButtonHtml(){
		return $this->getChildHtml('reset_button');
	}
	
	/**
	 * get the save button html
	 * @access public
	 * @return string
	 * @author Innovent Solutions, Inc.
	 */
	public function getSaveButtonHtml(){
		return $this->getChildHtml('save_button');
	}
	
	/**
	 * get the save and continue edit button html
	 * @access public
	 * @return string
	 * @author Innovent Solutions, Inc.
	 */
	public function getSaveAndEditButtonHtml(){
		return $this->getChildHtml('save_and_edit_button');
	}
	
	/**
	 * get the save and continue edit url
	 * @access public
	 * @return string
	 * @author Innovent Solutions, Inc.
	 */
	public function getSaveAndContinueUrl(){
		return $this->getUrl('*/*/save', array(
				'_current'   => true,
				///'back'   => 'edit',
				'tab'=> '{{tab_id}}',
		));
	}
	
	/**
	 * get the validation url
	 * @access public
	 * @return string
	 * @author Innovent Solutions, Inc.
	 */
	public function getValidationUrl(){
		return $this->getUrl('*/*/validate', array('_current'=>true));
	}
	
	/**
	 * check if edit mode is read only
	 * @deprecated 1.3
	 * @access public
	 * @return bool
	 * @author Innovent Solutions, Inc.
	 */
	public function isReadonly(){
		return false;
	}
	
}