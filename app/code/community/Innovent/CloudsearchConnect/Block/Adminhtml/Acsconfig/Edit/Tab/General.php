<?php
/*

 * Innovent_CloudsearchConnect extension

*

* NOTICE OF LICENSE

*

* This source file is subject to the Magento Extension License Agreement

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://www.innoventsolutions.com/magento-extension-license.html

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to magento@innoventsolutions.com so we will send you a copy immediately.

*

*

* @category	Innovent

* @package		Innovent_CloudsearchConnect

* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)

* @author      Innovent Solutions, Inc.

* @license     http://www.innoventsolutions.com/magento-extension-license.html

*/

/**
 * ExportColumn edit form tab
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Block_Adminhtml_Acsconfig_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{	
	/**
	 * prepare the form
	 * @access protected
	 * @return Acs_Excol_Block_Adminhtml_Excol_Edit_Tab_Form
	 * @author Innovent Solutions, Inc.
	 */
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('general_');
		
		$data = Mage::registry('Search_Acsconfig');
		if ($data)
		{
			$values = new Varien_Object($data->asArray());
		}
		else
		{
			$values = Mage::getSingleton('search/acsconfig');
		}
				
		$fieldset = $form->addFieldset('general_fieldset', array('legend'=>Mage::helper('search')->__('General Options')));
		$fieldset->addField('acs_enabled', 'checkbox', array(

				'label' => Mage::helper('search')->__('Enable Amazon CloudSearch'),

				'name'  => 'acs_enabled',

				'onclick'   => 'this.value = this.checked ? 1 : 0;',

				'checked' => $values->getAcsEnabled()

		));

		

		$fieldset->addField('return_fields', 'text', array(

				'label' => Mage::helper('search')->__('Additional Fields'),

				'name'  => 'return_fields',

				'value' => $values->getReturnFields(),

				'note' => Mage::helper('search')->__('A comma-delimited list of fields to return with the results. 
					ID is always included by default.'),

		));

		$fieldset->addField('extra_searchable_fields', 'text', array(

				'label' => Mage::helper('search')->__('Additional Searchable Fields'),

				'name'  => 'extra_searchable_fields',

				'value' => $values->getExtraSearchableFields(),

				'note' => Mage::helper('search')->__('A comma-delimited list of fields to search that are not attribute fields

						 <br><b>IMPORTANT NOTE:</b> The field Must exist in CloudSearch, or searches may fail'),

		));		

		$fieldset->addField('search_limit', 'text', array(

				'label' => Mage::helper('search')->__('Search Limit'),

				'name'  => 'search_limit',

				'value' => $values->getSearchLimit(),

		));

		
		$fieldset->addField('sayt_enabled', 'select', array(
				'label' => Mage::helper('search')->__('Autocomplete Setting'),
				'name'  => 'sayt_enabled',
				'values'=> array(
					array(
                    'value'     => Mage::helper('search')->__('none'),
                    'label'     => Mage::helper('search')->__('None')),
                	array(
                    'value'     => Mage::helper('search')->__('terms'),
                    'label'     => Mage::helper('search')->__('Terms')),                
                	array(
                    'value'     => Mage::helper('search')->__('items'),
                    'label'     => Mage::helper('search')->__('Items')),
					array(
					'value'     => Mage::helper('search')->__('both'),
					'label'     => Mage::helper('search')->__('Both'))),
					'value' => $values->getSaytEnabled(),
		));
/*		$fieldset->addField('spell_enabled', 'select', array(
				'label' => Mage::helper('search')->__('Enable Spellcheck'),
				'name'  => 'spell_enabled',
				'values'=> array(
						array(
								'value'     => Mage::helper('search')->__('1'),
								'label'     => Mage::helper('search')->__('Yes')),
						array(
								'value'     => Mage::helper('search')->__('0'),
								'label'     => Mage::helper('search')->__('No'))),
				'value' => $values->getSpellEnabled(),
		));
*/		
		$this->setForm($form);
		$form->addFieldNameSuffix('general');
		return parent::_prepareForm();
	}
		
}