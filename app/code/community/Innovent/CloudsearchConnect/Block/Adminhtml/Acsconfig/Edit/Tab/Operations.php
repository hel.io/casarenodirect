<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * ExportColumn edit form tab
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Block_Adminhtml_Acsconfig_Edit_Tab_Operations extends Mage_Adminhtml_Block_Widget_Form
{	
	
	/**
	 * prepare the form
	 * @access protected
	 * @return Acs_Excol_Block_Adminhtml_Excol_Edit_Tab_Form
	 * @author Innovent Solutions, Inc.
	 */
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('ops_');
		
		$data = Mage::registry('Search_Acsconfig');
		if ($data)
		{
			$values = new Varien_Object($data->asArray());
		}
		else
		{
			$values = Mage::getSingleton('search/acsconfig');
		}

		$fieldset = $form->addFieldset('schema', array('legend'=>Mage::helper('search')->__('Automated Search Domain Operations')));
		$fieldset->addField('l1', 'label', array(
				'label' 	=> '',
				'name'  => 'l1',
		));
				
		$fieldset->addField('domain', 'text', array(
				'label' => Mage::helper('search')->__('CloudSearch Search Domain'),
				'name'  => 'domain',
				'value' => $values->getDomain(),
				'note'	=> Mage::helper('search')->__('<b>Important:</b> If you change the search domain, be sure to save the configuration!')
		));
		$fieldset->addField('create_index', 'button', array(
				'label' => Mage::helper('search')->__('Create or Recreate CloudSearch Search Domain'),
				'name'  => 'create_index',
				'value' => Mage::helper('search')->__(' Go '),
				'onclick'	=> "setLocation('" . Mage::helper('adminhtml')->getUrl('adminhtml/acsconfig/buildIndex') . "')",
				'note' => Mage::helper('search')->__('Creates (or recreates) a CloudSearch search domain using the domain specified above.
						<b>Note:</b> This process may take several minutes to complete.')
		));
		$fieldset->addField('status', 'button', array(
				'label' => Mage::helper('search')->__('Get Domain Status'),
				'name'  => 'status',
				'value' => Mage::helper('search')->__(' Go '),
				'onclick'	=> "setLocation('" . Mage::helper('adminhtml')->getUrl('adminhtml/acsconfig/checkStatus') . "')",
				'note' => Mage::helper('search')->__('Check the CloudSearch search domain status.')
		));
		
		$fieldset = $form->addFieldset('setup', array('legend'=>Mage::helper('search')->__('Manual Search Domain Operations')));
		$fieldset->addField('delete_data', 'button', array(
				'label' => Mage::helper('search')->__('Delete Domain data'),
				'name'  => 'delete_data',
				'value' => Mage::helper('search')->__(' Go '),
				'onclick'	=> "setLocation('" . Mage::helper('adminhtml')->getUrl('adminhtml/acsconfig/deleteData') . "')",
				'note' => Mage::helper('search')->__('Deletes all existing index entries from the CloudSearch search domain.')				
		));
		
		$fieldset->addField('auto_process', 'button', array(
				'label' => Mage::helper('search')->__('Create and load domain data'),
				'name'  => 'auto_process',
				'value' => Mage::helper('search')->__(' Go '),
				'onclick'	=> "setLocation('" . Mage::helper('adminhtml')->getUrl('adminhtml/acsconfig/autoProcess') . "')",
				'note'	=> Mage::helper('search')->__('Use this option to automatically create a XML format data feed file
						 and load it into your CloudSearch search domain.')
		));
				
		$this->setForm($form);
		$form->addFieldNameSuffix('ops');
		
		return parent::_prepareForm();
	}

	/*
	 * Create additional text for the form before rendering
	*/
	public function _afterToHtml($html)
	{
		$srch = '<table cellspacing="0" class="form-list">';
		$newHtml = "";
		$strToSearch = $html;
		$pos1 = 0; $pos2 = 0;
		$insert = "<col width='20%'><col width='80%'>";
	
		$pos2 = strpos($strToSearch, $srch, $pos1);
	
		while (1 == 1)
		{
			$pos2 += strlen($srch);
			$htmlPart = substr($strToSearch, $pos1, $pos2 - $pos1 + 1) . $insert;
			//Mage::log('String found, pos1 = ' . $pos1 . ', pos2 = ' . $pos2 . ', html = ' . $htmlPart);
			$newHtml .= $htmlPart;
				
			$pos1 = $pos2 + 1;
			$pos2 = strpos($strToSearch, $srch, $pos1);
			if ($pos2 === false)
			{
				$pos2 = strlen($strToSearch);
				$htmlPart = substr($strToSearch, $pos1, $pos2 - $pos1 + 1) . $insert;
				$newHtml .= $htmlPart;
	
				break;
			}
	
		}
		$msg = Mage::helper('search')->__('<td colspan="2">Use this option to automatically create a CloudSearch search domain. '
				. 'This option will create a search domain, add field definitions for every attribute that has "Use in Quick Search" set to "Yes" in your Magento store, ' 
				. 'create access policies, add a suggester, then initiate a re-index procedure. Once this process '
				. 'completes, you may create and upload your search domain data. '
				. 'See the CloudsearchConnect installation document for more information and complete details.<br><hr>'
				. '<b><i>Important Note</i>: Should you make a change to product attributes that triggers a message to rebuild your '
				. 'Magento catalog index, you should also recreate this CloudSearch domain and reload index data - running this operation on a previously '
				. 'created index domain will add or delete index fields as appropriate.</b>'
				. '</td><tr><td></td></tr>');
		$new = str_replace('<td class="label"><label for="ops_l1"></label></td>', $msg, $newHtml);
		return $new;
	}
	
}