<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * Admin edit tabs
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Block_Adminhtml_Acsconfig_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->setId('acsconfig_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('search')->__('CloudsearchConnect Administration'));	
	}
	
	
	/**
	 * before render html
	 * @access protected
	 * @return Innovent_CloudsearchConnect_Block_Adminhtml_Config_Edit_Tabs
	 * @author Innovent Solutions, Inc.
	 */
	protected function _beforeToHtml()
	{
		$tab = $this->getRequest()->getParam('tab');
		$this->addTab('general', array(
				'label'     => Mage::helper('search')->__('General'),
				'title'     => Mage::helper('search')->__('General'),
				'content'	=> $this->getLayout()->createBlock('search/adminhtml_acsconfig_edit_tab_general')->toHtml(),
				'active' 	=> ($tab == 'general'),
		));
		
		$this->addTab('remote', array(
			'label'		=> Mage::helper('search')->__('Remote Server Settings'),
			'title'		=> Mage::helper('search')->__('Remote Server Settings'),
			'content'	=> $this->getLayout()->createBlock('search/adminhtml_acsconfig_edit_tab_remote')->toHtml(),
			'active' 	=> ($tab == 'remote'),
		));

		$this->addTab('export', array(
				'label'		=> Mage::helper('search')->__('Export Data'),
				'title'		=> Mage::helper('search')->__('Export Data'),
				'content'	=> $this->getLayout()->createBlock('search/adminhtml_acsconfig_edit_tab_export')->toHtml(),
				'active' 	=> ($tab == 'export'),
		));
		
		$this->addTab('ops', array(
			'label'		=> Mage::helper('search')->__('Operations'),
			'title'		=> Mage::helper('search')->__('Operations'),
			'content'	=> $this->getLayout()->createBlock('search/adminhtml_acsconfig_edit_tab_operations')->toHtml(),
			'active' 	=> ($tab == 'ops'),			
		));
		
		return parent::_beforeToHtml();
	}
}