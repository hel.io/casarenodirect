<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * ExportColumn edit form tab
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Block_Adminhtml_Acsconfig_Edit_Tab_Remote extends Mage_Adminhtml_Block_Widget_Form
{	
	/**
	 * prepare the form
	 * @access protected
	 * @return Acs_Excol_Block_Adminhtml_Excol_Edit_Tab_Form
	 * @author Innovent Solutions, Inc.
	 */
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('remote_');

		$values = Mage::getModel('search/acsconfig');
		
		$fieldset = $form->addFieldset('remote_fieldset', array('legend'=>Mage::helper('search')->__('Remote Server Settings')));
		$fieldset->addField('api_version', 'text', array(
				'label' => Mage::helper('search')->__('AWS API Version'),
				'name'  => 'api_version',
				'value' => $values->getApiVersion(),
				'note' => Mage::helper('search')->__('API Version must be included in all calls to CloudSearch APIs.'),	
				'disabled' => True			
		));
		
		$fieldset->addField('search_endpoint', 'text', array(
			'label' => Mage::helper('search')->__('Search Endpoint'),
			'name'  => 'search_endpoint',
			'value' => $values->getSearchEndpoint(),
			'note'	=> Mage::helper('search')->__('CloudsearchConnect will attempt to populate this field from the index domain name.')
		));

		$fieldset->addField('document_endpoint', 'text', array(
				'label' => Mage::helper('search')->__('Document Endpoint'),
				'name'  => 'document_endpoint',
				'value' => $values->getDocumentEndpoint(),			
				'note'	=> Mage::helper('search')->__('CloudsearchConnect will attempt to populate this field from the index domain name.')
		));
		
		$fieldset->addField('region', 'text', array(
			'label' => Mage::helper('search')->__('Region'),
			'name'  => 'region',
			'value' => $values->getRegion(),	
		));
		
		$fieldset->addField('access_key', 'text', array(
				'label' => Mage::helper('search')->__('Access Key'),
				'name'  => 'access_key',
				'value' => $values->getAccessKey(),		
		));
		
		$fieldset->addField('secret_key', 'text', array(
				'label' => Mage::helper('search')->__('Secret Key'),
				'name'  => 'secret_key',
				'value' => $values->getSecretKey(),
		));
				
		$fieldset->addField('client_ip', 'text', array(
			'label' => Mage::helper('search')->__('Client IP Address'),
			'name'  => 'client_ip',
			'value' => $values->getClientIP(),	
			'note' => Mage::helper('search')->__('This IP address is used to configure access to the document endpoint when creating a search domain.')
		));
		
		$this->setForm($form);
		$form->addFieldNameSuffix('remote');
		return parent::_prepareForm();
	}

	
}