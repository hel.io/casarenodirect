<?php
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */

/**
 * Handles decimal attribute filtering in layered navigation.
 *
 * @package Innovent_CloudsearchConnect
 * @subpackage Innovent_CloudsearchConnect_Block
 */
class Innovent_CloudsearchConnect_Block_Catalog_Layer_Filter_Decimal extends Mage_Catalog_Block_Layer_Filter_Abstract
{
    /**
     * Defines specific filter model name.
     *
     * @see Innovent_CloudsearchConnect_Model_Catalog_Layer_Filter_Decimal
     */
    public function __construct()
    {
        parent::__construct();
        $this->_filterModelName = 'search/catalog_layer_filter_decimal';
    }

    /**
     * Prepares filter model.
     *
     * @return Innovent_CloudsearchConnect_Block_Catalog_Layer_Filter_Decimal
     */
    protected function _prepareFilter()
    {
        $this->_filter->setAttributeModel($this->getAttributeModel());

        return $this;
    }

    /**
     * Adds facet condition to filter.
     *
     * @see Innovent_CloudsearchConnect_Model_Catalog_Layer_Filter_Decimal::addFacetCondition()
     * @return Innovent_CloudsearchConnect_Block_Catalog_Layer_Filter_Decimal
     */
    public function addFacetCondition()
    {
        $this->_filter->addFacetCondition();

        return $this;
    }
}
