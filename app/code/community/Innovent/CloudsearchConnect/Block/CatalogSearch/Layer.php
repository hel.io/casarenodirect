<?php
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */

/**
 * Overrides default layer view process to define custom filter blocks.
 *
 * @package Innovent_CloudsearchConnect
 * @subpackage Innovent_CloudsearchConnect_Block
 */
class Innovent_CloudsearchConnect_Block_CatalogSearch_Layer extends Mage_CatalogSearch_Block_Layer
{
    /**
     * Boolean block name.
     *
     * @var string
     */
    protected $_booleanFilterBlockName;

    /**
     * Modifies default block names to specific ones if engine is active.
     */
    protected function _initBlocks()
    {
    	parent::_initBlocks();
        if (Mage::helper('search')->isActiveEngine()) 
        {
            $this->_categoryBlockName = 'search/catalog_layer_filter_category';
            $this->_attributeFilterBlockName = 'search/catalogsearch_layer_filter_attribute';
            $this->_priceFilterBlockName = 'search/catalog_layer_filter_price';
            $this->_decimalFilterBlockName = 'search/catalog_layer_filter_decimal';
            $this->_booleanFilterBlockName   = 'search/catalog_layer_filter_boolean';
        }
    }

    /**
     * Prepares layout if engine is active.
     * Difference between parent method is addFacetCondition() call on each created block.
     *
     * @return Innovent_CloudsearchConnect_Block_Catalogsearch_Layer
     */
    protected function _prepareLayout()
    {    	 
        /** @var $helper Innovent_CloudsearchConnect_Helper_Data */
        $helper = Mage::helper('search');
        if (!$helper->isActiveEngine()) {
            parent::_prepareLayout();
        } else {
            $stateBlock = $this->getLayout()->createBlock($this->_stateBlockName)
                ->setLayer($this->getLayer());

            $categoryBlock = $this->getLayout()->createBlock($this->_categoryBlockName)
                ->setLayer($this->getLayer())
                ->init();

            $this->setChild('layer_state', $stateBlock);
            $this->setChild('category_filter', $categoryBlock->addFacetCondition());

            $filterableAttributes = $this->_getFilterableAttributes();
            $filters = array();
            foreach ($filterableAttributes as $attribute) {
                if ($attribute->getAttributeCode() == 'price') {
                    $filterBlockName = $this->_priceFilterBlockName;
                } elseif ($attribute->getSourceModel() == 'eav/entity_attribute_source_boolean') {
                    $filterBlockName = $this->_booleanFilterBlockName;
                } elseif ($attribute->getBackendType() == 'decimal') {
                    $filterBlockName = $this->_decimalFilterBlockName;
                } else {
                    $filterBlockName = $this->_attributeFilterBlockName;
                }

                $filters[$attribute->getAttributeCode() . '_filter'] = $this->getLayout()->createBlock($filterBlockName)
                    ->setLayer($this->getLayer())
                    ->setAttributeModel($attribute)
                    ->init();
            }

            foreach ($filters as $filterName => $block) {
                $this->setChild($filterName, $block->addFacetCondition());
            }

            $this->getLayer()->apply();
        }

        return $this;
    }

    /**
     * Checks display availability of layer block.
     *
     * @return bool
     */
    public function canShowBlock()
    {
        return ($this->canShowOptions() || count($this->getLayer()->getState()->getFilters()));
    }

    /**
     * Returns current catalog layer.
     *
     * @return Innovent_CloudsearchConnect_Model_Catalogsearch_Layer|Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        /** @var $helper Innovent_CloudsearchConnect_Helper_Data */
        $helper = Mage::helper('search');
        if ($helper->isActiveEngine()) {
            return Mage::getSingleton('search/catalogsearch_layer');
        }

        return parent::getLayer();
    }
}
