<?php
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */
 
 /**
 * ExportColumn edit form tab
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Block_CatalogSearch_Result extends Mage_CatalogSearch_Block_Result
{	

	/**
	 * Prepare layout
	 *
	 * @return Mage_CatalogSearch_Block_Result
	 */
	protected function _prepareLayout()
	{
		// add Home breadcrumb
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {
			$title = $this->__("Search results");
	
			$breadcrumbs->addCrumb('home', array(
					'label' => $this->__('Home'),
					'title' => $this->__('Go to Home Page'),
					'link'  => Mage::getBaseUrl()
			))->addCrumb('search', array(
					'label' => $title,
					'title' => $title
			));
		}
		else 
		{
			// modify page title
			$title = $this->__("Search results");
		}	
		
		$this->getLayout()->getBlock('head')->setTitle($title);
	}
	
	public function getHeaderText()
	{
		// Return a formatted search notice, including spellcheck results if there
		///$link = "<a href='" . Mage::helper('core/url')->getCurrentUrl() . "'>" . $this->helper('catalogsearch')->getEscapedQueryText() . "</a>";
		$msg = "Showing results for '" . $this->helper('catalogsearch')->getEscapedQueryText() . "'.";
		
		// Append spellcheck results
		$suggest = Mage::registry('suggest');
		if (Mage::getStoreConfig(Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS . '/spell_enabled') 
			&& count($suggest) && (Mage::registry('count') == 0))
		{
			$temp = " Search instead for ";
			$numItems = count($suggest);
			$i = 0;
			foreach($suggest as $word)
			{
				$temp .= "<a href='" . Mage::getBaseUrl() . "catalogsearch/result?q=" . $word . "'>" . $word . "</a>";
				if (++$i < $numItems) { $temp .= ', '; }				
			}
			$msg .= $temp . ".";
		}
		return $msg;
	}

}