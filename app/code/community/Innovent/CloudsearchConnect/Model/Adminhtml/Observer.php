<?php
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */
require Mage::getBaseDir('lib') . '/aws.phar';

/**
 * Adminhtml observer
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author 		Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Model_Adminhtml_Observer
{
	protected $_awsDirs;
	
	public function __construct()
	{
		if (!empty($this->_awsDirs))
		{
			//Mage::log('Adding AWS directories');
			$this->_getDirectory(Mage::getBaseDir('lib') . '/Amazon');
		}
	}
	
	public function loadAWSLibraries(Varien_Event_Observer $observer)
	{
		//Mage::log('In Load AWSLibraries');

		//we need to manually include Zend_Loader, or else our zend autoloader
		//will try to use it, won't find it, and then try to use Zend_Loader to
		//load Zend_Loader
		require_once('Zend/Loader.php');
		$autoLoader = Zend_Loader_Autoloader::getInstance();
		
		//get a list of all the registered autoloader callbacks
		//and pull out the Varien_Autoload.  It's unlikely there
		//are others, but famous last words and all that
		$autoloader_callbacks = spl_autoload_functions();
		$original_autoload=null;
		foreach($autoloader_callbacks as $callback)
		{
			if(is_array($callback) && $callback[0] instanceof Varien_Autoload)
			{
				$original_autoload = $callback;
			}
		}
		
		//remove the Varien_Autoloader from the stack
		spl_autoload_unregister($original_autoload);
		
		//register our autoloader, which gets on the stack first
		///require_once('library/EZComponents/Base/src/base.php');
		$autoLoader->unshiftAutoloader(array($this, 'awsAutoload'));
		
		//IMPORANT: add the Varien_Autoloader back to the stack
		spl_autoload_register($original_autoload);
	}
	
	public function awsAutoload($className)
	{
		if (strpos($className, "_")) { return; }
				
		//Mage::log('In awsAutoload for ' . $className);
		if (strpos($className, '\\')) 
		{
			$file = Mage::getBaseDir('lib') . '/amazon/' . str_replace('\\', '/', $className) . '.php';
			if(file_exists($file))
			{
				require_once($file);
				return true;
			}
		}
		
		foreach($this->_awsDirs as $directory)
		{
			//see if the file exsists
			$file = $directory . '/' . $className . '.php';
			if(file_exists($file))
			{
				require_once($file);
				return true;
			}
		}
		return false;
	}
	
	protected function _getDirectory($path)
	{
		$ignore = array( 'cgi-bin', '.', '..' );
		$dh = @opendir($path);
		while( false !== ( $file = readdir( $dh )))
		{
			// Loop through the directory
	 
			if( !in_array( $file, $ignore ))
			{
				//$spaces = str_repeat( '&nbsp;', ( $level * 4 ) );		 
				if( is_dir( "$path/$file" ))
				{
					// Its a directory, so we need to keep reading down...
				 	if (!empty($this->_awsDirs))
				 	{
				 		if (!in_array($path . '/' . $file, $this->_awsDirs)) { $this->_awsDirs[] = $path . '/' . $file; }
				 		$this->_getDirectory( "$path/$file", ($level+1) );				 			
				 	}
				 	else
				 	{
				 		$this->_awsDirs[] = $path . '/' . $file; //substr($path,3) . '/' . $file;
				 		$this->_getDirectory( "$path/$file", ($level+1) );				 			
				 	}
				}
			}
		 
		}
		 
		closedir( $dh );
	}
	
	/**
	 * Adds search weight parameter in attribute form.
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function eavAttributeEditFormInit(Varien_Event_Observer $observer)
	{
		/** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
		$attribute = $observer->getEvent()->getAttribute();
		$form = $observer->getEvent()->getForm();
		$fieldset = $form->getElement('front_fieldset');
	
		$fieldset->addField('search_weight', 'text', array(
				'name' => 'search_weight',
				'label' => Mage::helper('catalog')->__('Attribute Boost'),
				'note' => Mage::helper('search')->__('Use this field to boost the importance of this attribute
						 when searching. This is a decimal(5,3) value.'),
				//'values' => array(),
		), 'is_searchable');
	
		//if ($attribute->getAttributeCode() == 'name') {
		//	$form->getElement('is_searchable')->setDisabled(1);
		//}
	}
	
     /*
     * @param Varien_Event_Observer $observer
     */
    public function requireCatalogsearchReindex(Varien_Event_Observer $observer)
    {
    	Mage::log('In requireCatalogsearchReindex');
		/** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
		$attribute = $observer->getEvent()->getAttribute();
		if ($attribute->getData('search_weight') != $attribute->getOrigData('search_weight')) 
		{
			Mage::getSingleton('index/indexer')->getProcessByCode('catalogsearch_fulltext')
                    ->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
		}
    }

    /**
     * Deletes index if full catalog search reindexation is asked.
     *
     * @param Varien_Event_Observer $observer
     */
    public function beforeIndexProcessStart(Varien_Event_Observer $observer)
    {
    	//Mage::log('In beforeIndexProcessStart');
    	    	    	 
    }   
}