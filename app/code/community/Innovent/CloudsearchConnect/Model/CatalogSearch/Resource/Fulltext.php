<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/*
 * Innovent_CloudsearchConnect_Model_Catalogsearch_Resource_Fulltext
 * Override file for fulltext search - this file adds functionality for 
 * the supported 3rd party search engine.
 *  
 * Copyright 2014, Innovent Solutions, Inc.
 * 
 */
class Innovent_CloudsearchConnect_Model_CatalogSearch_Resource_Fulltext extends Mage_CatalogSearch_Model_Resource_Fulltext 
{
	public function rebuildIndex($storeId = NULL, $productIds = NULL)
	{
		$provider = Mage::getStoreConfig(Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_CURRENT);
		Mage::log('In rebuildIndex for provider: ' . $provider);
		
		if ($provider == Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_ACS)
		{
			$model = Mage::getModel('engine/acs_indexer');
			$model->rebuildIndex($storeId, $productIds);
		}
		else 
		{
			return parent::rebuildIndex($storeId, $productIds);
		}
		
	}

    /**
     * Overloaded method prepareResult. Prepare results for query
     * Replaces the traditional fulltext search with a 3rd party Search (if active)
     *
     * @param Mage_CatalogSearch_Model_Fulltext $object
     * @param string $queryText
     * @param Mage_CatalogSearch_Model_Query $query
     */
    public function prepareResult($object, $queryText, $query) 
    {
		if (!Mage::helper('search')->test())
		{
			Mage::log('Your CloudSearch server is not running or is not responding, using Magento built-in search provider.');
			return parent::prepareResult($object, $queryText, $query);  			 
		}
		
    	$limit = 10;
    	$model = null;
		    	
    	/*
    	 * Determine which search provider to use based on their enabled flags
    	 */
    	$provider = Mage::helper('search')->getActiveProvider();
    	// Mage::log('Provider: ' . $provider);
    	switch ($provider)
    	{
    		case Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_ACS:
    			// Too much logging at inapropriate times at this line
                // Mage::log('Using Acs as search provider');
    			$model = Mage::getModel('engine/acs_search');
    			$limit = Mage::getStoreConfig(Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS . '/search_limit');
    			break;
    		default:
    			Mage::log('Using built-in Magento search provider');
    			return parent::prepareResult($object, $queryText, $query);  			 
    	}
    	
        $adapter = $this->_getWriteAdapter();
        
        $isProcessed = $query->getIsProcessed(); 
        										
        if (!$isProcessed) {
            
            try {
                Mage::log('Provider: ' . $provider);
                Mage::log('Using Acs as search provider');
            	//Mage::log('before loadQuery, query = ' . $queryText);
           		$search = $model->loadQuery($queryText,(int)$query->getStoreId(), $limit);
                if($search->count()) 
                {
                    $products = $search->getProducts();	
                    $data = array();
                    
                    foreach($products as $product) 
                    {
                    	$data[] = array('query_id'   => $query->getId(),
                                        'product_id' => $product['id'],
                                        'relevance'  => $product['relevance']);
                    	 
                    }
                    $adapter->insertMultiple($this->getTable('catalogsearch/result'),$data);
                	$query->setIsProcessed(1);
                }
                $spellCheck = null; /// $search->getSuggest();
                Mage::register('suggest', $spellCheck);
                Mage::register('count', $search->count());
                
            } catch (Exception $e) {
                Mage::log($e->getMessage() . "::" . $e->getTraceAsString(), 7,'search_error.log');
                return parent::prepareResult($object, $queryText, $query);
            }
            
        }
        return $this;
    }

}