<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * AcsIndexer - processes reindex commands from the Magento UI
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author 		Innovent Solutions, Inc.
 */

class Innovent_CloudsearchConnect_Model_Engine_Acs_Indexer
{
	const CRLF = "\r\n";
	const KEY_BASE = Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS;
	const PAGE_SIZE = 200;

	protected $_colNames = array();
	protected $_engine;
	protected $_prodAttrs;
	protected $_errorMsgs = array();

	protected $_processMsgs = array();


    public function __construct()
	{
		$this->_prodAttrs = Mage::helper('search')->getAttributes();
		
		$temp = array();
		foreach($this->_prodAttrs as $attr){
			$temp[] = $attr->getAttributeCode();
		}
		asort($temp);
		$this->_colNames = $temp;

	}


	protected function changeExtension($filename, $newExtension)
	{
		$file = pathinfo($filename);
		if ($file['extension'] != $newExtension)
		{
			$filename = $file['dirname'] . '/' . $file['filename'] . '.' . $newExtension;
		}
		return $filename;
	}

	protected function makeVersionedFilename($filename, $newExtension, $version)
	{
		$file = pathinfo($filename);
		return  $file['dirname'] . '/' . $file['filename'] . '_' . $version . '.' . $newExtension;
	}


	/**
	 * checkIndexStatus
	 * Gets the status of the CloudSearch domain
	 *
	 * @access public
	 * @return temp filename of CSV file
	 * @author Innovent Solutions, Inc.
	 *
	 */
	/**
	 * createIndex
	 * Creates a complete CloudSearch domain
	 *
	 * @access public
	 * @return
	 * @author Innovent Solutions, Inc.
	 *
	 */
	public function createIndex(){
		Mage::log('Indexer->createIndex : Start');
		$domainName = Mage::getStoreConfig(self::KEY_BASE . '/domain');
		$engine = Mage::helper('search')->getEngine();

		// Create the new domain - won't do anything if domain name already exists in this region
		$response = $engine->createDomain(array('DomainName' => $domainName));
		$arr = $response->toArray();
		if ($arr['DomainStatus']['Created'] != 1)
		{
			// The new domain wasn't created, or there was some other error, so return
			$msg = 'CloudSearch Domain was not created, check the error log for more information. ';
			Mage::log($msg);
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__($msg));
			return;

		}

		// If a suggester exists for this domain, delete it so any assigned field can be removed
		$response = $engine->deleteSuggester(array('DomainName'=>$domainName,'SuggesterName'=>'autocomplete'));

		// Next, remove any existing fields in this domain (should indexable attribs have been changed
		// in Magento, deleting and re-adding fields will fix this)
		$deleteFail = false;
		$response = $engine->describeIndexFields(array('DomainName'=>$domainName));
		$arr = $response->toArray();
		foreach($arr['IndexFields'] as $field)
		{
			$response = $engine->deleteIndexField(array('DomainName'=>$domainName, 'IndexFieldName'=>$field['Options']['IndexFieldName']));
			$arr1 = $response->toArray();
			if ($arr1['IndexField']['Status']['PendingDeletion'] != true)
			{
				$deleteFail = true;
				$msg = 'Index field [' . $field['Options']['IndexFieldName'] . '] was not deleted successfully.';
				Mage::log($msg);
			}
		}
		if ($deleteFail)
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__('One or more index fields for this domain could not be deleted. See the system log for more information.'));
		}

		// Now define all of the fields for this index. First do the default fields we need
		// $collection = $this->buildCollection(10);   // Why is this here???? A.B. 1662015
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'id','IndexFieldType'=>'text')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'sku','IndexFieldType'=>'text')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'name','IndexFieldType'=>'text')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'category','IndexFieldType'=>'text-array')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'store_id','IndexFieldType'=>'text-array')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'imageurl','IndexFieldType'=>'text')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'url','IndexFieldType'=>'text')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'autocomplete','IndexFieldType'=>'literal')));
		$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
				'IndexFieldName'=>'longtimestamp_l','IndexFieldType'=>'double')));



		// Now Loop through other attribs and add them as fields
		$productAttrs = $this->_prodAttrs;


		foreach ($productAttrs as $productAttr)
		{		
			$dType = $this->getBackendType($productAttr);
			if ($productAttr->getIsSearchable()) {
				$response = $engine->defineIndexField(array('DomainName'=>$domainName, 'IndexField'=>array(
					'IndexFieldName'=>$productAttr->getName(),'IndexFieldType'=>'text')));
			}
		}

		// catchall field to ignore fields not presented previously, but which may creep in
		// http://docs.aws.amazon.com/aws-sdk-php/latest/class-Aws.CloudSearch.CloudSearchClient.html#_defineIndexField
		$response = $engine->defineIndexField(array(
				'DomainName'=>$domainName,
				'IndexField'=>array(
					'IndexFieldName'=>'*',
					'IndexFieldType'=>'literal',
					'LiteralOptions' => array(
								'FacetEnabled' => false,
								'SearchEnabled' => true,
								'ReturnEnabled' => true,
								'SortEnabled' => false,
								)
				)
			)
		);

		// Now, set the default access policies for the new domain
		$response = $engine->UpdateServiceAccessPolicies(array('DomainName'=>$domainName,'AccessPolicies'=>$this->_getAccessPolicies($domainName)));

		// Add the suggester
		$response = $engine->DefineSuggester(array('DomainName'=>$domainName, 'Suggester'=>$this->_getSuggester()));

		// Finally start the indexdocuments process on this domain
		$response = $engine->IndexDocuments(array('DomainName'=>$domainName));

		Mage::helper('search')->setCronEnabled();
		Mage::log('Indexer->createIndex : End');
		return 1;
	}

	protected function _getSuggester()
	{
		$array = array('SuggesterName'=>'autocomplete',
				'DocumentSuggesterOptions'=>array('SourceField'=>'name','FuzzyMatching'=>'low'));
		return $array;
	}

	protected function _getAccessPolicies($domain)
	{
		$version = '2012-10-17'; //date('H:i:s m/d/Y');
		$region = Mage::getStoreConfig(self::KEY_BASE . '/region');
		$ip = Mage::getStoreConfig(self::KEY_BASE . '/client_ip');
		$policies = '{"Version":"' . $version . '","Statement":[ ';
		$policies .= '{"Sid":"public_search",
				"Effect":"Allow",
				"Principal":"*",
				"Action":["cloudsearch:search"]}, ';
		$policies .= '{"Sid":"CloudsearchConnect Default Doc Policy",
				"Effect":"Allow",
				"Principal":"*",
				"Action":["cloudsearch:document"],
				"Condition":{"IpAddress":{"aws:SourceIp":"' . $ip . '/32"}}}]}';
		return $policies;
	}

	/*
	 *  createMultiVFields($array)
	 *  Takes an input array and returns a number of <field></field>
	 *  definitions separated by CRLF.
	 *
	 *  @access protected
	 *  @return string
	 *  @author Innovent Solutions, Inc.
	 */
	protected function createMultiVFields($array, $fieldName)
	{
		$fields = '';
		foreach($array as $item)
		{
			$fields .= '<field name=' . $fieldName . '>' . $item . '</field>' . self::CRLF;
		}
		return $fields;
	}






	/***
	* Truncate catalogsearch_result table, and set is_processed=0 on catalogsearch_query to clear cache upon indexing
	* abillavara@innoventsolutions.com
	***/
	public function clearTables(){
		$mysqli = $this->_getConnection('core_write');
		$tableprefix = Mage::getConfig()->getTablePrefix();

		$sql_truncate="DELETE FROM " . $tableprefix. "catalogsearch_result";
		$sql_update="UPDATE " . $tableprefix. "catalogsearch_query SET is_processed=0";
		$result = $mysqli->query($sql_update);
		$result = $mysqli->query($sql_truncate);
	}

	public function getErrorMsgs()
	{
		return $this->_errorMsgs;
	}

	public function getProcessMsgs()
	{
		return $this->_processMsgs;
	}


    /**
    * Retrieve resource
    *
    * @return Mage_Core_Model_Resource
    */
    public function _getResource() {
        return Mage::getSingleton('core/resource');
    }

    /**
    * Retrieve connection
    *
    * @return Varien_Db_Adapter_Pdo_Mysql
    */
    public function _getConnection() {
        return $this->_getResource()->getConnection('core_read');
    }

    /**
    * Retrieve table name
    *
    * @return string
    */
    public function _getTable($tableName){
        return $this->_getResource()->getTableName($tableName);
    }

    /*
     * getBackendType
     * Returns a data type for the input attribute
     *
     * @param Magento Attribute
     * @return string
     */
    protected function getBackendType($attr)
    {
    	switch ($attr->getBackendType())
    	{
    		case 'text':
    			$dType = 'text'; break;
    		case 'varchar':
    			$dType = 'text'; break;
    		case 'int':
    			$dType = 'int';	break;
    		case 'static':
    			$dType = 'literal-array'; break;
    		case 'decimal':
    			$dType = 'double'; break;
    		case 'default':
    			$dType = 'text'; break;
    	}
    	return $dType;
    }


/* ************************************************************************************ */
/* Indexing with pagination -- Redo of the Entire indexing process
/* ************************************************************************************ */

	function getListofStoreIds(){
		return array(1, 13);
		$storeIdgroup = array();
		foreach (Mage::app()->getWebsites() as $website){ 
			foreach ($website->getGroups() as $group) { 
				$stores = $group->getStores(); 
				foreach ($stores as $store) { 
					array_push($storeIdgroup, $store->getId());
				} 
			} 
		}
		return $storeIdgroup;
	}

	/**
	 * createXML
	 * Creates an XML file from the product collection
	 *
	 * @access public
	 * @return temp filename of CSV file
	 * @author Innovent Solutions, Inc.
	 *
	 */
	public function createXML()
	{
		Mage::log('AcsIndexer->createXML : Start');
		$storeId = Mage::app()->getStore()->getStoreId();
		// $storeIdgroup = Mage::app()->getStore()->getGroup()->getStoreIds();
		$start = time();
		$storeIdgroup = $this->getListofStoreIds();

		foreach($storeIdgroup as $sid){
		$page = 1;
			while( ($products = $this->buildCollection($page, $sid)) !== null ){
				$this->formatForXML($products, false, $page, $sid);
				if( count($products) < self::PAGE_SIZE ){
					unset($products);
					break;
				}else{
					unset($products);
				}
				$page++;
				//if($page > 2) break;
			}
		}
		$this->_processMsgs[] = 'This operation has created ' . ($page-1) . ' file(s) in the export directory.';

		Mage::log('AcsIndexer->createXML : End, time = ' . (time() - $start) . ' seconds');
		return 1;
	}

	/**
	 * createCSV
	 * Creates a CSV file from the product collection
	 *
	 * @access public
	 * @return 0 if success
	 * @author Innovent Solutions, Inc.
	 *
	 */
	public function createDelimited(){
		Mage::log('Indexer->createDelimited : Starting New CSV Create Delimited files export');
		$page = 1;
		while( ($products = $this->buildCollection($page)) !== null ){
			//$products = $this->buildCollection();
			$outData = null;
			$outHeader = null;
			$storeId = Mage::app()->getStore()->getStoreId();

			$filename = Mage::getBaseDir('export') . '/' . $page . '_' . Mage::getStoreConfig(self::KEY_BASE . '/index_filename');
			$file = pathinfo($filename);
			//Mage::log('Creating data feed file at ' . $filename);

			$data = $this->formatDelimited($products);
			$writeFileResponse = Mage::helper('search')->writeFile($file['dirname'], $page . '_' . $file['basename'], $data);
			if( count($products) < self::PAGE_SIZE ){
				unset($products);
				break;
			}else{
				unset($products);
			}
			$page++;
			//if($page > 2) break;
		}
		return $writeFileResponse;
	}

	/*
    * Refresh Acs Index
    *
    */
    public function rebuildIndex(){
    	$domainName = Mage::getStoreConfig(self::KEY_BASE . '/domain');
    	$search = Mage::getModel('engine/acs_search');
    	$engine = Mage::helper('search')->getEngine();
    	$nowtimestamp = time();

    	Mage::log('Indexer->rebuildIndex : Starting New rebuildIndex');
        try{
            $search->deleteAllDocuments();
		$storeIdgroup = $this->getListofStoreIds();

		foreach($storeIdgroup as $sid){
		$page = 1;

            // Gather data, format into xml, and upload in chunks to Acs
			while( ($products = $this->buildCollection($page, $sid)) !== null ){
				$response = $this->formatForXML($products, true, $page++, $sid);
				$engine->IndexDocuments(array('DomainName'=>$domainName));
				if( count($products) < self::PAGE_SIZE ){
					unset($products);
					break;
				}else{
					unset($products);
				}
				//if($page > 2) break;
			}
		}


            if (strpos($response, 'success') == false){
            	Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__('CloudSearch upload did not complete successfully, check the error log for more information.'));
            }else{
            	Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('search')->__('CloudSearch upload has completed successfully.'));
            }

			//$this->deleteOldDocsFromACS($nowtimestamp);   
            $this->clearTables();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('search')->__('Cannot index data in CloudSearch. %s',$e->getMessage()));
        }

    	Mage::log('Indexer->rebuildIndex : End New rebuildIndex, time = ' . (time() - $nowtimestamp) . ' seconds');
        return;
    }

	/**
	 *  Find ID's of all documents not updated in this run, but that was built before this index 
	 *	They are most probably not active in the index any longer
	 *	Then delete them
	 */
	protected function deleteOldDocsFromACS( $nowtimestamp ){
		// 
		$findIDquery = 'q=all|-all&fq=longtimestamp_l:[0,' . ($nowtimestamp - 1)  . ']&return=id&size=10000';

		$responseIds = Mage::getModel('engine/acs_search')->search($findIDquery, $responseCode, $responseInfo);
		$delIdstr = '[';
		$i=0;

		foreach ( $responseIds{'hits'}{'hit'} as $hit ){
			if($i>0) $delIdstr .= ',';
			$delIdstr .= '{ "type":"delete", "id":"'. $hit{'id'} .'"}';
			$i++;
		}
		$delIdstr .= ']';
		//echo "<BR>delstr = " . $delIdstr; die();		
		$response = $this->curlACS($delIdstr);
	}

	/**
	 *  Build product/attribute collection for use in other functions
	 *
	 */
	protected function buildCollection($page = 1, $storeid = 1)
	{
		Mage::log('AcsIndexer->buildCollection : Start');
		
		Mage::app()->setCurrentStore($storeid);

		$products = Mage::getModel('catalog/product')->getCollection()
		->addAttributeToSelect($this->_colNames)
		->addAttributeToFilter('status', 1)
		->addAttributeToFilter(array(
				array('attribute'=>'visibility', 'like' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH),
				array('attribute'=>'visibility', 'like' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH)
		))
		->setPageSize(self::PAGE_SIZE )
		->setCurPage($page)
		// ->addStoreFilter($storeid)
		->addUrlRewrite()
		->load();

		Mage::log('AcsIndexer->buildCollection : End ' . count($products) . ' products in collection fore storeid = '. $storeid);
		return $products;
	}


	/**
	 * formatForXML
	 * Takes the product collection and formats an XML document, writing file chunks as appropriate
	 *
	 * @access private
	 * @return String of formatted data
	 * @author Innovent Solutions, Inc.
	 *
	 */
	protected function formatForXML($collection, $doUpload = false, $page = 1, $storeid = 1)
	{
		Mage::log('AcsIndexer->formatForXML : Start page = '. $page);

		$_baseId = 'acs_autoload_';

		// Add a root node to the document
		$root = '<batch>';

		// Loop through each row creating a <row> node with the correct data
		$counter = 0;
		$fileCount = $page;
		$outFiles = array();
		$response = null;


		foreach($collection as $item)
		{
			if ($item->getId() == null) { continue; }
			$counter++;
			$cats = Mage::helper('search')->getCategories($item);

			$container = self::CRLF . '<add id="' . $item->getId() . '_' . $storeid . '">';
			$container .= '<field name="id">' . $item->getId() . '_' . $storeid . '</field>';
			$container .= '<field name="longtimestamp_l">'. time() . '</field>';

			$oldcat = "";
			foreach($cats as $cat)
			{
				if($oldcat == $cat) continue;
				$container .= '<field name="category">' . htmlentities($cat) . '</field>';
				$oldcat = $cat;
			}

			// A.B. 12/30/2015 : Multi-Store. Storeid is passed in
			//foreach($item->getStoreIds() as $id){
				$container .= '<field name="store_id">'. $storeid . '</field>';
			//}

			$sku = $item->getSku();

			// Setup the autocomplete field with name of products
			//$container .= '<field name="name">'. htmlentities($item->getName()) . '</field>';
			//$container .= '<field name="autocomplete">'. htmlentities($item->getName()) . '</field>';
                        $container .= '<field name="autocomplete"><![CDATA['. htmlentities($item->getName()) . ']]></field>';

			// Now add in the image and url fields
			if (!empty($sku))
			{
				/* *************** IMG *****************/
				// $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
				// A.B 12/29/2015 : This loads 15 files where the above loaded only 9 XML files.
				$product = Mage::getModel('catalog/product')->load($item->getid());
				$img = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getImage());


				$container .= '<field name="imageurl">'. $img . '</field>';

				/* *************** Product URL *****************/
				$prodUrl = $product->getProductUrl();
				// $prodUrl = Mage::getResourceSingleton('catalog/product')->getAttributeRawValue($item->getId(), 'url_key', Mage::app()->getStore());
				
				$container .= '<field name="url">'. $prodUrl . '</field>';


				$attributes = $product->getAttributes();
				$precision = 2;
				foreach ($attributes as $attribute) {
					//var_dump($attribute);die();
					if ($attribute->getIsSearchable()) {
						$val = $attribute->getFrontend()->getValue($product);
						if($val != '' && $val != null){
							if("price" == strtolower($attribute->getName()) ){
								$val = number_format((float) $val, $precision, '.', '')  . ''; 
							}else{
								$val = '<![CDATA[' . htmlentities($val) . ']]>';
							}
							$container .= '<field name="' . $attribute->getName() . '">'. $val . '</field>';
						}
					}
				}

			}


			$container .= "</add>";
			$root .= $container;

		} // End Main For Loop.
		$root .= self::CRLF . "</batch>";



		// Plain & and business signs like TM & REG & Copyright
		$root = str_replace("& ", "&amp; ", $root);
		$root = str_replace("&nbsp;", " ", $root);
		$root = str_replace("&reg;", "&amp;reg;", $root);
		$root = str_replace("&trade;", "&amp;trade;", $root);
		$root = str_replace("&copy;", "&amp;copy;", $root);
		$root = str_replace("&deg;", "&amp;deg;", $root);


		// Fancy quotes Single
		$root = str_replace("&lsquo;", "&amp;lsquo;", $root);
		$root = str_replace("&rsquo;", "&amp;rsquo;", $root);
		// Fancy quotes double
		$root = str_replace("&quot;", "&amp;quot;", $root);
		$root = str_replace("&ldquo;", "&amp;ldquo;", $root);
		$root = str_replace("&rdquo;", "&amp;rdquo;", $root);
		$root = str_replace("&laquo;", "&amp;laquo;", $root);
		$root = str_replace("&raquo;", "&amp;raquo;", $root);

		// Other fancy docx chars
		$root = str_replace("&ndash;", "&amp;ndash;", $root);
		$root = str_replace("&mdash;", "&amp;mdash;", $root);
		$root = str_replace("&iexcl;", "&amp;iexcl;", $root);
		$root = str_replace("&iquest;", "&amp;iquest;", $root);
		$root = str_replace("&cent;", "&amp;cent;", $root);
		$root = str_replace("&divide;", "&amp;divide;", $root);
		$root = str_replace("&gt;", "&amp;gt;", $root);
		$root = str_replace("&lt;", "&amp;lt;", $root);
		$root = str_replace("&micro;", "&amp;micro;", $root);
		$root = str_replace("&middot;", "&amp;middot;", $root);
		$root = str_replace("&para;", "&amp;para;", $root);
		$root = str_replace("&plusmn;", "&amp;plusmn;", $root);
		$root = str_replace("&euro;", "&amp;euro;", $root);
		$root = str_replace("&pound;", "&amp;pound;", $root);
		$root = str_replace("&sect;", "&amp;sect;", $root);
		$root = str_replace("&yen;", "&amp;yen;", $root);
		$root = str_replace("&deg;", "&amp;deg;", $root);
		
		// Accents & Umlauts
		$root = str_replace("&aacute;", "&amp;aacute;", $root);
		$root = str_replace("&Aacute;", "&amp;Aacute;", $root);
		$root = str_replace("&agrave;", "&amp;agrave;", $root);
		$root = str_replace("&Agrave;", "&amp;Agrave;", $root);
		$root = str_replace("&acirc;", "&amp;acirc;", $root);
		$root = str_replace("&Acirc;", "&amp;Acirc;", $root);
		$root = str_replace("&aring;", "&amp;aring;", $root);
		$root = str_replace("&Aring;", "&amp;Aring;", $root);
		$root = str_replace("&atilde;", "&amp;atilde;", $root);
		$root = str_replace("&Atilde;", "&amp;Atilde;", $root);
		$root = str_replace("&auml;", "&amp;auml;", $root);
		$root = str_replace("&Auml;", "&amp;Auml;", $root);
		$root = str_replace("&aelig;", "&amp;aelig;", $root);
		$root = str_replace("&AElig;", "&amp;AElig;", $root);
		$root = str_replace("&ccedil;", "&amp;ccedil;", $root);
		$root = str_replace("&Ccedil;", "&amp;Ccedil;", $root);
		$root = str_replace("&eacute;", "&amp;eacute;", $root);
		$root = str_replace("&Eacute;", "&amp;Eacute;", $root);
		$root = str_replace("&egrave;", "&amp;egrave;", $root);
		$root = str_replace("&Egrave;", "&amp;Egrave;", $root);
		$root = str_replace("&ecirc;", "&amp;ecirc;", $root);
		$root = str_replace("&Ecirc;", "&amp;Ecirc;", $root);
		$root = str_replace("&euml;", "&amp;euml;", $root);
		$root = str_replace("&Euml;", "&amp;Euml;", $root);
		$root = str_replace("&iacute;", "&amp;iacute;", $root);
		$root = str_replace("&Iacute;", "&amp;Iacute;", $root);
		$root = str_replace("&igrave;", "&amp;igrave;", $root);
		$root = str_replace("&Igrave;", "&amp;Igrave;", $root);
		$root = str_replace("&icirc;", "&amp;icirc;", $root);
		$root = str_replace("&Icirc;", "&amp;Icirc;", $root);
		$root = str_replace("&iuml;", "&amp;iuml;", $root);
		$root = str_replace("&Iuml;", "&amp;Iuml;", $root);
		$root = str_replace("&ntilde;", "&amp;ntilde;", $root);
		$root = str_replace("&Ntilde;", "&amp;Ntilde;", $root);
		$root = str_replace("&oacute;", "&amp;oacute;", $root);
		$root = str_replace("&Oacute;", "&amp;Oacute;", $root);
		$root = str_replace("&ograve;", "&amp;ograve;", $root);
		$root = str_replace("&Ograve;", "&amp;Ograve;", $root);
		$root = str_replace("&ocirc;", "&amp;ocirc;", $root);
		$root = str_replace("&Ocirc;", "&amp;Ocirc;", $root);
		$root = str_replace("&oslash;", "&amp;oslash;", $root);
		$root = str_replace("&Oslash;", "&amp;Oslash;", $root);
		$root = str_replace("&otilde;", "&amp;otilde;", $root);
		$root = str_replace("&Otilde;", "&amp;Otilde;", $root);
		$root = str_replace("&ouml;", "&amp;ouml;", $root);
		$root = str_replace("&Ouml;", "&amp;Ouml;", $root);
		$root = str_replace("&szlig;", "&amp;szlig;", $root);
		$root = str_replace("&uacute;", "&amp;uacute;", $root);
		$root = str_replace("&Uacute;", "&amp;Uacute;", $root);
		$root = str_replace("&ugrave;", "&amp;ugrave;", $root);
		$root = str_replace("&Ugrave;", "&amp;Ugrave;", $root);
		$root = str_replace("&ucirc;", "&amp;ucirc;", $root);
		$root = str_replace("&Ucirc;", "&amp;Ucirc;", $root);
		$root = str_replace("&uuml;", "&amp;uuml;", $root);
		$root = str_replace("&Uuml;", "&amp;Uuml;", $root);
		$root = str_replace("&yuml;", "&amp;yuml;", $root);
	

		if ($doUpload)
		{
			/* **************************
			* Index directly into ACS
			*************************** */
			Mage::log('Beginning upload of data chunk');
			$response = Mage::getModel('engine/acs_search')->upload( $root );
			
			if (strpos($response,'status="success"') === False)
			{
				Mage::log('Error uploading data file at around row ' . $counter . ', will continue.');
				$this->_errorMsgs[] = 'Error uploading data file at around row ' . $counter . ', will continue.';
			}
			Mage::log('Finished upload of data chunk, page = '. $page);
		}
		else
		{
			/* **************************
			* Create XML File for it
			*************************** */

			$filename = $this->makeVersionedFilename(Mage::getBaseDir('export') . '/' . Mage::getStoreConfig(self::KEY_BASE . '/index_filename'),'XML', $fileCount . "_" .$storeid);
			$file = pathinfo($filename);

			Mage::log('Creating data feed file at ' . $filename);

			$outFiles[$fileCount++] = $filename;
			Mage::helper('search')->writeFile($file['dirname'], $file['basename'], $root );

		}


		if (libxml_use_internal_errors(true) === true) // previous setting was true?
		{
			libxml_clear_errors();
		}
		unset($collection);

		Mage::log('Finished processing ' . $counter . ' records');


		if ($doUpload == false)
		{
			return 1;
		}
		else
		{
			return $response;
		}

	}

	function precision($num) { 
		return strlen(substr($num, strpos($num, '.')+1)); 
	} 
	
	/**
	 * formatDelimited
	 * Takes the product collection and formats for delimited txt with headers
	 *
	 * @access private
	 * @return String of formatted data
	 * @author Innovent Solutions, Inc.
	 *
	 */
	protected function formatDelimited($collection)
	{
		Mage::log('In AcsIndexer, formatForTXT');
		$delim = Mage::helper('search')->getDelimiter(Mage::getStoreConfig(self::KEY_BASE . '/field_sep'));
		$headerDone = false;
		$data = '';

		// Build product rows
		foreach($collection as $item)
		{
			$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
			$img = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getImage());

			$cats = implode(',', Mage::helper('search')->getCategories($item));
			$stores = implode(Mage::helper('search')->getDelimiter(Mage::getStoreConfig(self::KEY_BASE . '/multivalue_sep')), $item->getStoreIds());

			// Push values into an attribute array for sorting later
			$attribs = array( 'id' => $product->getId(),
								//'sku' => $item->getSku(),
								//'short_description' => $product->getShortDescription(),
								'category' => $cats,
								'store_id' => $stores,
								'url' => $product->getProductUrl(),
								'imageurl' => $img );

			for($i=0;$i<count($this->_colNames); $i++)
			{
				$attribs[$this->_colNames[$i]] = $item[$this->_colNames[$i]];
			}

			$row = '';
			$hrow = '';
			$aCount = count($attribs);
			$i = 0; $ii = 0;

			foreach($attribs as $key => $value)
			{
				if ($headerDone == false)
				{
					// Build header row
					$hrow .= $key;
					if (++$i < $aCount) { $hrow .= $delim; }
				}
				$row .= $value;
				if (++$ii < $aCount) { $row .= $delim; }
			}

			if ($headerDone == false)
			{
				$headerDone = true;
				$data .= $hrow . self::CRLF;
			}

			$data .= $row . self::CRLF;
		}

		return $data;
	}

	private function buildUploadURL(){
		$docURL = 'http://' . Mage::getStoreConfig(self::KEY_BASE . '/document_endpoint') . '/' . Mage::getStoreConfig(self::KEY_BASE . '/api_version') . '/documents/batch';
		echo "<BR>". $docURL;
		return $docURL;
	}
	
    private function curlACS($data){
    	$verbose = fopen('php://temp', 'rw+');
    	
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    	curl_setopt($ch, CURLOPT_URL, $this->buildUploadURL());
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_STDERR, $verbose);
    	
    	// Set the appropriate header fields - required headers are 
    	// content-type, content-length & host. Optionals are accept (xml or json).
    	curl_setopt($ch, CURLOPT_HTTPHEADER, 
    		array(
    			'Host: ' . Mage::getStoreConfig(self::KEY_BASE . '/document_endpoint'),
    			'Content-Type: application/json',
    			'Content-Length: ' . strlen($data)
    		));   	 
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
   		$response = curl_exec($ch);
		//Mage::log('Data: ' . var_dump($data));

   		echo ('<BR>CloudSearch upload response: ' . var_export($response,true));
    	curl_close($ch);
    	//rewind($verbose);
    	//$verboseLog = stream_get_contents($verbose);
    	//Mage::log("Verbose information: " . htmlspecialchars($verboseLog));
    	
    	return $response;
    }


}

