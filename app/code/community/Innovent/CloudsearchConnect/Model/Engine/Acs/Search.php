<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

class Innovent_CloudsearchConnect_Model_Engine_Acs_Search extends Innovent_CloudsearchConnect_Model_Engine_Abstract
{
    protected $_response;
    protected $_count;
    protected $_products;
    protected $_domain;
    protected $_aws;
    protected $_version;
    protected $_searchEndpoint;
    protected $_docEndpoint;
    protected $_returnFields;
    protected $_engine;
	protected $_extraSearchableFields;

    const KEY_BASE = Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS;
      
    /**
     * Constructor, retrieve config for connection to the Amazon server.
     */
    public function __construct() 
    {
		$this->_engine = Mage::helper('search')->getEngine();
    	
		$this->_domain = Mage::getStoreConfig(self::KEY_BASE . '/domain');
	    $this->_version =  Mage::getStoreConfig(self::KEY_BASE . '/api_version');
	    $this->_searchEndpoint =  Mage::getStoreConfig(self::KEY_BASE . '/search_endpoint');
	    $this->_docEndpoint =  Mage::getStoreConfig(self::KEY_BASE . '/document_endpoint');
	    $this->_returnFields = Mage::getStoreConfig(self::KEY_BASE . '/return_fields');
	    $this->_extraSearchableFields = Mage::getStoreConfig(self::KEY_BASE . '/extra_searchable_fields');
    }

    public function getRawProducts()
    {
    	return $this->_products;
    }
    
    /**
     * Load query and set response
     * 
     * @param string $query
     * @param int $limit
     */
    public function loadQuery($query,$storeId=0,$limit=10) 
    {		
    	if (Mage::getStoreConfig(self::KEY_BASE . '/return_fields') != null)
    	{
    		$fields = Mage::getStoreConfig(self::KEY_BASE . '/return_fields');
    		$fields .= ',id,_score';
    	}
    	else
    	{
    		$fields = 'id,_score';
    	}
    	
    	///$url = $this->buildSearchUrl();
    	$url = '';
    	
    	// For CloudSearch, we must split all search terms at a space and replace with a plus "+"

		$url .= 'q=' . str_replace(' ', '+', urlencode($query) );
		$url .= '&return=' . $fields;
		
		// Add in the boost - field weighting
		$temp = '';
		$temp .= '&q.options={fields: [' . $this->getBoostQuery() . ']}';
		$temp .= "&q.parser=dismax";
		// $temp .= "&fq=store_id:" . $storeId;
		$url .= $temp;		
		
		// Add in the number of rows option
		$url .= '&size=' . Mage::getStoreConfig(self::KEY_BASE . '/search_limit');
		// Mage::log('In AcsSearch::loadQuery url = ' . $url);

		$response = $this->search($url, $responseCode, $responseInfo);
		if ($responseCode == 200)
		{
			$this->setLocalVars($response);
				
			/* *************************************************************************
			* A.B. 16 Feb 2015 
			* Redirect to product page if only one product found, AND it has a URL 
			* To Activate :
			* Set the URL in Admin UI > Catalog > INnovent Cloudsearch > General > 
			* Additional Fields = sku,name,url
			************************************************************************* */
			if( $this->count() == 1 && isset($response['hits']['hit'][0]['fields']['url']) && $response['hits']['hit'][0]['fields']['url'] !== ''){
		
				//get the http response object
				$rsp = Mage::app()->getFrontController()->getResponse();
				
				//set the redirect header to the response object
				$rsp->setRedirect($response['hits']['hit'][0]['fields']['url']);
				
				//send the response immediately and exit since there is nothing more to do with the current execution :)
				$rsp->sendResponse();
				exit;				
			}
		
		}
		else 
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__('Search query did not complete successfully, check the error log for more information.'));
			Mage::log('Error executing search query, http response code was ' . $responseCode . ': ' . $responseInfo);	
		}
        return $this;
    }
    
    public function search($query, &$responseCode, &$responseInfo)
    {
    	Mage::log('Search::search query = ' . $query);
    	$verbose = fopen('php://temp', 'rw+');
    	 
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    	curl_setopt($ch, CURLOPT_URL,$this->buildSearchURL());
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_STDERR, $verbose);
    	 
    	// Set the appropriate header fields - required headers are
    	// content-type, content-length & host. Optionals are accept (xml or json).
    	curl_setopt($ch, CURLOPT_HTTPHEADER,array('Host: ' . $this->_searchEndpoint));
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
    	$response = curl_exec($ch);
    	$info = curl_getinfo($ch);
    	 
    	//Mage::log('CloudSearch search response: ' . var_export($response,true));
    	curl_close($ch);
    	///rewind($verbose);
    	///$verboseLog = stream_get_contents($verbose);
    	///Mage::log("Verbose information: " . htmlspecialchars($verboseLog));
    	///echo '<pre>' . print_r(htmlspecialchars($verboseLog), true) . '</pre>';   
    	///die;
    	
    	$responseCode = $info['http_code'];
    	$responseInfo = Mage::helper('search')->getHTTPInfo($info['http_code']);
		$array = json_decode($response,true);
    	return $array; 
    }
    
    /*
     * Upload file to ACS for indexing
     */
    public function upload($data)
    {
    	$verbose = fopen('php://temp', 'rw+');
    	
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    	curl_setopt($ch, CURLOPT_URL, $this->buildUploadURL());
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_STDERR, $verbose);
    	
    	// Set the appropriate header fields - required headers are 
    	// content-type, content-length & host. Optionals are accept (xml or json).
    	curl_setopt($ch, CURLOPT_HTTPHEADER, 
    		array(
    			'Host: ' . $this->_docEndpoint,
    			'Content-Type: application/xml',
    			'Content-Length: ' . strlen($data)
    		));   	 
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
   		$response = curl_exec($ch);
		//Mage::log('Data: ' . var_dump($data));
		Mage::log('Upload data to: ' . $this->buildUploadURL());
   		Mage::log('CloudSearch upload response: ' . var_export($response,true));
    	curl_close($ch);
    	//rewind($verbose);
    	//$verboseLog = stream_get_contents($verbose);
    	//Mage::log("Verbose information: " . htmlspecialchars($verboseLog));
    	
    	return $response;
    }
    
    /*
     * Delete all existing index docs
     * 
     * returns ACS response
     */
    public function deleteAllDocuments()
    {
		Mage::log('deleteAllDocuments');
		$this->getAllDocuments();
		
		// If there are no documents in the index, abort and return true
		if ($this->_count == 0)
		{
			Mage::log('There are no documents to delete.');
			return -1;
		}
		
    	$doc  = new DomDocument();
    	$doc->formatOutput = true;
    
    	// Add a root node to the document
    	$root = $doc->createElement('batch');
    	$root = $doc->appendChild($root);
    
    	foreach($this->getRawProducts() as $item)
    	{
    		$container = $doc->createElement('delete');
    
    		$attr = $doc->createAttribute('id');
    		$attr->value=$item['id'];
    		$container->appendChild($attr);
    
    		$root->appendChild($container);
    	}
		//$tmp = $doc->saveXML();
		//Mage::helper('search')->writeFile('c:\temp', 'xml3.xml', $doc->saveXML());

		$response = Mage::getModel('engine/acs_search')->upload($doc->saveXML());
    	if (strpos($response, 'status="success"'))
    	{
			return 1;
    	}
    	else
    	{
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__('CloudSearch delete all did not complete successfully, check the error log for more information.'));
    		return 0;
    	}
    	 
    }
    
    /*
     * Get all documents query
     * 
     */
    public function getAllDocuments()
    {
    	$query .= 'q=matchall&q.parser=structured&size=0';  	    	
    	$response = $this->search($query,$responseCode,$responseInfo);
    	if ($responseCode == 200)
    	{
    		$count = $response['hits']['found'];
    		 
    		$query = 'q=matchall&q.parser=structured&return=sku&size=' . $count;
    		$response = $this->search($query,$responseCode,$responseInfo);
    		if ($responseCode == 200)
    		{
    			$this->setLocalVars($response); 
    		}
    		else 
    		{
    			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__('CloudsearchConnect had a problem retrieving all record IDs, check the error log for more information.'));
    			$msg = 'Cannot retrieve index document IDs, status = ' . $responseCode . ': ' . $responseInfo;
    			Mage::log($msg);
    		}
    		
    	}
    }
    

    /**
     * getBoostQuery
     * Formats a list of searchable attribs and boost values for this search engine as
     * a delimited string.
     *
     * @return string
     */
    public function getBoostQuery()
    {
    	$i = 1;
    	$temp = '';
    	$numItems = count($this->_getFQArray());
    	
    	if ($numItems > 0)
    	{
    		foreach($this->_getFQArray() as $item)
    		{
    			$key = $item["name"];
    			$value = $item["boost"];
    		
    			$temp .= '"' . $key . '^' . $value . '"';
    		
    			// Append a comma if not the last item
    			if (++$i <= $numItems)
    			{
    				$temp .= ',';
    			}
    		
    		}
    		/*
    		if(isset($this->_extraSearchableFields)){
    			$qfs = explode(' ' , $this->_extraSearchableFields);
    			foreach($qfs as $qf){
					$newqf = explode('^', $qf);
					if( strpos('"', $newqf[0]) === FALSE) {
						$temp .= ', "' . $newqf[0];

						if( !isset($newqf[1]) || (isset($newqf[1]) && !is_numeric($newqf[1])) ){
							$temp .= '^1"'; 
						}else{
							$temp .= '^'. $newqf[1] .'"';
						}
					}else{
						$temp .= ', ' . $this->_extraSearchableFields;
					}
    			}
    
    		}  
    		return $temp;
     		*/  		
   		
    	}
		if(isset($this->_extraSearchableFields)){
			if(strlen($temp) > 0 && strlen($this->_extraSearchableFields) > 0) $temp .= ',"';
			if(strlen($this->_extraSearchableFields) > 0) $temp .= str_replace(',', '","', $this->_extraSearchableFields) . '"';
		}      	
   		return $temp;
    	
    }
        
    /**
     * Set response into local vars
     * 
     */
    private function setLocalVars($response) 
    {
    	 
    	$this->_response = $response;
        $this->_count = $response['hits']['found'];
        $this->_products = $response['hits']['hit'];
        
        //Mage::log('In Search::setLocalVars, response = ' . var_export($response,true));
        //Mage::log('In Search::setLocalVars, count = ' . var_export($this->_count,true));
        //Mage::log('In Search::setLocalVars, products = ' . var_export($this->_products, true));
    }
    
    private function buildSearchURL()
    {
    	$urlHost = 'http://' . $this->_searchEndpoint . '/' . $this->_version . '/search';
    	return $urlHost;
    }

    private function buildUploadURL()
    {
    	$urlHost = 'http://' . $this->_docEndpoint . '/' . $this->_version . '/documents/batch';
    	return $urlHost;
    }
    		
    protected function getJsonError($input)
	{
		switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            echo ' - Unknown error';
        break;
		}		
	
	}
	
    /**
     * Extract product ids and score in ACS response
     * 
     * @return array $ids
     */
    public function getProducts() 
    {
    	$products = array();
    	$relevance = 1;
    	
		foreach($this->_products as $product)
		{
			foreach($product as $key => $value)
			{
				$arr = array();
				 
				switch (strtolower($key))
				{
					case 'id':
						$acsId = $value;
						$id = $value;
						break;
					case 'fields':
						$data = $value;
						$score = $data['_score'];
						break;
				}
			}
			if ($id != null)
			{
				$products[] = array('acs_id'=>$acsId,'id'=>$id,'data'=>$data,'relevance'=>$score);
			}
		}
		//Mage::log('Products found: ' . count($products));
		return $products;
    }

    public function checkIndexStatusCron()
    {      	
		$key = Mage::helper('search')->getCronEnabled();
    	Mage::log('Running check for cron: ' . $key . ' ' . date('m/d/Y H:i:s'));

		if ($key)
    	{
    		$this->checkIndexStatus(true);
    	}
    }
    
    public function checkIndexStatus($fromCron = false)
    {
    	Mage::log('Running checkIndexStatus at ' . date('m/d/Y H:i:s'));
    	
    	$retval = 0;
    	$domainName = Mage::getStoreConfig(self::KEY_BASE . '/domain');
    	$version = Mage::getStoreConfig(self::KEY_BASE . '/api_version');
    	$engine = Mage::helper('search')->getEngine();
    
    	$response = $engine->describeDomains(array('DomainName'=>$domainName));
    	foreach($response['DomainStatusList'] as $domain)
    	{
    		if ($domain['DomainName'] == $domainName)
    		{
    			$processing = '';
    			$requiresIndex = '';
    				
    			if ($domain['Processing'] == 1) { $processing = 'Domain is processing'; }
    			if ($domain['RequiresIndexDocuments'] == 1) { $requiresIndex = 'Domain requires re-indexing, will start this process'; } 
    			$msg = 'Search domain [' . $domainName . '] - ' . $processing . ' ' . $requiresIndex;
    			if ($processing == '' && $requiresIndex == '')
    			{
					$this->setDomainEndpoints();
    				$msg = 'Search domain [' . $domainName . '] is now ready. You may add documents to this index.';
    				$retval = 1;
    			}
    			Mage::log($msg);
    			Mage::getSingleton('adminhtml/session')->addNotice($msg);
    			
    			// If the domain indicates it requires a reindex, start the process
    			if ($domain['RequiresIndexDocuments'] == 1)
    			{
    				Mage::log('Starting index rebuild process for domain [' . $domainName . ']');
    				$response = $engine->IndexDocuments(array('DomainName'=>$domainName));
    			}
    			return $retval;
    		}
    	}
    	$msg = 'Index domain [' . $domainName . '] does not exist for this region and version.';
    	if ($fromCron == false) { Mage::getSingleton('core/session')->addWarning($msg); }
    	return $retval;
    }
    
    /*
     *  Verifies that a index domain exists and returns the two endpoints
     */
    public function verifyDomain($domain, &$docEndpoint, &$searchEndpoint, &$apiVersion)
    {
    	$response = $this->_engine->describeDomains(array('DomainName'=>$domain));
    	$arr = $response->toArray();
    	foreach ($arr['DomainStatusList'] as $domains)
    	{
    		if ($domains['DomainName'] == $domain)
    		{
    			Mage::log('In verify domain, incoming domain = ' . $domain . ', matching domain = ' . $domains['DomainName']);
				//Mage::log(var_export($domains, true));
    			$docEndpoint = $domains['DocService']['Endpoint'];
				$searchEndpoint = $domains['SearchService']['Endpoint'];
				return 1;
    		}
    	}
   		Mage::log('Domain [' . $domain . '] does not exist.');
   		return 0;
    }   
     
    /*
     *  setDomainEndpoints
     *  Sets the doc and search endpoints for the domain
     *  
     *  @return void
     */
    public function setDomainEndpoints()
    {
 
    	// Try and get the search and document endpoints from the domain name
		$config = Mage::getSingleton('search/acsconfig');
    	$domain = $config->getDomain();
    
    	if ($this->verifyDomain($domain,$docEndpoint,$searchEndpoint,$apiVersion) == 1)
    	{
    		Mage::log('Setting domain endpoints, ' . $docEndpoint . ', ' . $searchEndpoint);
    		$config->setDocumentEndpoint($docEndpoint);
    		$config->setSearchEndpoint($searchEndpoint);
    		
			// Delete the innovent.cron file after setting the endpoints
			Mage::helper('search')->stopCron();
    	}
    	else
    	{
    		$config->setDocumentEndpoint(null);
    		$config->setSearchEndpoint(null);
    		Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('search')->__('CloudSearch search domain [' . $domain . '] does not yet exist, please create it.'));
    	}
    	$config->save();
    	Mage::helper('search')->clearCache();
    	 
    }
    
    /**
     * Retreive documents count
     * 
     * @return int
     */
    public function count() {
    	return $this->_count;
    }
    
    /**
     * Test if server configuration is complete
     * 
     * @return bool
     */
    public function isConfigured() {
        return Mage::getStoreConfig(self::KEY_BASE . '/search_endpoint');
    }

    /**
     * Retrieve Store Id
     * 
     * @return int
     */
    public function getStoreId() {
        return Mage::app()->getStore()->getId();
    }

    public function __get($property) {
    	if (property_exists($this, $property)) {
    		return $this->$property;
    	}
    }
    
    public function __set($property, $value) {
    	if (property_exists($this, $property)) {
    		$this->$property = $value;
    	}
    }

    public function commit()
    {
    }
}
