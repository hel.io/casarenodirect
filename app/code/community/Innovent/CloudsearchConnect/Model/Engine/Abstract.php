<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

abstract class Innovent_CloudsearchConnect_Model_Engine_Abstract
{
	const KEY_BASE = ""; // This defines which config data section to use	
	const DEFAULT_ROWS_LIMIT = 9999;
	const UNIQUE_KEY = 'unique';
	
	protected $_response; // This is the response from all search engines
	protected $_engine; // This will contain a reference to the search engine (solr, acs, etc)

	
	/**
	 * @var object Search engine client.
	*/
	protected $_client;
	
	/**
	 * @var array List of dates format.
	 */
	protected $_dateFormats = array();
	
	/**
	 * @var array List of default query parameters.
	*/
	protected $_defaultQueryParams = array(
			'offset' => 0,
			'limit' => 100,
			'sort_by' => array(array('relevance' => 'desc')),
			'store_id' => null,
			'locale_code' => null,
			'fields' => array(),
			'params' => array(),
			'ignore_handler' => false,
			'filters' => array(),
	);
	
	/**
	 * @var array List of indexable attribute parameters.
	*/
	protected $_indexableAttributeParams = array();
	
	/**
	 * @var int Last number of results found.
	*/
	protected $_lastNumFound;
	
	/**
	 * @var array List of non fulltext fields.
	 */
	protected $_notInFulltextField = array(
			self::UNIQUE_KEY,
			'id',
			'store_id',
			'in_stock',
			'categories',
			'show_in_categories',
			'visibility'
	);
	
	/**
	 * @var bool Stores search engine availibility
	*/
	protected $_test = null;
	
	/**
	 * @var array List of used fields.
	 */
	protected $_usedFields = array(
			self::UNIQUE_KEY,
			'id',
			'sku',
			'price',
			'store_id',
			'categories',
			'show_in_categories',
			'visibility',
			'in_stock',
			'score'
	);


	/**
	 * _getFQString
	 *	Returns an array of attrib names and boost values to be formatted in the 
	 *	search class.
	 *
	 * @return array
	 */
	protected function _getFQArray()
	{
		// Get list of searchable attributes
		$searchable = Mage::helper('search')->getSearchableAttributes();
		$ar1 = null;
		
		foreach($searchable as $attrib)
		{
			$code = $attrib->getAttributeCode();
			$ar1[] = array("name"=>$code, "boost"=>$attrib->getSearchWeight());
					
		}
		return $ar1;
	}
		
	/**
	 * Set search engine response
	 *
	 * @param $response
	 */
	protected function setResponse($response) 
	{
		$this->_response = $response;
	}

	/**
	 * Extract product ids and score in Acs response
	 *
	 * @return array $ids
	 */
	protected function getProducts() {
		$products = array();
		foreach($this->_response->docs as $doc) {
			$products[] = array('id'=>$doc->id,'relevance'=>$doc->score);
		}
		//Mage::log('Number of products: ' . count($products));
		return $products;
	}
	
	/**
	 * Test if server configuration is complete
	 *
	 * @return bool
	 */
	protected function isConfigured() 
	{
		return Mage::getStoreConfig(self::KEY_BASE . '/host_name') && Mage::getStoreConfig(self::KEY_BASE . '/port');
	}
	
	/**
	 * Retrieve Store Id
	 *
	 * @return int
	 */
	protected function getStoreId() 
	{
		return Mage::app()->getStore()->getId();
	}
	
	/**
	 * Transforms specified object to an array.
	 *
	 * @param $object
	 * @return array
	 */
	protected function _objectToArray($object)
	{
		if (!is_object($object) && !is_array($object)){
			return $object;
		}
		if (is_object($object)){
			$object = get_object_vars($object);
		}
	
		return array_map(array($this, '_objectToArray'), $object);
	}
	
	/**
	 * Load query and set response
	 *
	 * @param string $query
	 * @param int $limit
	 * @return search response
	 */
	abstract function loadQuery($query,$storeId=0,$limit=10);
	
	/**
	 * getBoostQuery
	 * Use to create a formatted list of attribs and boost values for the search engine
	 *
	 * @return string
	*/
	abstract function getBoostQuery();
	
	/**
	 * commit
	 * Executes an explicit commit
	 *
	 * @return search engine response
	*/
	abstract function commit();
	
	/**
	 * upload
	 * Uploads data to the search index
	 *
	 * @return search engine response
	*/
	abstract function upload($data);
	
	/**
	 * Delete All documents
	 *
	 * @return Innovent_CloudsearchConnect_Model_Search
	*/
	abstract function deleteAllDocuments();
	
	/**
	 * Retreive documents count
	 *
	 * @return int
	*/
	abstract protected function count();
		
}
