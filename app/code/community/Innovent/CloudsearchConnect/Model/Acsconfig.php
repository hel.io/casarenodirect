<?php
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */

/**
 * Acsconfig model
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Model_Acsconfig extends Mage_Core_Model_Abstract
{
	protected $_prefix = Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS;
	
	/**
	 * Entity code.
	 * Can be used as part of method name for entity processing
	 */
	const ENTITY= 'acs_config';
	const CACHE_TAG = 'acs_config';
	/**
	 * Prefix of model events names
	 * @var string
	 */
	protected $_eventPrefix = 'acs_config';
	
	/**
	 * Parameter name in event
	 * @var string
	 */
	//protected $_eventObject = 'excol';
	//protected $_productInstance = null;

	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Innovent Solutions, Inc.
	 */
	public function _construct()
	{
		parent::_construct();
		$this->_init('search/acsconfig');

		$cfg_data = Mage::getStoreConfig($this->_prefix);
		if (empty($cfg_data))
		{
			/* Default values are no longer supplied in this class. Defaults are loaded into
			 * the database upon first startup of this module using the SQL script supplied.
			 * 
			 * Add a warning if no config values are set.
			 */
			Mage::getSingleton('adminhtml/session')->addWarning('WARNING: No default values for this configuration exist in the database.');
			$values = new Varien_Object();
		}
		else
		{
			$values = new Varien_Object($cfg_data);
		}
		//echo print_r($values, true);
		//die;
		$this->setAcsEnabled($values->getAcsEnabled());
		$this->setDomain($values->getDomain());
		$this->setReturnFields($values->getReturnFields());
		$this->setExtraSearchableFields($values->getExtraSearchableFields());
		$this->setSearchLimit($values->getSearchLimit());
		$this->setApiVersion($values->getApiVersion());
		$this->setDocumentEndpoint($values->getDocumentEndpoint());
		$this->setSearchEndpoint($values->getSearchEndpoint());
		$this->setAccessKey($values->getAccessKey());
		$this->setSecretKey($values->getSecretKey());
		$this->setRegion($values->getRegion());
		$this->setIndexFilename($values->getIndexFilename());
		$this->setFileType($values->getFileType());
		$this->setSaytEnabled($values->getSaytEnabled());
		$this->setClientIP($values->getClientIP());
	}
	
	/**
	 * before save config
	 * @access protected
	 * @return Innovent_Search_Model_Config
	 * @author Innovent Solutions, Inc.
	 */
	protected function _beforeSave()
	{
		return parent::_beforeSave();
	}

	/**
	 * save config
	 * @access public
	 * @return Innovent_Search_Model_Config
	 * @author Innovent Solutions, Inc.
	 */
	protected function _afterSave() {
		return parent::_afterSave();
	}
	
}