<?php 
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/*
 * CloudsearchConnect setup
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup
{
	public function getEntityType($type)
	{
		Mage::log('in setup class');
		$e = Mage::getModel('eav/config')->getEntityType('catalog_product');
		Mage::log('in setup class type='. $typpe);
		Mage::log('in setup class e = ' . $e);
		return $e->getEntityTypeId();
	}
}