<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * Acsconfig resource model
 *
 * @category		Innovent
 * @package		Innovent_CloudsearchConnect
 * @author 		Bob Laviguer, Innovent Solutions, Inc.
**/
class Innovent_CloudsearchConnect_Model_Resource_Acsconfig extends Mage_Core_Model_Resource_Db_Abstract
{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Bob Laviguer, Innovent Solutions, Inc.
	 */
	public function _construct(){
		$this->_init('search/acsconfig', 'name');
	}

	/**
	 * Perform operations after object load
	 * @access public
	 * @param Mage_Core_Model_Abstract $object
	 * @return Innovent_Search_Model_Resource_Excol
	 * @author Bob Laviguer, Innovent Solutions, Inc.
	 */
	protected function _afterLoad(Mage_Core_Model_Abstract $object)
	{
		return parent::_afterLoad($object);
	}

	/**
	 * Retrieve select object for load object data
	 *
	 * @param string $field
	 * @param mixed $value
	 * @param Innovent_Search_Model_Excol $object
	 * @return Zend_Db_Select
	 */
	protected function _getLoadSelect($field, $value, $object){
		$select = parent::_getLoadSelect($field, $value, $object);
		return $select;
	}

	public function save(Mage_Core_Model_Abstract $object)
	{
		//Mage::log('In Acsconfig::save, object = ' . var_export($object->getData(),true));
		$prefix = Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS;
		$SEP = "/";
		
		foreach($object->getData() as $key=>$value)
		{
			$path = $prefix . $SEP . $key;
			Mage::getConfig()->saveConfig($path, $value);
			
		}
		
		// Set current search provider
		Mage::helper('search')->setActiveProvider();
		
		$this->_afterSave($object);
		return true;
	}
	
}