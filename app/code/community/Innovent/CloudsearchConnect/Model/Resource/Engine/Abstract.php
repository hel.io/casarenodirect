<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/**
 * Innovent_CloudsearchConnect_Model_Engine_Abstract
 * Abstract class for search engines
 *
 * Copyright 2013, Innovent Solutions, Inc.
 * Author: Innovent Solutions, Inc
 *
 * Last Mod:
 * 08/25/2013 - Created, BL
 *
 */

abstract class Innovent_CloudsearchConnect_Model_Resource_Engine_Abstract
{
	const KEY_BASE = ""; // This defines which config data section to use	
	const DEFAULT_ROWS_LIMIT = 9999;
	const UNIQUE_KEY = 'unique';
	
	protected $_response; // This is the response from all search engines
	protected $_engine; // This will contain a reference to the search engine (solr, acs, etc)
	protected $_index; // Index name
	
	/**
	 * @var object Search engine client.
	*/
	protected $_client;
	
	/**
	 * @var array List of dates format.
	 */
	protected $_dateFormats = array();
	
	/**
	 * @var array List of default query parameters.
	*/
	protected $_defaultQueryParams = array(
			'offset' => 0,
			'limit' => 100,
			'sort_by' => array(array('relevance' => 'desc')),
			'store_id' => null,
			'locale_code' => null,
			'fields' => array(),
			'params' => array(),
			'ignore_handler' => false,
			'filters' => array(),
	);
	
	/**
	 * @var array List of indexable attribute parameters.
	*/
	protected $_indexableAttributeParams = array();
	
	/**
	 * @var int Last number of results found.
	*/
	protected $_lastNumFound;
	
	/**
	 * @var array List of non fulltext fields.
	 */
	protected $_notInFulltextField = array(
			self::UNIQUE_KEY,
			'id',
			'store_id',
			'in_stock',
			'categories',
			'show_in_categories',
			'visibility'
	);
	
	/**
	 * @var bool Stores search engine availibility
	*/
	protected $_test = null;
	
	/**
	 * @var array List of used fields.
	 */
	protected $_usedFields = array(
			self::UNIQUE_KEY,
			'id',
			'sku',
			'price',
			'store_id',
			'categories',
			'show_in_categories',
			'visibility',
			'in_stock',
			'score'
	);

	/**
	 * Checks if advanced index is allowed for current search engine.
	 *
	 * @return bool
	 */
	public function allowAdvancedIndex()
	{
		return true;
	}
	
	/**
	 * Returns product visibility ids for search.
	 *
	 * @see Mage_Catalog_Model_Product_Visibility
	 * @return mixed
	 */
	public function getAllowedVisibility()
	{
		return Mage::getSingleton('catalog/product_visibility')->getVisibleInSearchIds();
	}

	/**
	 * Prepares index data.
	 * Should be overriden in child classes if needed.
	 *
	 * @param $index
	 * @param string $separator
	 * @return array
	 */
	public function prepareEntityIndex($index, $separator = null)
	{
		return $this->_getHelper()->prepareIndexData($index, $separator);
	}
	
	/**
	 * Adds advanced index fields to index data.
	 *
	 * @param array $index
	 * @param int $storeId
	 * @param array $productIds
	 * @return array
	 */
	public function addAdvancedIndex($index, $storeId, $productIds = null)
	{
		return Mage::getModel('engine/elastic_indexer')->addAdvancedIndex($index, $storeId, $productIds);
		//return Mage::getResourceSingleton('search/engine_elastic')->addAdvancedIndex($index, $storeId, $productIds);
	}
	
	public function getIndex()
	{
		return $this->_index;
	}
	
	/**
	 * _getFQString
	 *	Returns an array of attrib names and boost values to be formatted in the 
	 *	search class.
	 *
	 * @return array
	 */
	protected function _getFQArray1()
	{
		// Get list of searchable attributes
		$searchable = Mage::helper('search')->getSearchableAttributes();

		// Get list of export columns (attributes)
		$attribs = Mage::getModel('search/excol')->getSelectedItems();
		$numItems = count($attribs);
		$ar1 = null;
		
		foreach($attribs as $attrib)
		{
			$name = $attrib->getName();
			
			// For each attribute, lookup its boost weight and append to string
			$attrib2 = $searchable[$name];
			$ar1[] = array("name"=>$name, "boost"=>$attrib2->getSearchWeight());
					
		}
		//echo '<pre>'. print_r($temp,true). '</pre>';
		return $ar1;
	}
		
	/**
	 * Set search engine response
	 *
	 * @param $response
	 */
	protected function setResponse($response) 
	{
		$this->_response = $response;
	}

	/**
	 * Extract product ids and score in Acs response
	 *
	 * @return array $ids
	 */
	protected function getProducts() {
		$products = array();
		foreach($this->_response->docs as $doc) {
			$products[] = array('id'=>$doc->id,'relevance'=>$doc->score);
		}
		//Mage::log('Number of products: ' . count($products));
		return $products;
	}
	
	/**
	 * Test if server configuration is complete
	 *
	 * @return bool
	 */
	protected function isConfigured() 
	{
		return Mage::getStoreConfig(self::KEY_BASE . '/host_name') && Mage::getStoreConfig(self::KEY_BASE . '/port');
	}
	
	/**
	 * Retrieve Store Id
	 *
	 * @return int
	 */
	protected function getStoreId() 
	{
		return Mage::app()->getStore()->getId();
	}
	
	/**
	 * Transforms specified object to an array.
	 *
	 * @param $object
	 * @return array
	 */
	protected function _objectToArray($object)
	{
		if (!is_object($object) && !is_array($object)){
			return $object;
		}
		if (is_object($object)){
			$object = get_object_vars($object);
		}
	
		return array_map(array($this, '_objectToArray'), $object);
	}

	/**
	 * Checks if layered navigation is available for current search engine.
	 *
	 * @return bool
	 */
	public function isLayeredNavigationAllowed()
	{
		return true;
	}
	
	/**
	 * Alias of isLayeredNavigationAllowed.
	 *
	 * @return bool
	 */
	public function isLeyeredNavigationAllowed()
	{
		return $this->isLayeredNavigationAllowed();
	}
		
	/**
	 * Load query and set response
	 *
	 * @param string $query
	 * @param int $limit
	 * @return search response
	 */
	abstract function loadQuery($query,$storeId=0,$limit=10);
	
	/**
	 * getBoostQuery
	 * Use to create a formatted list of attribs and boost values for the search engine
	 *
	 * @return string
	*/
	abstract function getBoostQuery();
	
	/**
	 * commit
	 * Executes an explicit commit
	 *
	 * @return search engine response
	*/
	abstract function commit();
	
	/**
	 * upload
	 * Uploads data to the search index
	 *
	 * @return search engine response
	*/
	abstract function upload($data);
	
	/**
	 * Delete All documents
	 *
	 * @return Innovent_CloudsearchConnect_Model_Search
	*/
	abstract function deleteAllDocuments();
	
	/**
	 * Retreive documents count
	 *
	 * @return int
	*/
	abstract protected function count();

	/**
	 * Saves products data in index.
	 *
	 * @abstract
	 * @param int $storeId
	 * @param array $indexes
	 * @param string $type
	 * @return mixed
	 */
	abstract public function saveEntityIndexes($storeId, $indexes, $type = 'product');
}
