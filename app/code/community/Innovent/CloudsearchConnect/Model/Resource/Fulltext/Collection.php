<?php
/*
 * Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/*
 * Innovent_CloudsearchConnect_Model_Resource_Fulltext_Collection
 * Override file for fulltext search - this file adds functionality for 
 * each of the 3 supported 3rd party search engines.
 *  
 */

/**
 * Fulltext Collection
 *
 * @category   	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author      Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Model_Resource_Fulltext_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    /**
     * Retrieve query model object
     *
     * @return Mage_Catalogsearch_Model_Query
     */
    protected function _getQuery()
    {
        return Mage::helper('search')->getQuery();
    }

    /**
     * Add search query filter
     *
     * @param string $query
     * @return Mage_Catalogsearch_Model_Resource_Fulltext_Collection
     */
    public function addSearchFilter($query)
    {
    	//Mage::log('addSearchFilter');
		Mage::getSingleton('search/fulltext')->prepareResult();
    	if (Mage::helper('search')->getActiveProvider() == Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_DEFAULT)
    	{
				$this->getSelect()->joinInner(
					array('search_result' => $this->getTable('catalogsearch/result')),
					$this->getConnection()->quoteInto(
						'search_result.product_id=e.entity_id AND search_result.query_id=?',
						$this->_getQuery()->getId()),
            		array('relevance' => 'relevance'));
		    	break;
    	}
    	else 
    	{
    		
    	}
    	 
        return $this;
    }

    /**
     * Set Order field
     *
     * @param string $attribute
     * @param string $dir
     * @return Mage_Catalogsearch_Model_Resource_Fulltext_Collection
     */
    public function setOrder($attribute, $dir = 'desc')
    {
        if ($attribute == 'relevance') {
            $this->getSelect()->order("relevance {$dir}");
        } else {
            parent::setOrder($attribute, $dir);
        }
        return $this;
    }

    /**
     * Stub method for campatibility with other search engines
     *
     * @return Mage_Catalogsearch_Model_Resource_Fulltext_Collection
     */
    public function setGeneralDefaultQuery()
    {
        return $this;
    }
}
