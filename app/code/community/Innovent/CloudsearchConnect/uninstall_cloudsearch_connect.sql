/*
* Innovent_CloudsearchConnect extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Extension License Agreement
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.innoventsolutions.com/magento-extension-license.html
* If you did not rece0ive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to magento@innoventsolutions.com so we will send you a copy immediately.
*
*
* @category	Innovent
* @package		Innovent_CloudsearchConnect
* @copyright   Copyright (c) 2015 Innovent Solutions Inc. (http://www.innoventsolutions.com)
* @author      Innovent Solutions, Inc.
* @license     http://www.innoventsolutions.com/magento-extension-license.html
*/

/* IMPORTANT NOTE!!
 * Make sure you execute a 'USE <db>' statement, changing <db> to your Magento
 * database name before you execute any of the following SQL statements.
 */

#USE <db>;

/*
** Delete the module entry
*/
DELETE FROM core_resource WHERE code = 'Innovent_CloudsearchConnect_setup';

/*
** Reset the catalog search index process to update on save
*/
UPDATE index_process SET mode = 'real_time' WHERE indexer_code = 'catalogsearch_fulltext';

/*
** Remove all extension-specific configuration options
*/
DELETE FROM core_config_data WHERE path LIKE 'innovent_search%';

/*
 * Remove the search_weight column from the attribute table
 */
ALTER TABLE catalog_eav_attribute DROP COLUMN search_weight;

