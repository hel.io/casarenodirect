<?php
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */

//$os = php_uname('s');
//if (strpos(strtolower($os), 'windows') > 0)
//{
	//Mage::log(php_uname('s'));
    @require_once('lib/aws.phar');
	use Aws\Common\Aws;
	error_reporting(E_ERROR | E_PARSE);
//}
//use Aws\Common\Enum\Region;
//use Aws\CloudSearch\CloudSearchClient;
//use Aws\CloudSearch\Exception;

/**
 * Acs default helper
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @author 		Innovent Solutions, Inc.
 */
class Innovent_CloudsearchConnect_Helper_Data extends Mage_Core_Helper_Abstract
{
	const ENGINE_KEY = 'catalog/search/engine';
	const KEY_BASE = Innovent_CloudsearchConnect_Helper_Data::SEARCH_PROVIDER_KEY_ACS;

	const EXPORT_OP_ADD = 'add';
	const EXPORT_OP_DEL = 'delete';

	const SEARCH_PROVIDER_DEFAULT = 0;
	const SEARCH_PROVIDER_ACS = 2;

	const SEARCH_PROVIDER_KEY_CURRENT = 'innovent_search/current_provider';
	const SEARCH_PROVIDER_KEY_ACS = 'innovent_search/acs';

	protected $_engine = NULL;
	protected $_prodAttribs = NULL;
	protected $_cronFile = NULL;

	/**
	 * Allowed languages.
	 * Example: array('en_US' => 'en', 'fr_FR' => 'fr')
	 *
	 * @var array
	 */
	protected $_languageCodes = array();

	/**
	 * Searchable attributes.
	 *
	 * @var array
	*/
	protected $_searchableAttributes;

	/**
	 * Sortable attributes.
	 *
	 * @var array
	 */
	protected $_sortableAttributes;

	/**
	 * Text field types.
	 *
	 * @var array
	 */
	protected $_textFieldTypes = array(
			'text',
			'varchar',
	);

	/**
	 * Unlocalized field types.
	 *
	 * @var array
	*/
	protected $_unlocalizedFieldTypes = array(
			'datetime',
			'decimal',
	);

	/*
	 *  Returns a conversion from delimiter name to delimiter character
	 *
	 *   @returns string
	 */
	public function getDelimiter($delimName)
	{
		switch ($delimName)
		{
			case 'comma': $delim = ","; break;
			case 'semicolon': $delim = ";"; break;
			case 'colon': $delim = ":"; break;
			case 'bar': $delim = "|"; break;
			case 'tilde': $delim = "~"; break;
			case 'tab': $delim = "\t"; break;
		}
		return $delim;
	}

	public function __construct()
	{
		$this->_cronFile = Mage::getModuleDir('etc','Innovent_CloudsearchConnect') . '/innovent.cron';
	}

	/**
	 * Get current search engine resource model
	 *
	 * @return object
	 */
	public function getEngine()
	{
		if (!$this->_engine)
		{

			///$docEndpoint = $creds['DocumentEndpoint'];
			///$searchEndpoint = $creds['SearchEndpoint'];
			///$apiVersion = $creds['APIVersion'];
			$aws = Aws::factory(array(
					'key'    => Mage::getStoreConfig(self::KEY_BASE . '/access_key'),
					'secret' => Mage::getStoreConfig(self::KEY_BASE . '/secret_key'),
					'region' => Mage::getStoreConfig(self::KEY_BASE . '/region')
			));
			$this->_engine = $aws->get('CloudSearch');
		}
		return $this->_engine;
	}

	/*
	 *  Test if the search engine is available
	*/
	public function test()
	{
		// NOTE: No ping test available for Cloudsearch - return true
		//$this->getEngine();
		//$response = $this->_engine->ping();
		return true;
	}

	public function clearCache()
	{
		try {
			$allTypes = Mage::app()->useCache();
			foreach($allTypes as $type => $blah) {
				Mage::app()->getCacheInstance()->cleanType($type);
			}
		} catch (Exception $e) {
			// do something
			error_log($e->getMessage());
		}
	}

	public function setActiveProvider()
	{
		Mage::getConfig()->reinit();

		$acsEnabled = Mage::getStoreConfig(self::SEARCH_PROVIDER_KEY_ACS . '/acs_enabled');

		if ($acsEnabled) {
			//Mage::getConfig()->saveConfig(self::ENGINE_KEY, 'catalogsearch/fulltext_engine');
			$provider = self::SEARCH_PROVIDER_ACS;
		}
		else {
			//Mage::getConfig()->saveConfig('catalog/search/engine', 'catalogsearch/fulltext_engine');
			$provider = self::SEARCH_PROVIDER_DEFAULT;
		}

		Mage::getConfig()->saveConfig(self::SEARCH_PROVIDER_KEY_CURRENT, $provider);
	}

	public function getActiveProvider()
	{
		return Mage::getStoreConfig(self::SEARCH_PROVIDER_KEY_CURRENT);
	}

	public function getActiveProviderKey($provider)
	{
		$retVal = "";
		switch ($provider){
			case self::SEARCH_PROVIDER_ACS:
				$retVal = self::SEARCH_PROVIDER_KEY_ACS;
				break;
			default:
				break;
		}
		return $retVal;
	}

	/*  _getAttributes
	 *
	 *  Build attribute array with the following criteris:
	 *  	1. Product attribute collection
	 *  	2. Inner joined to catalog_eav_attribute (cea)
	 *  	3. Where cea.is_visible = 1
	 *  	4. Where cea.is_searchable = 1
	 *
	 *  @return $productAttrs
	 */
	public function getAttributes()
	{
		if (NULL == $this->_prodAttribs)
		{
			//*** A.B. 17 Feb 2015 - Tend to the cases where people have table prefixes.

			$tableprefix = Mage::getConfig()->getTablePrefix();
			$entityTypeId = Mage::getModel('eav/config')->getEntityType('catalog_product')->getEntityTypeId();
			$productAttrs = Mage::getModel('eav/entity_attribute')->getCollection()->addFieldToFilter('entity_type_id', $entityTypeId);
			//**  $productAttrs->getSelect()->joinInner(array('cea' => 'catalog_eav_attribute'), 'cea.attribute_id=main_table.attribute_id');

			$productAttrs->getSelect()->joinInner(array('cea' => $tableprefix.'catalog_eav_attribute'), 'cea.attribute_id=main_table.attribute_id');
			$productAttrs->addFieldToFilter('is_visible',1);
			$productAttrs->addFieldToFilter('is_searchable',1);
			$this->_prodAttribs = $productAttrs;
		}
		return $this->_prodAttribs;
	}

	/**
	 * getSearchableAttributes
	 * Gets product attribute names
	 *
	 * @access public
	 * @return array of attrib names
	 * @author Innovent Solutions, Inc.
	 *
	 */
	public function getSearchableAttributes($backendType = null)
	{
		$this->getAttributes();
		if ($this->_searchableAttributes === null)
		{
			// Get all searchable attributes (as defined by Magento)
			$this->_searchableAttributes = array();
			$entityType = $this->getEavConfig()->getEntityType('catalog_product');
			$entity = $entityType->getEntity();

			foreach ($this->_prodAttribs as $attribute) {
				/** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
				$attribute->setEntity($entity);
				$this->_searchableAttributes[$attribute->getAttributeCode()] = $attribute;
			}
		}

		if (null !== $backendType) {
			$backendType = (array) $backendType;
			$attributes = array();
			foreach ($this->_searchableAttributes as $attribute) {
				/** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
				if (in_array($attribute->getBackendType(), $backendType)) {
					$attributes[$attribute->getAttributeCode()] = $attribute;
				}
			}
			return $attributes;
		}

		return $this->_searchableAttributes;
	}

	/**
	 * Returns EAV config singleton.
	 *
	 * @return Mage_Eav_Model_Config
	 */
	public function getEavConfig()
	{
		return Mage::getSingleton('eav/config');
	}

	/**
	 * Returns language code of specified locale code.
	 *
	 * @param string $localeCode
	 * @return bool
	 */
	public function getLanguageCodeByLocaleCode($localeCode)
	{
		$localeCode = (string) $localeCode;
		if (!$localeCode) {
			return false;
		}

		if (!isset($this->_languageCodes[$localeCode])) {
			$languages = $this->getSupportedLanguages();
			$this->_languageCodes[$localeCode] = false;
			foreach ($languages as $code => $locales) {
				if (is_array($locales)) {
					if (in_array($localeCode, $locales)) {
						$this->_languageCodes[$localeCode] = $code;
					}
				} elseif ($localeCode == $locales) {
					$this->_languageCodes[$localeCode] = $code;
				}
			}
		}

		return $this->_languageCodes[$localeCode];
	}

	/**
	 * Returns store language code.
	 *
	 * @param mixed $store
	 * @return bool
	 */
	public function getLanguageCodeByStore($store = null)
	{
		return $this->getLanguageCodeByLocaleCode($this->getLocaleCode($store));
	}

	/**
	 * Returns store locale code.
	 *
	 * @param null $store
	 * @return string
	 */
	public function getLocaleCode($store = null)
	{
		return Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE, $store);
	}


	/**
	 * getCategories
	 * Gets category names for the product
	 *
	 * @access public
	 * @return array of category names
	 * @author Innovent Solutions, Inc.
	 *
	 */
	public function getCategories($product)
	{
		$names = array();
		$cats = $product->getCategoryIds();
		foreach($cats as $cat)
		{
			$names[] = Mage::getModel('catalog/category')->load($cat)->getName();
		}
		return $names;
	}

	public function readFile($path, $fileName)
	{
		$io = new Varien_Io_File();
		try
		{
			$io->open(array('path' => $path));
			$io->streamOpen($fileName, 'r');
			$io->streamLock(false);
			$in = true;
			$data = null;
			while($in != false)
			{
				$in = $io->streamRead();
				$data .= $in;
			}
			$io->streamClose();

			return $data;
		}
		catch (Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__('Error opening or reading file, [' . $path . '\\' . $fileName . ']. See the error log for more information.'));
		}
		return $data;
	}

	public function writeFile($path, $fileName, $data)
	{
		try
		{
			$io = new Varien_Io_File();
			$io->setAllowCreateFolders(true);
			$io->open(array('path' => $path));
			$io->streamOpen($fileName, 'w');
			$io->streamLock(true);
			if ($io->isWriteable($fileName))
			{
				$io->streamWrite($data);
			}
			else
			{
				Mage::log('Filename is not writeable: ' . $path . $fileName);
				return 0;
			}
		}
		catch (Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('search')->__('Error opening or writing file, [' . $path . '\\' . $fileName . ']. See the error log for more information.'));
			return 0;
		}
		return 1;
	}

	/**
	 * Defines supported languages for snowball filter.
	 *
	 * @return array
	 */
	public function getSupportedLanguages()
	{
		$default = array(
				/**
				 * SnowBall filter based
		*/
				// Danish
				'da' => 'da_DK',
				// Dutch
				'nl' => 'nl_NL',
				// English
				'en' => array('en_AU', 'en_CA', 'en_NZ', 'en_GB', 'en_US'),
				// Finnish
				'fi' => 'fi_FI',
				// French
				'fr' => array('fr_CA', 'fr_FR'),
				// German
				'de' => array('de_DE','de_DE','de_AT'),
				// Italian
				'it' => array('it_IT','it_CH'),
				// Norwegian
				'nb' => array('nb_NO', 'nn_NO'),
				// Portuguese
				'pt' => array('pt_BR', 'pt_PT'),
				// Romanian
				'ro' => 'ro_RO',
				// Russian
				'ru' => 'ru_RU',
				// Spanish
				'es' => array('es_AR', 'es_CL', 'es_CO', 'es_CR', 'es_ES', 'es_MX', 'es_PA', 'es_PE', 'es_VE'),
				// Swedish
				'sv' => 'sv_SE',
				// Turkish
				'tr' => 'tr_TR',

				/**
				 * Lucene class based
		*/
				// Czech
				'cs' => 'cs_CZ',
				// Greek
				'el' => 'el_GR',
				// Thai
				'th' => 'th_TH',
				// Chinese
				'zh' => array('zh_CN', 'zh_HK', 'zh_TW'),
				// Japanese
				'ja' => 'ja_JP',
				// Korean
				'ko' => 'ko_KR'
		);

		return $default;
	}

	/**
	 * Checks if specified attribute is indexable by search engine.
	 *
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
	 * @return bool
	 */
	public function isAttributeIndexable($attribute)
	{
		if ($attribute->getBackendType() == 'varchar' && !$attribute->getBackendModel()) {
			return true;
		}

		if ($attribute->getBackendType() == 'int'
				&& $attribute->getSourceModel() != 'eav/entity_attribute_source_boolean'
						&& ($attribute->getIsSearchable() || $attribute->getIsFilterable() || $attribute->getIsFilterableInSearch())
		) {
			return true;
		}

		if ($attribute->getIsSearchable() || $attribute->getIsFilterable() || $attribute->getIsFilterableInSearch()) {
			return true;
		}

		return false;
	}

	public function getConfig()
	{
		return $this->_config;
	}

	/**
	 * Method that can be overriden for customing product data indexation.
	 *
	 * @param array $index
	 * @param string $separator
	 * @return array
	 */
	public function prepareIndexData($index, $separator = null)
	{
		return $index;
	}
	/**
	 * Checks if debug mode is enabled.
	 *
	 * @return bool
	 */
	public function isDebugEnabled()
	{
		$config = Mage::getStoreConfig($this->getActiveProviderKey($this->getActiveProvider()));
		return array_key_exists('enable_debug_mode', $config ) && $config['enable_debug_mode'];
	}

	/**
	 * Forces error display.
	 *
	 * @param string $error
	 */
	public function showError($error)
	{
		echo Mage::app()->getLayout()->createBlock('core/messages')->addError($error)->getGroupedHtml();
	}

	/**
	 * Returns attribute field name (localized if needed).
	 *
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
	 * @param string $localeCode
	 * @return string
	 */
	public function getAttributeFieldName($attribute, $localeCode = null)
	{
		if (is_string($attribute)) {
			$this->getSearchableAttributes(); // populate searchable attributes if not already set
			if (!isset($this->_searchableAttributes[$attribute])) {
				return $attribute;
			}
			$attribute = $this->_searchableAttributes[$attribute];
		}
		$attributeCode = $attribute->getAttributeCode();
		$backendType = $attribute->getBackendType();

		if ($attributeCode != 'score' && in_array($backendType, $this->_textFieldTypes)) {
			if (null === $localeCode) {
				$localeCode = $this->getLocaleCode();
			}
			$languageCode = $this->getLanguageCodeByLocaleCode($localeCode);
			$languageSuffix = $languageCode ? '_' . $languageCode : '';
			$attributeCode .= $languageSuffix;
		}

		return $attributeCode;
	}

	/**
	 * Returns cache lifetime in seconds.
	 *
	 * @return int
	 */
	public function getCacheLifetime()
	{
		return Mage::getStoreConfig('core/cache/lifetime');
	}

	/**
	 * Returns sortable attribute field name (localized if needed).
	 *
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
	 * @param string $locale
	 * @return string
	 */
	public function getSortableAttributeFieldName($attribute, $locale = null)
	{
		if (is_string($attribute)) {
			$this->getSortableAttributes(); // populate sortable attributes if not already set
			if (!isset($this->_sortableAttributes[$attribute])) {
				return $attribute;
			}
			$attribute = $this->_sortableAttributes[$attribute];
		}

		$attributeCode = $attribute->getAttributeCode();

		if ($attributeCode != 'score' && !in_array($attribute->getBackendType(), $this->_unlocalizedFieldTypes)) {
			if (null === $locale) {
				$locale = $this->getLocaleCode();
			}
			$languageCode = $this->getLanguageCodeByLocaleCode($locale);
			$languageSuffix = $languageCode ? '_' . $languageCode : '';
			$attributeCode .= $languageSuffix;
		}

		return 'sort_by_' . $attributeCode;
	}

	/**
	 * Retrieves all sortable product attributes.
	 *
	 * @return array
	 */
	public function getSortableAttributes()
	{
		if (null === $this->_sortableAttributes) {
			$this->_sortableAttributes = Mage::getSingleton('catalog/config')->getAttributesUsedForSortBy();
			if (array_key_exists('price', $this->_sortableAttributes)) {
				unset($this->_sortableAttributes['price']); // Price sorting is handled with searchable attribute.
			}
		}

		return $this->_sortableAttributes;
	}

	/*
	 * getHTTPInfo()
	 * Returns an info string for the corresponding status code
	 *
	 * @param int $status
	 * @return string
	 */
	public function getHTTPInfo($status)
	{
		$http_status_codes = array(100 => "Continue", 101 => "Switching Protocols", 102 => "Processing", 200 => "OK",
				201 => "Created", 202 => "Accepted", 203 => "Non-Authoritative Information", 204 => "No Content",
				205 => "Reset Content", 206 => "Partial Content", 207 => "Multi-Status", 300 => "Multiple Choices",
				301 => "Moved Permanently", 302 => "Found", 303 => "See Other", 304 => "Not Modified", 305 => "Use Proxy",
				306 => "(Unused)", 307 => "Temporary Redirect", 308 => "Permanent Redirect", 400 => "Bad Request",
				401 => "Unauthorized", 402 => "Payment Required", 403 => "Forbidden", 404 => "Not Found", 405 => "Method Not Allowed",
				406 => "Not Acceptable", 407 => "Proxy Authentication Required", 408 => "Request Timeout", 409 => "Conflict",
				410 => "Gone", 411 => "Length Required", 412 => "Precondition Failed", 413 => "Request Entity Too Large",
				414 => "Request-URI Too Long", 415 => "Unsupported Media Type", 416 => "Requested Range Not Satisfiable",
				417 => "Expectation Failed", 418 => "I'm a teapot", 419 => "Authentication Timeout", 420 => "Enhance Your Calm",
				422 => "Unprocessable Entity", 423 => "Locked", 424 => "Failed Dependency", 424 => "Method Failure",
				425 => "Unordered Collection", 426 => "Upgrade Required", 428 => "Precondition Required", 429 => "Too Many Requests",
				431 => "Request Header Fields Too Large", 444 => "No Response", 449 => "Retry With", 450 => "Blocked by Windows Parental Controls",
				451 => "Unavailable For Legal Reasons", 494 => "Request Header Too Large", 495 => "Cert Error", 496 => "No Cert",
				497 => "HTTP to HTTPS", 499 => "Client Closed Request", 500 => "Internal Server Error", 501 => "Not Implemented",
				502 => "Bad Gateway", 503 => "Service Unavailable", 504 => "Gateway Timeout", 505 => "HTTP Version Not Supported",
				506 => "Variant Also Negotiates", 507 => "Insufficient Storage", 508 => "Loop Detected", 509 => "Bandwidth Limit Exceeded",
				510 => "Not Extended", 511 => "Network Authentication Required", 598 => "Network read timeout error", 599 => "Network connect timeout error");

		if (array_key_exists($status, $http_status_codes))
		{
			return $http_status_codes[$status];
		}
		else
		{
			return 'HTTP status code not found.';
		}

	}

	public function getCronEnabled()
	{
		return file_exists($this->_cronFile);
	}

	public function setCronEnabled()
	{
		$filename = $this->_cronFile;
		$somecontent = "run";

		if ($handle = fopen($filename, 'c'))
		{
			if (fwrite($handle, $somecontent) === FALSE)
			{
				Mage::log('Cannot write to cron enabled file ' . $this->_cronFile);
				return;
			}
			fclose($handle);
		}
		else
		{
			Mage::log('Cannot create cron enabled file ' . $this->_cronFile);
		}
	}

	public function stopCron()
	{
		if (file_exists($this->_cronFile)) { unlink($this->_cronFile); }
	}

}
