<?php
/**
 * Cryozonic
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Single Domain License
 * that is available through the world-wide-web at this URL:
 * http://cryozonic.com/licenses/stripe.html
 * If you are unable to obtain it through the world-wide-web,
 * please send an email to info@cryozonic.com so we can send
 * you a copy immediately.
 *
 * @category   Cryozonic
 * @package    Cryozonic_Stripe
 * @copyright  Copyright (c) 2013 Cryozonic Ltd (http://cryozonic.com)
 */
require_once 'Cryozonic/Stripe/lib/Stripe.php';

class Cryozonic_Stripe_Model_Standard extends Mage_Payment_Model_Method_Abstract {
    protected $_code = 'cryozonic_stripe';

    protected $_isInitializeNeeded      = false;
    protected $_canUseForMultishipping  = true;
    protected $_isGateway               = true;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = true;
    protected $_canRefund               = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid                 = true;
    protected $_canCancelInvoice        = true;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canSaveCc               = false;
    protected $_formBlockType           = 'cryozonic_stripe/form_standard';

    // Docs: http://docs.magentocommerce.com/Mage_Payment/Mage_Payment_Model_Method_Abstract.html
    // mixed $_canCreateBillingAgreement
    // mixed $_canFetchTransactionInfo
    // mixed $_canManageRecurringProfiles
    // mixed $_canOrder
    // mixed $_canReviewPayment
    // array $_debugReplacePrivateDataKeys
    // mixed $_infoBlockType

    /**
     * Stripe Modes
     */
    const TEST = 'test';
    const LIVE = 'live';

    public function __construct()
    {
        $store = $this->getStore();
        $mode = $store->getConfig('payment/cryozonic_stripe/stripe_mode');
        $this->saveCards = $store->getConfig('payment/cryozonic_stripe/ccsave');
        $path = "payment/cryozonic_stripe/stripe_{$mode}_sk";
        $apiKey = $store->getConfig($path);
        Stripe::setApiKey($apiKey);

        if ($this->saveCards)
        {
            $this->ensureStripeCustomer();
        }
    }

    protected function getStore()
    {
        // Admins may be viewing an order placed on a specific store
        if (Mage::app()->getStore()->isAdmin())
        {
            try
            {
                $orderId = Mage::app()->getRequest()->getParam('order_id');
                $order = Mage::getModel('sales/order')->load($orderId);
                $store = $order->getStore();
                if (!empty($store) && $store->getId())
                    return $store;
            }
            catch (Exception $e) {}
        }

        // Users get the store they are on
        return Mage::app()->getStore();
    }

    protected function ensureStripeCustomer()
    {
        $customerStripeId = $this->getCustomerStripeId();
        $retrievedSecondsAgo = (time() - $this->customerLastRetrieved);

        if (!$customerStripeId)
        {
            $customer = $this->createStripeCustomer();
        }
        // if the customer was retrieved from Stripe in the last 10 minutes, we're good to go
        // otherwise retrieve them now to make sure they were not deleted from Stripe somehow
        else if ((time() - $this->customerLastRetrieved) > (60 * 10))
        {
            if (!$this->getStripeCustomer($customerStripeId))
            {
                $this->deleteStripeCustomerId($customerStripeId);
                $this->createStripeCustomer();
            }
        }
    }

    protected function getCustomerEmail()
    {
        return $this->getSessionQuote()->getCustomerEmail();
    }

    protected function getCustomerId()
    {
        // If we are in the back office
        if (Mage::app()->getStore()->isAdmin())
        {
            return Mage::getSingleton('adminhtml/sales_order_create')->getQuote()->getCustomerId();
        }
        // If we are on the checkout page
        else if (Mage::getSingleton('customer/session')->isLoggedIn())
        {
            return Mage::getSingleton('customer/session')->getCustomer()->getId();
        }

        return null;
    }

    protected function getSessionQuote()
    {
        // If we are in the back office
        if (Mage::app()->getStore()->isAdmin())
        {
            return Mage::getSingleton('adminhtml/sales_order_create')->getQuote();
        }
        // If we are a user
        return Mage::getSingleton('checkout/session')->getQuote();
    }

    protected function getAvsFields($card)
    {
        if (Mage::getStoreConfig('payment/cryozonic_stripe/avs'))
        {
            $checkout = Mage::getSingleton('checkout/session')->getQuote();
            $billAddress = $checkout->getBillingAddress();
            $card['address_line1'] = $billAddress->getData('street');
            $card['address_zip'] = $billAddress->getData('postcode');

            // If there is no checkout session then we must be coming here from the back office.
            if (empty($card['address_line1'])) {
                $quote = $this->getInfoInstance()->getQuote();

                if (!empty($quote)) {
                    $billAddress = $quote->getBillingAddress();
                    $card['address_line1'] = $billAddress->getData('street');
                    $card['address_zip'] = $billAddress->getData('postcode');
                }
            }
        }
        return $card;
    }

    protected function createToken($params)
    {
        try
        {
            $params['card'] = $this->getAvsFields($params['card']);
            $token = Stripe_Token::create($params);

            if (empty($token['id']) || strpos($token['id'],'tok_') !== 0)
                Mage::throwException($this->t('Sorry, this payment method can not be used at the moment. Try again later.'));

            return $token['id'];
        }
        catch (Stripe_CardError $e)
        {
            $this->log($e->getMessage());
            Mage::throwException($this->t($e->getMessage()));
        }
    }

    protected function getInfoInstanceCard()
    {
        $info = $this->getInfoInstance();
        return array(
            "card" => array(
                "name" => $info->getCcOwner(),
                "number" => $info->getCcNumber(),
                "cvc" => $info->getCcCid(),
                "exp_month" => $info->getCcExpMonth(),
                "exp_year" => $info->getCcExpYear()
            )
        );
    }

    protected function getToken()
    {
        $info = $this->getInfoInstance();
        $token = $info->getAdditionalInformation('token');

        // Is this a saved card?
        if (strpos($token,'card_') === 0)
            return $token;

        // Are we coming from the back office?
        if (strstr($token,'tok_') === false)
        {
            $params = $this->getInfoInstanceCard();
            $token = $this->createToken($params);
        }

        return $token;
    }

    public function assignData($data)
    {
        $info = $this->getInfoInstance();

        if (!empty($data['cc_saved']) && $data['cc_saved'] != 'new_card')
        {
            $info->setAdditionalInformation('token', $data['cc_saved']);
            return $this;
        }

        if (empty($data['cc_number']))
            return $this;

        $params = array(
            "card" => array(
                "name" => $data['cc_owner'],
                "number" => $data['cc_number'],
                "cvc" => $data['cc_cid'],
                "exp_month" => $data['cc_exp_month'],
                "exp_year" => $data['cc_exp_year']
            )
        );

        // Add the card to the customer
        if ($this->saveCards && $data['cc_save'])
        {
            $cu = $this->getStripeCustomer($this->getCustomerStripeId());
            if ($cu)
            {
                // We could error out here, but we don't want to upset the customer.
                try
                {
                    $card = $this->addCardToCustomer($cu, $params['card']);
                    $token = $card->id;
                }
                catch (Exception $e)
                {
                    // We may get here if a CVC check failed, but we do not
                    // error out because Stripe will not give the exact reason
                    // that the card was declined. The card will error again
                    // at the final step with the correct reason.
                    $token = $this->createToken($params);
                }
            }
        }
        else
            $token = $this->createToken($params);

        $info->setAdditionalInformation('token', $token);

        return $this;
    }

    public function addCardToCustomer($customer, $newcard)
    {
        // Check if the customer already has this card, set it as the default
        $last4 = substr($newcard['number'], -4);
        $month = $newcard['exp_month'];
        $year = $newcard['exp_year'];
        foreach ($customer->cards->data as $card)
        {
            if ($last4 == $card->last4 &&
                $month == $card->exp_month &&
                $year == $card->exp_year)
            {
                $customer->default_card = $card->id;
                $customer->save();
                return $card;
            }

        }

        // If the customer doesn't have the card, create it and set it as the default card
        $newcard = $this->getAvsFields($newcard);
        $createdCard = $customer->cards->create(array('card'=>$newcard));
        $customer->default_card = $createdCard->id;
        $customer->save();
        return $createdCard;
    }

    public function authorize(Varien_Object $payment, $amount)
    {
        parent::authorize($payment, $amount);

        if ($amount > 0)
        {
            $this->createCharge($payment, $amount, false);
        }

        return $this;
    }

    public function capture(Varien_Object $payment, $amount)
    {
        parent::capture($payment, $amount);

        if ($amount > 0)
        {
            $captured = $payment->getAdditionalInformation('captured');
            $action = Mage::getStoreConfig('payment/cryozonic_stripe/payment_action');
            $depreciatedVersion = (($captured === null) && ($action == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE));

            if ($captured === false || $depreciatedVersion) {
                $token = $payment->getTransactionId();
                $token = preg_replace('/-.*$/', '', $token);
                try
                {
                    $ch = Stripe_Charge::retrieve($token);
                    $ch->capture(array('amount' => round($amount * 100)));
                }
                catch (Exception $e)
                {
                    $this->log($e->getMessage());
                    if (Mage::app()->getStore()->isAdmin() && $this->isAuthorizationExpired($e->getMessage()) && $this->retryWithSavedCard())
                        $this->createCharge($payment, $amount, true, true);
                    else
                        Mage::throwException($e->getMessage());
                }
            } else {
                $this->createCharge($payment, $amount, true);
            }
        }

        return $this;
    }

    protected function isAuthorizationExpired($errorMessage)
    {
        return (strstr($errorMessage, "cannot be captured because the charge has expired") !== false);
    }

    protected function retryWithSavedCard()
    {
        return Mage::getStoreConfig('payment/cryozonic_stripe/expired_authorizations');
    }

    public function createCharge(Varien_Object $payment, $amount, $capture, $forceUseSavedCard = false)
    {
        if ($forceUseSavedCard)
        {
            $token = $this->getSavedCardFrom($payment);
            $this->customerStripeId = $this->getCustomerStripeId($payment->getOrder()->getCustomerId());

            if (!$token || !$this->customerStripeId)
                Mage::throwException('The authorization has expired and the customer has no saved cards to re-create the order.');
        }
        else
            $token = $this->getToken();

        try {
            $order = $payment->getOrder();

            if (Mage::getStoreConfig('payment/cryozonic_stripe/use_store_currency'))
            {
                $amount = $order->getGrandTotal();
                $currency = $order->getOrderCurrencyCode();
            } else {
                $currency = $order->getBaseCurrencyCode();
            }

            $params = array(
              "amount" => round($amount * 100),
              "currency" => $currency,
              "card" => $token,
              "description" => "Order #".$order->getRealOrderId().' by '.$order->getCustomerName(),
              "capture" => $capture
            );

            // If this is a saved card, pass the customer id too
            if (strpos($token,'card_') === 0)
                $params["customer"] = $this->customerStripeId;

            try
            {
                $charge = Stripe_Charge::create($params);
            }
            catch (Exception $e)
            {
                // Necessary nested try-catch for the back-end
                Mage::throwException($this->t($e->getMessage()));
            }

            $payment->setTransactionId($charge->id);
            $payment->setAdditionalInformation('captured', $capture);
            $payment->setIsTransactionClosed(0);

            // Set the order status according to the configuration
            $newOrderStatus = Mage::getStoreConfig('payment/cryozonic_stripe/order_status');
            if (!empty($newOrderStatus))
            {
                $order->addStatusToHistory($newOrderStatus, $this->t('Changing order status as per New Order Status configuration'));
            }
        }
        catch(Stripe_CardError $e)
        {
            $this->log($e->getMessage());
            Mage::throwException($this->t($e->getMessage()));
        }
    }

    protected function getSavedCardFrom(Varien_Object $payment)
    {
        $card = $payment->getAdditionalInformation('token');

        if (strstr($card, 'card_') === false)
        {
            // $cards will be NULL if the customer has no cards
            $cards = $this->getCustomerCards(true, $payment->getOrder()->getCustomerId());
            if (is_array($cards) && !empty($cards[0]))
                return $cards[0]->id;
        }

        if (strstr($card, 'card_') === false)
            return null;

        return $card;
    }

    /**
     * Cancel payment
     *
     * @param   Varien_Object $invoicePayment
     * @return  Mage_Payment_Model_Abstract
     */
    public function cancel(Varien_Object $payment, $amount = null)
    {
        $amount = (is_null($amount)) ? $payment->getOrder()->getBaseTotalDue() : $amount;
        $transactionId = $payment->getParentTransactionId();
        $transactionId = preg_replace('/-.*$/', '', $transactionId);

        try {
            $params = array(
                'amount' => round($amount * 100)
            );
            $charge = Stripe_Charge::retrieve($transactionId);

            // This is true when an authorization has expired or when there was a refund through the Stripe account
            if (!$charge->refunded)
            {
                $charge->refund($params);

                $payment->getOrder()->addStatusToHistory(
                    Mage_Sales_Model_Order::STATE_CANCELED,
                    $this->t('Customer was refunded the amount of '). $amount
                );
            }
            else
            {
                $payment->getOrder()->addStatusToHistory(
                    Mage_Sales_Model_Order::STATE_CANCELED,
                    $this->t('Tried to refund customer but the order was already refunded.')
                );
            }
        }
        catch (Exception $e)
        {
            $this->log('Could not refund payment: '.$e->getMessage());
            Mage::throwException($this->t('Could not refund payment: ').$e->getMessage());
        }

        return $this;
    }

    /**
     * Refund money
     *
     * @param   Varien_Object $invoicePayment
     * @return  Mage_Payment_Model_Abstract
     */
    public function refund(Varien_Object $payment, $amount)
    {
        parent::refund($payment, $amount);
        $this->cancel($payment, $amount);

        return $this;
    }

    /**
     * Void payment
     *
     * @param   Varien_Object $invoicePayment
     * @return  Mage_Payment_Model_Abstract
     */
    public function void(Varien_Object $payment)
    {
        parent::void();
        $this->cancel($payment);

        return $this;
    }

    protected function getCustomerStripeId($customerId = null)
    {
        if ($this->customerStripeId)
            return $this->customerStripeId;

        // Get the magento customer id
        if (empty($customerId))
            $customerId = $this->getCustomerId();

        if (empty($customerId) || $customerId < 1)
            return false;

        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $query = $connection->select()
            ->from('cryozonic_stripesubscriptions_customers', array('*'))
            ->where('customer_id=?', $customerId);

        if ($this->getCustomerEmail())
            $query = $query->orWhere('customer_email=?', $this->getCustomerEmail());

        $result = $connection->fetchRow($query);
        if (empty($result)) return false;
        $this->customerLastRetrieved = $result['last_retrieved'];
        return $this->customerStripeId = $result['stripe_id'];
    }

    protected function getCustomerStripeIdByEmail($maxAge = null)
    {
        $email = $this->getCustomerEmail();

        if (empty($email))
            return false;

        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $query = $connection->select()
            ->from('cryozonic_stripesubscriptions_customers', array('*'))
            ->where('customer_email=?', $email);

        if (!empty($maxAge))
            $query = $query->where('last_retrieved >= ?', time() - $maxAge);

        $result = $connection->fetchRow($query);
        if (empty($result)) return false;
        return $this->customerStripeId = $result['stripe_id'];
    }

    protected function createStripeCustomer()
    {
        $quote = $this->getSessionQuote();
        $customerFirstname = $quote->getCustomerFirstname();
        $customerLastname = $quote->getCustomerLastname();
        $customerEmail = $quote->getCustomerEmail();
        $customerId = $quote->getCustomerId();

        // This may happen if we are creating an order from the back office
        if (empty($customerId) && empty($customerEmail))
            return;

        // When we are in guest or new customer checkout, we may have already created this customer
        if ($this->getCustomerStripeIdByEmail() !== false)
            return;

        // This is the case for new customer registrations and guest checkouts
        if (empty($customerId))
            $customerId = -1;

        try
        {
            $response = Stripe_Customer::create(array(
              "description" => "$customerFirstname $customerLastname",
              "email" => $customerEmail
            ));
            $response->save();

            $this->setStripeCustomerId($response->id, $customerId);

            return $this->customer = $response;
        }
        catch (Exception $e)
        {
            $this->log('Could not set up customer profile: '.$e->getMessage());
            Mage::throwException($this->t('Could not set up customer profile: ').$this->t($e->getMessage()));
        }

    }

    public function getStripeCustomer($id = null)
    {
        if ($this->customer)
            return $this->customer;

        if (empty($id))
            $id = $this->getCustomerStripeId();

        try
        {
            $this->customer = Stripe_Customer::retrieve($id);
            $this->updateLastRetrieved($this->customer->id);
            if (!$this->customer || $this->customer->deleted)
                return false;
            return $this->customer;
        }
        catch (Exception $e)
        {
            $this->log($this->t('Could not retrieve customer profile: '.$e->getMessage()));
            return false;
        }
    }

    public function deleteCards($cards)
    {
        $customer = $this->getStripeCustomer();

        if ($customer)
        {
            foreach ($cards as $cardId)
            {
                try
                {
                    $customer->cards->retrieve($cardId)->delete();
                }
                catch (Exception $e)
                {
                    // @todo
                }
            }
            $customer->save();
        }
    }

    protected function updateLastRetrieved($stripeCustomerId)
    {
        try
        {
            $resource = Mage::getSingleton('core/resource');
            $connection = $resource->getConnection('core_write');
            $fields = array();
            $fields['last_retrieved'] = time();
            $condition = array($connection->quoteInto('stripe_id=?', $stripeCustomerId));
            $result = $connection->update('cryozonic_stripesubscriptions_customers', $fields, $condition);
        }
        catch (Exception $e)
        {
            $this->log($this->t('Could not update Stripe customers table: '.$e->getMessage()));
        }
    }

    protected function deleteStripeCustomerId($stripeId)
    {
        try
        {
            $resource = Mage::getSingleton('core/resource');
            $connection = $resource->getConnection('core_write');
            $condition = array($connection->quoteInto('stripe_id=?', $stripeId));
            $connection->delete('cryozonic_stripesubscriptions_customers',$condition);
        }
        catch (Exception $e)
        {
            $this->log($this->t('Could not clear Stripe customers table: '.$e->getMessage()));
        }
    }

    protected function setStripeCustomerId($stripeId, $forCustomerId)
    {
        try
        {
            $resource = Mage::getSingleton('core/resource');
            $connection = $resource->getConnection('core_write');
            $fields = array();
            $fields['stripe_id'] = $stripeId;
            $fields['customer_id'] = $forCustomerId;
            $fields['last_retrieved'] = time();
            $fields['customer_email'] = $this->getCustomerEmail();
            $condition = array($connection->quoteInto('customer_id=? OR customer_email=?', $forCustomerId, $fields['customer_email']));
            $connection->delete('cryozonic_stripesubscriptions_customers',$condition);
            $result = $connection->insert('cryozonic_stripesubscriptions_customers', $fields);
        }
        catch (Exception $e)
        {
            $this->log($this->t('Could not update Stripe customers table: '.$e->getMessage()));
        }
    }

    public function getCustomerCards($isAdmin = false, $customerId = null)
    {
        if (!$this->saveCards && !$isAdmin)
            return null;

        if (!$customerId)
            $customerId = $this->getCustomerId();

        if (!$customerId)
            return null;

        $customerStripeId = $this->getCustomerStripeId($customerId);
        if (!$customerStripeId)
            return null;

        // Saved cards not supported on IE7
        if (!$isAdmin && strpos($_SERVER['HTTP_USER_AGENT'],'MSIE 7.0') !== false)
            return null;

        return $this->listCards($customerStripeId);
    }

    private function listCards($customerStripeId, $params = array())
    {
        try
        {
            return Stripe_Customer::retrieve($customerStripeId)->cards->all($params)->data;
        }
        catch (Exception $e)
        {
            return null;
        }
    }

    protected function log($msg)
    {
        Mage::log("Stripe Payments - ".$msg);
    }

    protected function t($str) {
        return Mage::helper('cryozonic_stripe')->__($str);
    }

    public function isGuest()
    {
        return $this->getSessionQuote()->getCheckoutMethod() == 'guest';
    }

    public function showSaveCardOption()
    {
        return ($this->saveCards && !$this->isGuest());
    }

    public function alwaysSaveCard()
    {
        return ($this->showSaveCardOption() && ($this->saveCards == 2 || !empty($this->hasRecurringProducts)));
    }
}
?>