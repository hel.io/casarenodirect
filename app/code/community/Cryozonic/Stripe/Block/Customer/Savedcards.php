<?php

class Cryozonic_Stripe_Block_Customer_Savedcards extends Mage_Customer_Block_Account_Dashboard
{
    protected function _construct()
    {
        parent::_construct();
        $this->stripe = Mage::getModel('cryozonic_stripe/standard');
    }
}
