jQuery(function($){
	//cart dropdown
	var config = {
	     over: function(){
	     			$('.cart-top .details').animate({opacity:1, height:'toggle'}, 400);
	     		},
	     timeout: 500, // number = milliseconds delay before onMouseOut
	     out: function(){
	     			$('.cart-top .details').animate({opacity:0, height:'toggle'}, 400);
	     		}
	};
	$("div.cart-top").hoverIntent( config );

	//fix menu behavior
	$('#nav li').hover(
		function(){
			$(this).children('div').addClass('shown-sub');
		},
		function(){
			$(this).children('div').removeClass('shown-sub');
		}
	);

	//fix description height
	$('#nav li.menu-category-description').each(function(){
		var height = 0;
		$(this).parent().children('li').each(function(){
			if ( $(this).height() > height )
				height = $(this).height();
		});
		$(this).height( height );
	});

	$('#newsletter').focus( function() {
	    if( $(this).val() == $(this).attr('title') ) { $(this).val(''); }
	  })
	  .blur( function() {
		if( $(this).val() == '' ) { $(this).val( $(this).attr('title') ); }
	});

	//banner hover
	$('.banners a, .banner a').each(function(i, obj){
		$(obj).append('<em/>');
	});

    //site-blocks
    $(".site-block-title").hover(
        function(e){
            if ( $(this).parent().hasClass('left-side') ) {
                $(this).parent().animate({ left: "0"} , 500);
            } else {
                $(this).parent().animate({ right: "0"} , 500);
            }
        },
        function(e){
            if ( $(this).parent().hasClass('left-side') ) {
                $(this).parent().animate({ left: -$(this).parent().width()} , 500);
            } else {
                $(this).parent().animate({ right: -$(this).parent().width()} , 500);
            }
        }
    );

});


	//smooth scrolling *http://css-tricks.com/snippets/jquery/smooth-scrolling/*


$(document).ready(function() {
  function filterPath(string) {
  return string
    .replace(/^\//,'')
    .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
    .replace(/\/$/,'');
  }
  var locationPath = filterPath(location.pathname);
  var scrollElem = scrollableElement('html', 'body');

  $('a[href*=#]').each(function() {
    var thisPath = filterPath(this.pathname) || locationPath;
    if (  locationPath == thisPath
    && (location.hostname == this.hostname || !this.hostname)
    && this.hash.replace(/#/,'') ) {
      var $target = $(this.hash), target = this.hash;
      if (target) {
        var targetOffset = $target.offset().top;
        $(this).click(function(event) {
          event.preventDefault();
          $(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
            location.hash = target;
          });
        });
      }
    }
  });

  // use the first element that is "scrollable"
  function scrollableElement(els) {
    for (var i = 0, argLength = arguments.length; i <argLength; i++) {
      var el = arguments[i],
          $scrollElement = $(el);
      if ($scrollElement.scrollTop()> 0) {
        return el;
      } else {
        $scrollElement.scrollTop(1);
        var isScrollable = $scrollElement.scrollTop()> 0;
        $scrollElement.scrollTop(0);
        if (isScrollable) {
          return el;
        }
      }
    }
    return [];
  }

});


$(function() {


  // pagecorner smooth scrolling

    var $sidebar   = $("#pagecorner"), 
        $window    = $(window),
        offset     = $sidebar.offset(),
        topPadding = 15;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding
            });
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            });
        }
    });
    
});


   // IE fix for div widths - size header to width of content
    if (!$.support.cssFloat) {
        $("div:has(.boxheader) > table").each(function () {
                $(this).parent().width($(this).width());
        });
    }