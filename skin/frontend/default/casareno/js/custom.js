jQuery.noConflict();
jQuery( document ).ready(function() {
    /*var defaults = {
		containerID: 'toTop', // fading element id
		containerHoverID: 'toTopHover', // fading element hover id
		scrollSpeed: 1200,
		easingType: 'linear'
	};
	
	//$("input[type='radio'], input[type='checkbox']").ionCheckRadio();
	jQuery().UItoTop({easingType: 'easeOutQuart'});
	jQuery(".selector").uniform();*/


	jQuery('button.mini-cart').bind('click', function() {
		if( jQuery(this).hasClass('cart-hide') ) {
			jQuery(this).removeClass('cart-hide');
			jQuery('.block-cart').animate( {right: '-900px'}, 500, function() {
				jQuery(this).removeClass('active');//addClass('active');
			});//addClass('active');
			
			jQuery('.row-offcanvas').animate( {left:'0px'}, 300, function() {
				jQuery(this).removeClass('active');//addClass('active');
			});
												
		}else {
			jQuery(this).addClass('cart-hide')
			jQuery('.row-offcanvas').addClass('active').animate( {left:'-80%'}, 300 );//addClass('active');
			jQuery('.block-cart').addClass('active').animate( {right: '0px'}, 500 );//addClass('active');
			//$('.cart-block').addClass('active');
		}
	});

	if((!navigator.userAgent.match(/iPhone/i)) && !(navigator.userAgent.match(/iPod/i))) {
		jQuery('body').addClass('not-ios');
	}
	
	if(jQuery('#contact_right').length)
	{
		jQuery(".select_contact").change(function() {
			var current = jQuery(this).val();
			jQuery('#contact_right').fadeOut('slow').promise().done(function(){
				var new_html = jQuery('.contacts-container .'+current).html();
				jQuery('#contact_right').html(new_html);
				jQuery(this).fadeIn('slow').promise().done(function(){
			
				});
			});
		});
	}
	jQuery("#localisation_choose").change(function() {
		var loc = jQuery(this).val();
		window.location.href = window.location.origin+'/'+loc; 
	});
	
	jQuery('.color_palit .tab a').on('click', function(){
		var temp = jQuery(this).attr('id').split('_');
		var id = temp[1];
		jQuery('.color_palit .tab').removeClass('active_li');
		jQuery('.color_palit .tab a').removeClass('active');
		jQuery('#tab_'+id).parent('li').addClass('active_li');
		jQuery('#tab_'+id).addClass('active');

		if(jQuery('#content_'+id).length)
		{
			jQuery('.content-tab').fadeOut('slow').promise().done(function(){
				jQuery('#content_'+id).fadeIn('slow').promise().done(function(){});
			});
		}
	});
	
	if(jQuery('.tooltipster').length > 0)
	{
		jQuery('.tooltipster').tooltipster({});
	}
	
});