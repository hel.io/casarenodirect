/*
 * Search As You Type Widget
 *
 * Apply to any text control to turn it into a search as you type control.  Will pop up a div to display search results (or multimple search results)
 * based on user entry into the text field.  Baseline rendered structure is:
 *
 * <div class='sayt'>
 * 		<div class='sayt-section'>
 * 			<div>header</div>
 * 			<ul>
 * 				<li>template
 * 					<ul>
 * 						<li>subFieldTemplate</li>
 * 						...
 * 					</ul>
 * 				</li>
 * 				...
 * 			</ul>
 * 			<div>footer</div>
 * 		</div>
 * 		<div class='sayt-section'>
 * 			<div>header</div>
 * 			<ul>
 * 				<li>template</li>
 * 				...
 * 			</ul>
 * 			<div>footer</div>
 * 		</div>
 * 		...
 * </div>
 *
 * Events:
 *
 * 	requestResults - {
 * 						section: 0, // The section requesting data
 * 						userText: "", // The text the user has typed so far
 * 						fieldText: "", // The text actually in the text control (can be different from userText if a selected item is overriding the text
 * 						selectedSection: 0, // The section holding the selected item
 * 						selectedData: user text, string or object, // The data object associated with the selected item (if ther is a selection)
 * 						selectedSubdata: string or object // The subdata object associated with the selected item (if applicable)
 * 					 }
 *
 *  search - {
 * 				userText: "", // The text the user has typed so far
 * 				fieldText: "", // The text actually in the text control (can be different from userText if a selected item is overriding the text
 * 				selectedSection: 0, // The section holding the selected item
 * 				selectedData: user text, string or object, // The data object associated with the selected item (if ther is a selection)
 * 				selectedSubdata: string or object // The subdata object associated with the selected item (if applicable)
 *  		 }
 */
 
// Avoid PrototypeJS conflicts, assign jQuery to $j instead of $
var $j = jQuery.noConflict();

$j.widget( "innovent.sayt", {
	options: {
	 	applyClass: 'sayt', // Class to apply to div containing the suggestions
		minLengthToQuery: 2, // Required length of user input before a query is sent
		queryDelay: 100, // Number of milliseconds that must pass without user activity before a query is sent
	 	vOffset: 0, // Indicates how far down to move the suggestions window from the bottom of the text control
	 	hOffset: 0, // Indicates how for right to move the suggestions window from the left of the text control
	 	orientation: 'horizontal', // The orientation of the sections for keyboard navigation, vertical or horizontal
		sections: [] // Array of section objects
	 },

	 section: {
  	   selectable: true, // Items in this section can be highlighted
	   autocompleteField: null, // The field name or array index of the field containing the value to be displayed in the text control; null indicates never autofill text control
	   suggestUserText: false, // User text should be added to the suggestions list as the first item
	   preselectFirst: false, // Select the first item in the section as soon as it's loaded
	   minimumResultsToDisplay: 0, // Number of results that must be available for the section to display
	   source: 'user', // Source of value to request results for; 'user', 'selection', 'either', or 'both' (both only requests results when there is user text and a selection is available)
	   minLengthToQuery: 1, // Local override of global minLengthToQuery
	   target: null, // Indicates an element to render the section structure into. If null, then target is part of a pop-up display
	   header: null, // Value to put in a header for the section (rendered in a div)
	   footer: null, // Value to put in a footer for the section (rendered in a div)
	   template: '{$!:0}', // The template to display for the field. {$} renders the whole data value, {$:field} renders the specified field within the data object; the ! indicates that text matching User Text should be highlighted
	   renderCB: null, // A callback method to render a cell; template and renderCB are mutually exclusive - renderCB(dataObject) returns HTML string
	   subField: null, // The field name or index of a field containing an array of values to display in an inner list
	   subFieldTemplate: null, // Same as template, but for subField objects
	   subFieldRenderCB: null // A callback method to render the subfield content
	 },

	 KEY_ENTER: 13,
	 KEY_LEFT: 37,
	 KEY_UP: 38,
	 KEY_RIGHT: 39,
	 KEY_DOWN: 40,

	 TEMPLATE_FIELD_BANG_REGEX: /\{\$!:(.*?)\}/,
	 TEMPLATE_FIELD_REGEX: /\{\$:.*?\}/,
	 TEMPLATE_BANG_REGEX: /\{\$!\}/,
	 TEMPLATE_REGEX: /\{\$\}/,

	_create: function() {
		this.queryTimer = null;
		this.targetElement = null;
		this.mouseOverTarget = false;
		this.userText = '';
		this.translateDiv = $j('<div />');

		this.element.attr('autocomplete', 'off');

		// Make sure all sections have the minimum fields set
		for (var x = 0; x < this.options.sections.length; x++) {
			this.options.sections[x] = $j.extend(true, {_lastQuery: {}}, this.section, this.options.sections[x]);
		}

		this._createTargetElement();

		// Set up the scroll event handler for autoloading
		this.element.keyup(this._keyup.bind(this));
		this.element.focus(this._focus.bind(this));
		this.element.blur(this._blur.bind(this));
	},

	_createTargetElement: function() {
		$j(this.targetElement).remove();
		if ($j.isArray(this.options.sections)) {
			for (var x = 0; x < this.options.sections.length; x++) {
				if (this.options.sections[x].target == null) {
					// Create main SAYT div if necessary
					if (this.targetElement == null) {
						this.targetElement = $j('<div class="' + this.options.applyClass + '" />');
						var offset = this.element.offset();
						this.targetElement.css({
							left: this.element.offset().left + this.options.hOffset,
							top: this.element.offset().top + this.element.height() + this.options.vOffset
						});
						this.targetElement.hide();
						this.targetElement.appendTo('body');
						this.targetElement.mouseenter(this._targetMouseEnter.bind(this));
						this.targetElement.mouseleave(this._targetMouseLeave.bind(this));
					}

					// Append the main section div
					this.options.sections[x].target = $j('<div />');
					this.targetElement.append(this.options.sections[x].target);
				}

				// Append the section layout
				if (this.options.sections[x].header) {
					this.options.sections[x].target.append($j('<div class="sayt-section-header">' + this.options.sections[x].header + '</div>'));
				}
				this.options.sections[x].target.append($j('<ul class="sayt-section-list" />'));
				if (this.options.sections[x].footer) {
					this.options.sections[x].target.append($j('<div class="sayt-section-footer">' + this.options.sections[x].footer + '</div>'));
					if (this.options.orientation == 'horizontal') {
						this.options.sections[x].target.append($j('<div class="sayt-section-footer" style="position:relative;visibility:hidden">' + this.options.sections[x].footer + '</div>'));
					}
				}
				if (this.options.sections[x].minimumResultsToDisplay > 0) {
					this.options.sections[x].target.hide();
				} else {
					this.options.sections[x].target.show();
				}
			}
		}
	},

	val: function() {
		if (arguments.length > 0) {
			this.element.val(arguments[0]);
		} else {
			return this.element.val();
		}
	},

	setSectionResults: function(sectionNumber, results) {
		if ($j.isArray(results)) {
			if (this.options.sections[sectionNumber].minimumResultsToDisplay > results.length) {
				this.options.sections[sectionNumber].target.hide();
			} else {
				var ulEl = this.options.sections[sectionNumber].target.find('ul.sayt-section-list');
				ulEl.empty();
				var ctr = 0;
				for (var x = 0; x < results.length; x++) {
					var liEl = $j('<li />');
					liEl.data('section', sectionNumber);
					liEl.data('mainRec', results[x]);
					liEl.data('order', ctr++);
					var liElContent = $j('<div />');
					liEl.append(liElContent);
					if (this.options.sections[sectionNumber].selectable) {
						liElContent.addClass('selectable');
						liElContent.mouseenter(this._liMouseEnter.bind(this));
						liElContent.mouseleave(this._liMouseLeave.bind(this));
						liElContent.click(this._liClick.bind(this));
					}
					if (typeof this.options.sections[sectionNumber].renderCB === 'function') {
						liElContent.append(this.options.sections[sectionNumber].renderCB(results[x]));
					} else {
						liElContent.append(this._renderTemplate(this.options.sections[sectionNumber].template, results[x]));
					}
					if (typeof this.options.sections[sectionNumber].subField === 'string') {
						var subUlEl = $j('<ul class="sayt-section-sub-list" />');
						var subField = results[x][this.options.sections[sectionNumber].subField];
						if ($j.isArray(subField)) {
							for (var y = 0; y < subField.length; y++) {
								var subLiEl = $j('<li />');
								subLiEl.data('section', sectionNumber);
								subLiEl.data('mainRec', results[x]);
								subLiEl.data('subRec', subField[y]);
								subLiEl.data('order', ctr++);
								var subLiElContent = $j('<div />');
								subLiEl.append(subLiElContent);
								if (this.options.sections[sectionNumber].selectable) {
									subLiElContent.addClass('selectable');
									subLiElContent.mouseenter(this._liMouseEnter.bind(this));
									subLiElContent.mouseleave(this._liMouseLeave.bind(this));
									subLiElContent.click(this._liClick.bind(this));
								}
								if (typeof this.options.sections[sectionNumber].renderCB === 'function') {
									subLiElContent.append(this.options.sections[sectionNumber].subFieldRenderCB(subField[y]));
								} else {
									subLiElContent.append(this._renderTemplate(this.options.sections[sectionNumber].subFieldTemplate, subField[y]));
								}
								subUlEl.append(subLiEl);
							}
						}
						liEl.append(subUlEl);
					}
					ulEl.append(liEl);
				}
				this.options.sections[sectionNumber].target.show();
			}
		} else {
			if (this.options.sections[sectionNumber].minimumResultsToDisplay > 0) {
				this.options.sections[sectionNumber].target.hide();
			} else {
				this.options.sections[sectionNumber].target.show();
			}
		}
	},

	_initiateQuerDelay: function() {
		if (this.queryTimer != null) {
			clearTimeout(this.queryTimer);
		}
		this.queryTimer = setTimeout(this._timerEvent.bind(this), this.options.queryDelay);
	},

	_renderTemplate: function(template, data) {
		var out = template;
		var match = null;
		// First look for field matches with !
		while (match = this.TEMPLATE_FIELD_BANG_REGEX.exec(out)) {
			var txt = data[match[1]];
			if (typeof txt === 'string') {
				// Highlight matching text matching user text
				var regEx = new RegExp('(' + this.element.val().replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + ')', "i");
				txt = txt.replace(regEx, '<span class="sayt-text-highlight">' + '$1' + '</span>');
			}
			if (typeof txt === 'undefined' || typeof txt === 'object') {
				out = out.replace(match[0], '');
			} else {
				out = out.replace(match[0], txt);
			}
		}
		// Next handle field matches without !
		while (match = this.TEMPLATE_FIELD_REGEX.exec(out)) {
			var txt = data[match[0].substring(3, match[0].length - 1)];
			if (typeof txt === 'undefined' || typeof txt === 'object') {
				out = out.replace(match[0], '');
			} else {
				out = out.replace(match[0], txt);
			}
		}
		// Next handle full data match with !
		while (match = this.TEMPLATE_BANG_REGEX.exec(out)) {
			var txt = data;
			if (typeof txt === 'string') {
				// Highlight matching text matching user text
				var regEx = new RegExp('(' + this.element.val().replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + ')', "i");
				txt = txt.replace(regEx, '<span class="sayt-text-highlight">' + '$1' + '</span>');
			}
			if (typeof txt === 'undefined' || typeof txt === 'object') {
				out = out.replace(match[0], '');
			} else {
				out = out.replace(match[0], txt);
			}
		}
		// Finally, handle full data match without !
		while (match = this.TEMPLATE_REGEX.exec(out)) {
			if (typeof data === 'undefined' || typeof data === 'object') {
				out = out.replace(match[0], '');
			} else {
				out = out.replace(match[0], data);
			}
		}
		return out;
	},

	_updateSuggestions: function() {
		if (this.element.val().length >= this.options.minLengthToQuery) {
			if (this.targetElement) {
				this.targetElement.show();
			}
			// Loop through the sections sending queries as necessary
			for (var x = 0; x < this.options.sections.length; x++) {
				if (this.options.sections[x].target) {
					var selectedLi = this._findSelectedItem();
					var eventData = { section: x, userText: this.userText, fieldText: this.element.val(), selectedSection: selectedLi.data('section'), selectedData: selectedLi.data('mainRec'), selectedSubdata: selectedLi.data('subRec') };
					if (this.options.sections[x].source == 'user') {
						if (this.element.val().length >= this.options.sections[x].minLengthToQuery) {
							if (this.options.sections[x]._lastQuery.userText != this.userText) {
								this.element.trigger('requestResults', { section: x, userText: this.userText, fieldText: this.element.val() });
							}
						} else {
							//this.options.sections[x].target.empty();
							 this.options.sections[x].target.find('ul.sayt-section-list').empty();
							if (this.options.sections[x].minimumResultsToDisplay > 0) {
								this.options.sections[x].target.hide();
							}
						}
					} else if (this.options.sections[x].source == 'selection') {
						if (selectedLi.length > 0) {
							if (JSON.stringify(this.options.sections[x]._lastQuery) != JSON.stringify(eventData)) {
								if(eventData.fieldText != eventData.userText){
									this.element.trigger('requestResults', eventData);
								}
							}


						} else {
							//this.options.sections[x].target.empty();
							 this.options.sections[x].target.find('ul.sayt-section-list').empty();
							if (this.options.sections[x].minimumResultsToDisplay > 0) {
								this.options.sections[x].target.hide();
							}
						}
					} else if (this.options.sections[x].source == 'either') {
						if (selectedLi.length > 0) {
							if (JSON.stringify(this.options.sections[x]._lastQuery) != JSON.stringify(eventData)) {
								this.element.trigger('requestResults', eventData);
							}
						} else if (this.element.val().length >= this.options.sections[x].minLengthToQuery) {
							if (JSON.stringify(this.options.sections[x]._lastQuery) != JSON.stringify(eventData)) {
								this.element.trigger('requestResults', eventData);
							}
						} else {
							//this.options.sections[x].target.empty();
							 this.options.sections[x].target.find('ul.sayt-section-list').empty();
							if (this.options.sections[x].minimumResultsToDisplay > 0) {
								this.options.sections[x].target.hide();
							}
						}
					} else if (this.options.sections[x].source == 'both') {
						if (selectedLi.length > 0 && this.element.val().length >= this.options.sections[x].minLengthToQuery) {
							if (JSON.stringify(this.options.sections[x]._lastQuery) != JSON.stringify(eventData)) {
								this.element.trigger('requestResults', eventData);
							}
						} else {
							//this.options.sections[x].target.empty();
							this.options.sections[x].target.find('ul.sayt-section-list').empty();
							if (this.options.sections[x].minimumResultsToDisplay > 0) {
								this.options.sections[x].target.hide();
							}
						}
					}
					if (this.options.sections[x].minimumResultsToDisplay == 0) {
						this.options.sections[x].target.show();
					}
					this.options.sections[x]._lastQuery = eventData;
				}
			}
		} else { // Clear results and hide the sayt div
			if (this.targetElement) {
				this.targetElement.hide();
			}
			for (var x = 0; x < this.options.sections.length; x++) {
				if (this.options.sections[x].target) {
					$j(this.options.sections[x].target).find('ul.sayt-section-list').empty();
					if (this.options.sections[x].minimumResultsToDisplay > 0) {
						$j(this.options.sections[x].target).hide();
					}
				}
			}
		}
	},

	_findSelectedItem: function() {
		var x = 0;
		var selected = $j([]);
		while (x < this.options.sections.length && selected.length == 0) {
			selected = selected.add(this.options.sections[x].target.find('li.selected'));
			x++;
		}
		return selected;
	},

	_selectItem: function(target) {
		if (this.options.sections[target.data('section')].source == 'user') {
			this.targetElement.find('li.selected').removeClass('selected');
		} else {
			this.options.sections[target.data('section')].target.find('li.selected').removeClass('selected');
		}
		target.addClass('selected');
		if (this.options.sections[target.data('section')].autocompleteField !== null) {
			// Hack to translate HTML encoded values to straight text
			this.translateDiv.html(target.data('mainRec')[this.options.sections[target.data('section')].autocompleteField]);
			this.element.val('').val(this.translateDiv.text());
		} else {
			this.element.val('').val(this.userText);
		}
		this._initiateQuerDelay();
	},

	_clearSelection: function() {
		this.targetElement.find('li.selected').removeClass('selected');
		this.element.val(this.userText);
		this._initiateQuerDelay();
	},

	_selectPreviousSectionLastItem: function(start) {
		for (var x = start - 1; x >= 0; x--) {
			if (this.options.sections[x].selectable) {
				var lastChild = this.options.sections[x].target.children().children('li:last');
				if (lastChild.length > 0) {
					// Check if the last child has children
					var lastChildOfChild = lastChild.children('ul').children('li:last');
					if (lastChildOfChild.length > 0) {
						this._selectItem(lastChildOfChild);
					} else {
						this._selectItem(lastChild);
					}
					return true;
				}
			}
		}
		return false;
	},

	_selectNextSectionFirstItem: function(start) {
		// Find the first LI in the first selectable section
		for (var x = start + 1; x < this.options.sections.length; x++) {
			if (this.options.sections[x].selectable) {
				var firstChild = this.options.sections[x].target.children().children('li:first');
				if (firstChild.length > 0) {
					this._selectItem(firstChild);
					return true;
				}
			}
		}
		return false;
	},

	_selectNextSectionLastItem: function(start) {
		for (var x = start + 1; x < this.options.sections.length; x++) {
			if (this.options.sections[x].selectable) {
				var lastChild = this.options.sections[x].target.children().children('li:last');
				if (lastChild.length > 0) {
					// Check if the last child has children
					var lastChildOfChild = lastChild.children('ul').children('li:last');
					if (lastChildOfChild.length > 0) {
						this._selectItem(lastChildOfChild);
					} else {
						this._selectItem(lastChild);
					}
					return true;
				}
			}
		}
		return false;
	},

	// Events
	_keyup: function(event, ui) {
		if (event.keyCode == this.KEY_UP) {
			var selected = this._findSelectedItem();
			if (selected.length > 0) {
				// Check if this item has a previous sibling available
				var prevSibling = selected.prev();
				if (prevSibling.length > 0) {
					// Check if previous sibling has children
					var prevSibChild = prevSibling.children('ul').children('li:last');
					if (prevSibChild.length > 0) {
						this._selectItem(prevSibChild);
					} else {
						this._selectItem(prevSibling);
					}
					return;
				}

				// Check if this item has a parent
				var parent = selected.parent().parent();
				if (parent.is('li')) {
					this._selectItem(parent);
					return;
				}

				// Otherwise, it's at the top of the section
				if (this.options.orientation == 'horizontal') {
					// If orientation is horizontal, just remove clear selections and restore user text
					this._clearSelection();
				} else {
					// Otherwise, see if there is a previous section to move into
					if (!this._selectPreviousSectionLastItem(selected.data('section'))) {
						this._clearSelection();
					}
				}
			} else {
				// If the orientation is horizontal, find the last LI in the first selectable section
				// If the orientation is vertical, find the last LI in the last selectable section
				if (this.options.orientation == 'horizontal') {
					this._selectNextSectionLastItem(-1);
				} else {
					this._selectPreviousSectionLastItem(this.options.sections.length);
				}
			}
		} else if (event.keyCode == this.KEY_DOWN) {
			var selected = this._findSelectedItem();
			if (selected.length > 0) {
				// Check if this item has children to step through
				var firstChild = selected.children('ul').children('li:first');
				if (firstChild.length > 0) {
					this._selectItem(firstChild);
					return;
				}

				// Check if this item is at the end of the list
				var nextSibling = selected.next();
				if (nextSibling.length > 0) {
					this._selectItem(nextSibling);
					return;
				} else {
					// If this is a subdata node, check if the parent has a sibling
					nextSibling = selected.parent().parent().next();
					if (nextSibling.is('li')) {
						this._selectItem(nextSibling);
						return;
					}
				}

				// Otherwise it's at the bottom of the list
				if (this.options.orientation == 'horizontal') {
					// just clear selections and restore user text
					this._clearSelection();
				} else {
					// See if there is a next section to move into
					if (!this._selectNextSectionFirstItem(selected.data('section'))) {
						this._clearSelection();
					}
				}
			} else {
				this._selectNextSectionFirstItem(-1);
			}
		} else if (event.keyCode == this.KEY_RIGHT) {
			if (this.options.orientation == 'horizontal') {
				var selected = this._findSelectedItem();
				if (selected.length > 0) {
					// Find the first available selectable section, starting with the one after the current one and looping around
					var section = selected.data('section') + 1;
					var sectionList = [];
					while (section < this.options.sections.length && sectionList.length == 0) {
						if (this.options.sections[section].selectable) {
							sectionList = this.options.sections[section].target.find('li');
						}
						section++;
					}
					if (sectionList.length == 0) {
						section = 0;
						while (section < selected.data('section') && sectionList.length == 0) {
							if (this.options.sections[section].selectable) {
								sectionList = this.options.sections[section].target.find('li');
							}
							section++;
						}
					}
					if (sectionList.length > 0) {
						if (selected.data('order') < sectionList.length) {
							// Find the matching order number and select that item
							for (var x = 0; x < sectionList.length; x++) {
								if ($j(sectionList[x]).data('order') == selected.data('order')) {
									this._selectItem($j(sectionList[x]));
								}
							}
						} else {
							// Find the highest order number item in the section list and select that one
							var highestItem = null;
							for (var x = 0; x < sectionList.length; x++) {
								if (highestItem == null) {
									highestItem = $j(sectionList[x]);
								} else if ($j(sectionList[x]).data('order') > highestItem.data('order')) {
									highestItem = $j(sectionList[x]);
								}
							}
							this._selectItem(highestItem);
						}
					} // Else don't change the selection
					event.preventDefault();
				} // Else don't do anything because the user is just manipulating the text field
			} // Else don't do anything because left and right don't mean anything in a vertical arrangement
		} else if (event.keyCode == this.KEY_LEFT) {
			if (this.options.orientation == 'horizontal') {
				var selected = this._findSelectedItem();
				if (selected.length > 0) {
					// Find the first available selectable section, starting with the one before the current one and looping around
					var section = selected.data('section') - 1;
					var sectionList = [];
					while (section >= 0 && sectionList.length == 0) {
						if (this.options.sections[section].selectable) {
							sectionList = this.options.sections[section].target.find('li');
						}
						section--;
					}
					if (sectionList.length == 0) {
						section = this.options.sections.length - 1;
						while (section > selected.data('section') && sectionList.length == 0) {
							if (this.options.sections[section].selectable) {
								sectionList = this.options.sections[section].target.find('li');
							}
							section--;
						}
					}
					if (sectionList.length > 0) {
						if (selected.data('order') < sectionList.length) {
							// Find the matching order number and select that item
							for (var x = 0; x < sectionList.length; x++) {
								if ($j(sectionList[x]).data('order') == selected.data('order')) {
									this._selectItem($j(sectionList[x]));
								}
							}
						} else {
							// Find the highest order number item in the section list and select that one
							var highestItem = null;
							for (var x = 0; x < sectionList.length; x++) {
								if (highestItem == null) {
									highestItem = $j(sectionList[x]);
								} else if ($j(sectionList[x]).data('order') > highestItem.data('order')) {
									highestItem = $j(sectionList[x]);
								}
							}
							this._selectItem(highestItem);
						}
					} // Else don't change the selection
					event.preventDefault();
				} // Else don't do anything because the user is just manipulating the text field
			} // Else don't do anything because left and right don't mean anything in a vertical arrangement
		} else if (event.keyCode == this.KEY_ENTER) {
			var selected = this._findSelectedItem();
			if (selected.length > 0) {
				if (selected.length > 1) {
					// Find the selected item that is in the dependency
					for (var x = 0; x < selected.length; x++) {
						if (this.options.sections[selected[x].data('section')].source != 'user') {
							selected = $j(selected[x]);
							break;
						}
					}
				}
				this.element.trigger('search', { userText: this.userText, fieldText: this.element.val(), selectedSection: selected.data('section'), selectedData: selected.data('mainRec'), selectedSubdata: selected.data('subRec') });
				event.preventDefault();
			} // Else let the normal field operation occur
		} else {
			this.userText = this.element.val();
		}
		this._initiateQuerDelay();
	},

	_focus: function() {
		this._updateSuggestions();
	},

	_blur: function() {
		// Don't hide the target element if either the text control or the target element has the focus
		if (this.mouseOverTarget) {
			this.element.focus();
		} else {
			if (this.targetElement !== 'null') {
				this.targetElement.hide();
			}
			this.element.val(this.userText);
		}
	},

	_timerEvent: function() {
		this._updateSuggestions();
		this.queryTimer = null;
	},

	_targetMouseEnter: function(event, ui) {
		this.mouseOverTarget = true;
	},

	_targetMouseLeave: function(event, ui) {
		this.mouseOverTarget = false;
		this._clearSelection();
	},

	_liMouseEnter: function(event, ui) {
		this._selectItem($j(event.currentTarget).parent());
	},

	_liMouseLeave: function(event, ui) {
//		this._clearSelection();
	},

	_liClick: function(event, ui) {
		var eventTarget = $j(event.currentTarget).parent();
		this.element.trigger('search', { userText: this.userText, fieldText: this.element.val(), selectedSection: eventTarget.data('section'), selectedData: eventTarget.data('mainRec'), selectedSubdata: eventTarget.data('subRec') });
	}
});
