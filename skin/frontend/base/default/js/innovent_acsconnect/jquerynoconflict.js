<!--
/*
 * Innovent_CloudsearchConnect extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Extension License Agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.innoventsolutions.com/magento-extension-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to magento@innoventsolutions.com so we will send you a copy immediately.
 *
 *
 * @category	Innovent
 * @package		Innovent_CloudsearchConnect
 * @copyright   Copyright (c) 2014 Innovent Solutions Inc. (http://www.innoventsolutions.com)
 * @author      Innovent Solutions, Inc.
 * @license     http://www.innoventsolutions.com/magento-extension-license.html
 */
-->

// Avoid PrototypeJS conflicts, assign jQuery to $j instead of $
var $j = jQuery.noConflict();
